﻿using UnityEngine;
using System.Collections;

public class OpenDoor : MonoBehaviour {

	private Quaternion initRot;
	public bool closed;
	public float openAngle;
	private Quaternion open;
	public float openingSpeed = 2f;

	private MeshCollider col;

	// Use this for initialization
	void Start () {

		initRot = transform.localRotation;
		closed = true;
		open = Quaternion.Euler(transform.localRotation.eulerAngles.x, openAngle , transform.localRotation.eulerAngles.z);

		col = GetComponent<MeshCollider> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.localRotation.eulerAngles.y < 10 && transform.localRotation.eulerAngles.y > -10) {
			col.enabled = true;
		} else if (transform.localRotation.eulerAngles.y > openAngle - 10 || transform.localRotation.eulerAngles.y < -openAngle + 10) {
			col.enabled = true;
		} else {
			col.enabled = false;
		}

	}

	void OnTriggerStay(Collider player){
		if(Input.GetButtonDown("Interact") && player.CompareTag("Player")){
			closed = !closed;

			col.enabled = false;

			if (!closed) {
				StopCoroutine("CloseDoor");
				StartCoroutine("OpenDoorPos");

				
			}
			if (closed) {
				StopCoroutine("OpenDoorPos");
				StartCoroutine("CloseDoor");
			}
			
		}
	}

	IEnumerator OpenDoorPos(){
		while (transform.localRotation.eulerAngles.y < openAngle - 1) {
			transform.localRotation = Quaternion.Slerp (transform.localRotation, open, openingSpeed * Time.deltaTime); 
			yield return 0;
		}
		transform.localRotation = open;
	}

	IEnumerator CloseDoor(){
		while (transform.localRotation.eulerAngles.y > 1 || transform.localRotation.eulerAngles.y < -1) {
			transform.localRotation = Quaternion.Slerp (transform.localRotation, initRot, openingSpeed * Time.deltaTime); 
			yield return 0;
		}
		transform.localRotation = initRot;

	}
}
