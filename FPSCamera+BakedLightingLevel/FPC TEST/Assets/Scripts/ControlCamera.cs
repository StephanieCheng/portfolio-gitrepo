﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class ControlCamera : MonoBehaviour {

	private Camera cam;
	public float zoomSpeed;
	private DepthOfField dofScript;

//	public float focalDistance =  1.68f;
//	public float focalSize = 0.63f;

	public float zoomFOV = 35f;

	
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera> (); 
		dofScript = GetComponent<DepthOfField> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown ("Zoom")) {
			StopCoroutine("ZoomOutCam");
			StartCoroutine("ZoomInCam");
			dofScript.enabled = false;

			
		}
		if (Input.GetButtonUp ("Zoom")) {
			StopCoroutine("ZoomInCam");
			StartCoroutine("ZoomOutCam");

			dofScript.enabled = true;
		}
	
	}

	IEnumerator ZoomInCam(){

		while (cam.fieldOfView > zoomFOV+0.1f) {
			cam.fieldOfView = Mathf.Lerp (cam.fieldOfView, zoomFOV, zoomSpeed * Time.deltaTime);
			yield return 0;
		}
		cam.fieldOfView = zoomFOV;


	}

	IEnumerator ZoomOutCam(){
		
		while (cam.fieldOfView < 59.9f) {
			cam.fieldOfView = Mathf.Lerp (cam.fieldOfView, 60f, zoomSpeed * Time.deltaTime);
			yield return 0;
		}
		cam.fieldOfView = 60f;
		
	}


}
