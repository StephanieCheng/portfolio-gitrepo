﻿using UnityEngine;
using System.Collections;

public class BobbingOverride : MonoBehaviour {

	private Transform CamCenter;


	// Use this for initialization
	void Start () {
		CamCenter = GameObject.FindGameObjectWithTag ("TransformReference").transform;

	}
	void Update(){


	}
	// Update is called once per frame
	void LateUpdate () {

		//Debug.Log (CamCenter.localRotation.eulerAngles);
		transform.localRotation = Quaternion.Euler (transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, CamCenter.localRotation.eulerAngles.z);
	}
}
