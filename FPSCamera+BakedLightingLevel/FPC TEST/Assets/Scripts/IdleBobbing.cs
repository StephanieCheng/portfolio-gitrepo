﻿using UnityEngine;
using System.Collections;

public class IdleBobbing : MonoBehaviour {

	public float bobRange = 3f;

	public float bobSpeed = 0.2f;

	private Quaternion centerRotation;

	// Use this for initialization
	void Start () {
		centerRotation = transform.localRotation;

	
	}
	
	// Update is called once per frame
	void LateUpdate () {


	
		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");


		//centerRotation = Quaternion.Euler (transform.rotation.eulerAngles.x, centerRotation.eulerAngles.y, centerRotation.eulerAngles.z);

		Bobb ();
	}

	void Bobb (){


		float zAngle = (Hermite(Mathf.PingPong(Time.time * bobSpeed, 1 )) * bobRange - bobRange/4f ) * -1;
		transform.localRotation = transform.localRotation * Quaternion.Euler(0,0,zAngle);
		Debug.Log (transform.localRotation.eulerAngles);
			

	}

	float Hermite(float t)
	{
		return -t*t*t*2f + t*t*3f;
	}
}
