﻿using UnityEngine;
using System.Collections;

public class FlashLight : MonoBehaviour {
	private bool lightOn;
	private Light lightSource; 

	private int levelMask ;
	public float camRayLength =100f;

	private Quaternion flashlightCenter;
	public float bobSpeed = 0.6f;
	public float bobRange = 6f;

	public float fRegPercentage = 0.5f;
	 
	public Rect fRegion;

	// Use this for initialization
	void Start () {
		lightSource = GetComponent<Light> ();
		flashlightCenter = transform.localRotation;

	}
	
	// Update is called once per frame
	void Update () {


		//levelMask = LayerMask.GetMask("Level");

		//turn on off
		if (Input.GetButtonDown ("Flashlight")) {

			if (lightOn){
				lightSource.enabled = false;
				lightOn = false;
			}
			else{
				lightSource.enabled = true;
				lightOn = true;
			}
				
		}

		//FlashlightTrack ();

		//transform.rotation = flashlightCenter;


		Bobbing();

	
	}

	void FlashlightTrack(){

		fRegion = new Rect (Screen.width / 2f - (Screen.width * fRegPercentage)/2f,  Screen.height / 2 - (Screen.height *fRegPercentage)/2f, 
		                    Screen.width* fRegPercentage, Screen.height*fRegPercentage);


		RaycastHit levelHit;

		Vector2 boundedMouse = new Vector2 (Mathf.Clamp(Input.mousePosition.x, fRegion.min.x, fRegion.max.x),
		                                    Mathf.Clamp(Input.mousePosition.y, fRegion.min.y, fRegion.max.y));

		Ray camRay = Camera.main.ScreenPointToRay (boundedMouse);                                 
		
		if (Physics.Raycast (camRay, out levelHit, camRayLength, levelMask)) {
			transform.LookAt(levelHit.point);
			
			
		}




	}
	

	void Bobbing(){


		//float yAngle = Mathf.PingPong(Time.time * bobSpeed, 10 ) - 5 ;

		float yAngle = Hermite(Mathf.PingPong(Time.time * bobSpeed, 1 )) * bobRange - bobRange/2f ;

		float xAngle = Hermite(Mathf.PingPong(Time.time * bobSpeed * 2, 1 )) * bobRange - bobRange/2f ;

		//transform.localRotation = transform.localRotation * Quaternion.Euler (xAngle, yAngle, 0); //flashlightCenter if something goes wrong
		transform.localRotation = flashlightCenter * Quaternion.Euler (xAngle, yAngle, 0); //flashlightCenter if something goes wrong


	}

	float Hermite(float t)
	{
		return -t*t*t*2f + t*t*3f;
	}


//	void OnGUI() {
//		DrawQuad (fRegion, Color.blue);
//	}
//
//
//	void DrawQuad(Rect position, Color color) {
//		Texture2D texture = new Texture2D(1, 1);
//		texture.SetPixel(0,0,color);
//		texture.Apply();
//		GUI.skin.box.normal.background = texture;
//		GUI.Box(position, GUIContent.none);
//	}
}
