﻿using UnityEngine;
using System.Collections;

public class CamCenterControll : MonoBehaviour {

	private bool walking;
	private Animator anim;
	
	private float horizontal;
	private float vertical;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

		horizontal = Input.GetAxis("Horizontal");
		vertical = Input.GetAxis("Vertical");

		//Debug.Log (horizontal);

		if (horizontal == 0 && vertical == 0) {
			walking = false;
		} else
			walking = true;

		anim.SetBool ("Walking", walking);
	}
}
