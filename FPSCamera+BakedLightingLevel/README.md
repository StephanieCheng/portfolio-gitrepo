* Mouse and keyboard controls:  
    * wasd - move  
    * mouse - move camera  
    * ctrl - zoom  
    * space - jump 
    * left click - interact (open door)



### Simple Level

![gif1](https://imgur.com/w5KNJzT.gif)

### Camera Bobbing + light Bobbing

![gif1](https://imgur.com/pATTqAP.gif)