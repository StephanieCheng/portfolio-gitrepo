﻿using UnityEngine;
using System.Collections;

public class BSplineFollow : MonoBehaviour {

	//public GameObject item; //the objects that follows the spline

	public BSplineDraw bsd; 
	public float moveSpeed;

	private Quaternion initRot;
	private float t;
	private float u;
	private float uNext;
	private Vector3 pNext;

	private int brTetiva;
	private int brSegments;
	private float splineLength;


	private Vector3 s;
	private Vector3 e;
	private Vector3 os;
	private float angle;
	private Vector3 norm;
	private Vector3 binorm;


	private float timewait;
	// Use this for initialization
	void Start () {
		//initial position = 000
		//inittial rotation  = 000
		initRot = this.transform.rotation;

		//position the item at the start of the spline

		//rotate before translating
		//os = s x e

		s = Vector3.forward; //z+
		e = bsd.pDer (0, 1); //t = 0, first segment

		os = RotAxis (s, e);

		//cos of angle = s*e /|s||e|

		angle = Mathf.Acos(Vector3.Dot (s, e) / (s.magnitude * e.magnitude)) * Mathf.Rad2Deg;
		//Debug.Log (angle);

		this.transform.RotateAround (transform.position, os, angle);

		this.transform.position = bsd.p (0, 1);


		brTetiva = bsd.getSegments();
		brSegments = bsd.noSegments ();

		brTetiva *= brSegments;

		timewait = 0;

		splineLength = bsd.SplineLength ();

		u = 0;
		uNext = 1.0f / brTetiva;
		pNext = bsd.p(uNext, 1);
		t = 0;

		//StartCoroutine("AnimateFollow");
	}
	
	// Update is called once per frame
	void Update () {

		//duljina krivulje

		//must wait for move speed
		if (timewait <= 0) { //timewait <= 0 transform.position == pNext
			//get next u
			// 0 =< u =< brSegments
			// t = u- floor(u);

			u = uNext;

			uNext += 1.0f / brTetiva;
			if (uNext > brSegments){
				uNext = 0;
			}
			int segNext = (int) Mathf.Floor(uNext);
			float tNext = uNext - segNext;


			int segNo = (int) Mathf.Floor(u); //segNo+1 = segment number
			t = u - segNo;
			
			//Debug.Log (u);

			transform.position = Vector3.zero;
			transform.rotation = initRot;

			//rotate

			s = Vector3.forward; //z+
			e = bsd.pDer (t, segNo+1); 
			
			os = RotAxis (s, e);

		
			//cos of angle = s*e /|s||e|
			
			angle = Mathf.Acos(Vector3.Dot (s, e) / (s.magnitude * e.magnitude)) * Mathf.Rad2Deg;
			//Debug.Log (angle);

			//get normal and binormal

			
			this.transform.RotateAround (transform.position, os, angle);

			//translate

			transform.position = bsd.p (t, segNo+1);
			pNext = bsd.p(tNext, segNext+1);
			//Debug.Log(Vector3.Distance(transform.position, pNext));

			//determen waiting time for the next dot;

			timewait = (Vector3.Distance(transform.position, pNext)/ splineLength) * moveSpeed; //seconds till next movement
			//Debug.Log(timewait);
			//timewait = moveSpeed * Time.deltaTime;
		} else {
			timewait -= Time.deltaTime;
			//transform.position = Vector3.MoveTowards(transform.position, pNext, Time.deltaTime * moveSpeed);

		}
	
	}

//	IEnumerator AnimateFollow(){
//		while (true) {
//		
//			//get next u
//			// 0 =< u =< brSegments
//			// t = u- floor(u);
//			
//			u = uNext;
//			
//			uNext += 1.0f / brTetiva;
//			if (uNext > brSegments){
//				uNext = 0;
//			}
//			int segNext = (int) Mathf.Floor(uNext);
//			float tNext = uNext - segNext;
//			
//			
//			int segNo = (int) Mathf.Floor(u); //segNo+1 = segment number
//			t = u - segNo;
//			
//			//Debug.Log (u);
//			
//			//transform.position = Vector3.zero;
//			
//			//rotate
//			//translate
//			
//			transform.position = bsd.p (t, segNo+1);
//			Vector3 pNext = bsd.p(tNext, segNext+1);
//			
//			
//			//determen waiting time for the next dot;
//			
//			timewait = (Vector3.Distance(transform.position, pNext)/ splineLength);
//			//timewait = moveSpeed * 0.01f;
//
//
//			
//			yield return new WaitForSeconds (moveSpeed *  timewait);
//		}
//	}

	Vector3 RotAxis(Vector3 s, Vector3 e){
		return new Vector3 (s.y*e.z - e.y*s.z, 0-(s.x*e.z - e.x*s.z), s.x*e.y -s.y*e.x);
	}
}
