﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class BSplineDraw : MonoBehaviour {

	public float pointRad;
	public Vector3[] r; //control points
	public GameObject[] RGame;
	public Camera cam;

	public int Segments;

	//private float[][] Bi3 = new float[4][];// {{-1, 3, -3, 1},{3, -6, 3, 0},{-3, 0, 3, 0},{1, 4, 1, 0}};
	private float[][] Bi3 = new float[4][]{
		new float[4]{-1, 3, -3, 1},
		new float[4]{3, -6, 3, 0},
		new float[4]{-3, 0, 3, 0},
		new float[4]{1, 4, 1, 0}
	};


	private int k = 3;
	//private int s, m, n;

	private Vector3[] points;

	private Vector3[] tangents;

	// Use this for initialization
	void Start () {
//		k = 3; // segmenti krivulje
//		//n = r.Length - 1; // n+1 = br kontrolnih tocaka
//		//m = n + k + 1; // m+1 = br vrijednosti u vektoru uzlova
//
//		//s = n - k + 1; // br segmenata
//		
//		//Debug.Log (r.Length);
//		RGame = new GameObject[r.Length];
//
//		for (int i = 0; i< r.Length; i++) {
//			RGame[i] = new GameObject();
//			RGame[i].transform.position = r[i];
//		}

	}
	
	// Update is called once per frame
	void Update () {


//		if (Application.isPlaying) {
//			return;
//		}


		if (!Application.isPlaying) {
	
			if (r.Length - RGame.Length < 0) {
				GameObject[] tmp = new GameObject[r.Length];
				for (int i = 0; i <  RGame.Length - r.Length; i++) {
					DestroyImmediate (RGame [RGame.Length - 1 - i]);
				}
				for (int i = 0; i <  r.Length; i++) {
					tmp[i] = RGame[i];
				}

				RGame = tmp;
			} else if (r.Length - RGame.Length > 0) {
				GameObject[] tmp = new GameObject[r.Length];
				for (int i = 0; i <  RGame.Length; i++) {
					tmp[i] = RGame[i];
				}
				for(int i = r.Length - RGame.Length - 1; i >= 0; i--){
					//Debug.Log(r.Length -1 -i);
					tmp[r.Length - 1 - i] = new GameObject();

					tmp[r.Length - 1 - i].transform.position = r[r.Length - 1 - i];
				}
				RGame = tmp;
			}


			for (int i = 0; i< r.Length; i++) {
				//RGame[i].transform.position = r[i];
				r[i] = RGame[i].transform.position;
			}
		}

		points = new Vector3[Segments * (RGame.Length - k)];
		tangents = new Vector3[Segments * (RGame.Length - k)];

		//drawrin da curb
		Vector3 point;
		Vector3 tangent;
		//Debug.Log (RGame.Length - k);
		for (int i = 1; i <=( RGame.Length - k); i++) { //od 1 do br segmenata
			float t = 0;

			for(int j = 0; j < Segments; j++){ //svaki segment ima segmenta koje iscrtavamo
				point = p(t,i);
				tangent = pDer(t,i);
				t += (1.0f/Segments);
				//Debug.Log(t);

				points[(i-1)*Segments + j ] = point;
				tangents[(i-1)*Segments + j ] = tangent;
				//Debug.Log(point);
			}

		} 

		//actul draw curb
		//		for (int i = 0; i < (points.Length-1); i++) {
		//			if(i <= Segments)
		//				Debug.DrawLine(points[i], points[i+1], Color.white);
		//			if(Segments < i && i <= Segments*2)
		//				Debug.DrawLine(points[i], points[i+1], Color.blue);
		//			if(i > Segments*2)
		//				Debug.DrawLine(points[i], points[i+1], Color.red);
		//		}
		
		for (int i = 0; i < (points.Length-1); i++) {
			Debug.DrawLine(points[i], points[i+1], Color.white);
		}
		
		//20
		//Vector3 tangent = pDer (20 * 1.0f / Segments, 1); 
		//Debug.DrawLine (Vector3.zero, tangent , Color.blue);
		//Debug.DrawLine (points [20], points [20] + Vector3.up, Color.blue);
		for (int i = 0; i < points.Length; i++) {
			Debug.DrawLine (points[i] , tangents[i] + points[i], Color.red);
		}
		//Debug.DrawLine (points[20] , tangents[20] + points[20], Color.red);
		
		for (int i = 0; i < r.Length-1; i++) {
			Debug.DrawLine(r[i], r[i+1], Color.grey);
		}
	
	}
	

	public void DrawGLLines(){
		if (Application.isPlaying) {
			GL.LoadIdentity();
			GL.LoadProjectionMatrix(cam.projectionMatrix);
			GL.modelview = cam.worldToCameraMatrix;
			GL.PushMatrix();
			GL.Begin(GL.LINES);
			for (int i = 0; i < (points.Length-1); i++) {
				GL.Color(Color.white);
				GL.Vertex(points[i]);
				GL.Vertex(points[i+1]);
			}
			GL.End();
			GL.PopMatrix();
			
		}

	}


	private float[]  Tmatrix(float t){// 0<= t < 1

		float[] T3 = {t*t*t, t*t, t, 1f};
		return T3;

	}

	public Vector3 p(float t, int i){
		float[][] t3 = new float[1][];
		t3[0] = Tmatrix (t);

		float[][] Ri = GetRmatrix (i);


		float[][] result = MatrixMulti (t3, Bi3);

		//Debug.Log (result.Length);
		for (int j = 0; j < result.Length; j++){
			//Debug.Log(result[j][0] + " " + result[j][1] + " " + result[j][2] + " " + result[j][3]);
			for (int k = 0; k < result[0].Length; k++){
				result[j][k] *= (1.0f/6.0f);
			}
		}

		result = MatrixMulti (result, Ri);
//		Debug.Log (result.Length);
//		Debug.Log (result [0].Length);
//		for (int j = 0; j < result.Length; j++){
//			Debug.Log(result[j][0] + " " + result[j][1] + " " + result[j][2]);
//
//		}


		return new Vector3(result[0][0], result[0][1], result[0][2]);
	}


	public Vector3 pDer(float t, int i){
		float[][] t3d = new float[1][];
		t3d [0] = new float[]{t * t, t, 1};

		float[][] Bi3d = new float[3][];
		Bi3d [0] = new float[4]{-1, 3, -3, 1};
		Bi3d [1] = new float[4]{2, -4, 2, 0};
		Bi3d [2] = new float[4]{-1 , 0, 1, 0};


		float[][] Ri = GetRmatrix (i);

		float[][] result = MatrixMulti (t3d, Bi3d);

		for (int j = 0; j < result.Length; j++){
			for (int k = 0; k < result[0].Length; k++){
				result[j][k] *= (1.0f/2.0f);
			}
		}

		result = MatrixMulti (result, Ri);

		return new Vector3(result[0][0], result[0][1], result[0][2]); 

	}

	public Vector3 pDer2(float t, int i){
		float[][] t3d2 = new float[1][];
		t3d2 [0] = new float[]{t, 1};

		float[][] Bi3d2 = new float[2][];
		Bi3d2[0] = new float[4]{-1, 3, -3, 1};
		Bi3d2[1] = new float[4]{1, -2, 1, 0};

		float[][] Ri = GetRmatrix (i);

		float[][] result = MatrixMulti (t3d2, Bi3d2);


		result = MatrixMulti (result, Ri);
		return new Vector3(result[0][0], result[0][1], result[0][2]); 
	}


	private float[][] GetRmatrix(int i){
		float[][] Ri = new float[4][];
		Ri [0] = new float[3]{r[i - 1].x, r[i - 1].y, r[i - 1].z};
		Ri [1] = new float[3]{r [i].x, r [i].y, r [i].z};
		Ri [2] = new float[3]{r [i + 1].x, r [i + 1].y, r [i + 1].z};
		Ri [3] = new float[3]{r [i + 2].x, r [i + 2].y, r [i + 2].z};
		return Ri;
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.yellow;


		for (int i = 1; i < RGame.Length; i++){
			Gizmos.DrawSphere(RGame[i].transform.position, pointRad);
		}
		Gizmos.color = Color.green;
		Gizmos.DrawSphere(RGame[0].transform.position, pointRad);

		Gizmos.color = Color.red;
		Gizmos.DrawSphere(RGame[RGame.Length-1].transform.position, pointRad);

		Gizmos.color = Color.magenta;

//		for (int i = 0; i < points.Length; i++) {
//			Gizmos.DrawSphere(points[i], pointRad/2);
//		}

	}


	public static float[][] MatrixMulti(float[][] _a, float[][] b)
	{
		int n = _a.Length; //N
		int m = b[0].Length;//M
		int l = _a[0].Length;

//		Debug.Log (n);
//		Debug.Log (m);
//		Debug.Log (l);
//		Debug.Log (b[0].Length);
		if (l != b.Length) {
			Debug.Log("Matrices not multipliable");
			return null;
		}
			//throw new ArgumentException("Illegal matrix dimensions for multiplication. _a.M must be equal b.N");
		float[][] result = new float[n][]; //a.n, b.m
		for (int i = 0; i < n; i++) {
			result[i] = new float[m];
			for (int j = 0; j < m; j++)
			{
				float sum = 0.0f;
				for (int k = 0; k < l; k++)
					sum += _a[i][k]*b[k][j];
				result[i][j] = sum;
			}
		}
		return result;
	}

	public Vector3[] getPoints(){
		return points;
	}
	public Vector3[] getTangents(){
		return tangents;
	}
	public int getSegments(){
		return Segments;
	}
	public int noSegments(){
		return (RGame.Length - k);
	}

	public float SplineLength(){
		float d = 0;
		for (int i = 0; i < r.Length - 1; i++) {
			d += Vector3.Distance(r[i], r[i+1]);
		}
		return d;
	}
}
