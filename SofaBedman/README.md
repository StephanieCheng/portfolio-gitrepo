# Sofa Bedman  
sofa bedman is a strategy role playing game made by 2 students in 3 days in Unity 

## Download link: https://gitlab.com/Dood/sofabedman/repository/archive.zip

<br><br>
## controls via mouse right click  
<br>
## selection  
click on object on the map to open menus for them  
player characters can move and attack  
furniture can be possesed into becoming playable characters  
the blue pawn is the player character that can move and attack at the strat of the game  
the enemy characters cannot be controlled, they will attack you when you and your turn  
<br>
## moving & attacking
selecting the move option will highlight panels that the character can move to, clicking on a blue panel will move the character and use up the move ability for that turn  

selecting the attack option will highlight panels that the caracter can attack, clicking on an entity on those panels will deal damage
pressing outside of the blue panels will cancel the action
<br>
## ending turn
pressing end turn will start the enemy turn, enemies will move and attack you if your in their range
<br>
## notes
this game HAS NOT BEEN TESTED on framerates below 60FPS, in case of lower fraerates certain bugs might occur
no end game state or win state has been imlemented yet, once the blue pawn dies that should be game over and killing all the enemies should be a win state

