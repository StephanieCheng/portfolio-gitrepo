﻿using UnityEngine;
using System;
using System.Collections;

public class Node {


	private int heurtistic;
	
	private Vector2 pos;
	private Node parent;
	private int cost;

	public Node(int h, Node p, int c, Vector2 po){
		heurtistic = h;
		parent = p;
		cost = c;
		pos = po;
	}
	public Node(){
		heurtistic = 0;
		parent = null;
		cost = 0;
	}



	public int Heurtistic {
		get {
			return heurtistic;
		}
		set {
			heurtistic = value;
		}
	}

	public Node Parent {
		get {
			return parent;
		}
		set {
			parent = value;
		}
	}


	public int Cost {
		get {
			return cost;
		}
		set {
			cost = value;
		}
	}

	public Vector2 Pos {
		get {
			return pos;
		}
		set {
			pos = value;
		}
	}

	public override bool Equals( System.Object obj )
	{
		if (obj == null)
			return false;
		Node c = obj as Node ;
		if ((System.Object)c == null)
			return false;
		return pos == c.Pos;        
	}

	public bool Equals(Node c)
	{
		if ((object)c == null)
			return false;
		return c.Pos == pos;
	}
}
