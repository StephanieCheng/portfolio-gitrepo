﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PassiveHeal : PassiveSkill {

	int radius;
	int healPower;

	private CollisionMap map;

	// Use this for initialization
	void Start () {
		map = GameObject.FindWithTag("Map").GetComponent<CollisionMap>();

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Execute(){
		Debug.Log("executing passive healing");

		List<Pawn> targets = FindAllFriendlyInRadius (radius);

		Debug.Log (string.Format ("found {0} pawns to heal", targets.Count));

		for (int i = 0; i < targets.Count; i++) {
			map.DealDamage(-healPower, targets[i]);
		}


	}

	public int Radius {
		get {
			return radius;
		}
		set {
			radius = value;
		}
	}

	public int HealPower {
		get {
			return healPower;
		}
		set {
			healPower = value;
		}
	}

	private List<Pawn> FindAllFriendlyInRadius(int depth){

		List<Pawn> pawns = new List<Pawn> ();

		List<Vector2> open = new List<Vector2>();
		List<Vector2> closed = new List<Vector2>();

		Vector2 start = new Vector2 (transform.position.x, transform.position.z);
		open.AddRange (GetNeighboursNotWall(start));
		closed.Add (start);

		FindAllFriendlyRecursion (depth, pawns, open, closed);

		return pawns;
	}

	private void FindAllFriendlyRecursion(int depth, List<Pawn> pawns,List<Vector2> open, List<Vector2> closed){
		if (depth == 0)
			return;

		int count = open.Count;

		for (int i = 0; i < count; i++) {

			Vector2 cur = open[0];
			open.RemoveAt(0);
			closed.Add(cur);



			// only raycast celltype pawn

			if (map.GetColMapAt((int)cur.x, (int)cur.y) == CollisionMap.CellType.Pawn){
				Vector3 raycastStart = new Vector3 (cur.x , 30, cur.y);

				Debug.Log(cur);

				RaycastHit hit;
				Ray ray = new Ray(raycastStart, -Vector3.up);
				
				if (Physics.Raycast (ray, out hit)) {
					if (hit.transform.tag == "Pawn")
					{
						Pawn pawnScript = hit.transform.GetComponent<Pawn>();
						pawns.Add(pawnScript);
						Debug.Log("healing pending for pawn");
					}
				}

			}


			List<Vector2> neighbours = GetNeighboursNotWall(cur);

			for(int  j = 0; j < neighbours.Count; j++){

				if (!open.Contains(neighbours[j]) && !closed.Contains(neighbours[j])) 
					open.Add(neighbours[j]);

			}

		}

		FindAllFriendlyRecursion (depth - 1, pawns, open, closed);


		
		
	


	}

	private List<Vector2> GetNeighboursNotWall(Vector2 pos){
		List<Vector2> neighbours = new List<Vector2>();
				
		
		if (map.GetColMapAt((int)pos.x + 1, (int)pos.y) != CollisionMap.CellType.Wall)
			neighbours.Add(new Vector2(pos.x + 1, pos.y));
		
		if (map.GetColMapAt((int)pos.x - 1, (int)pos.y) != CollisionMap.CellType.Wall)
			neighbours.Add(new Vector2(pos.x - 1, pos.y));
		
		if (map.GetColMapAt((int)pos.x, (int)pos.y + 1) != CollisionMap.CellType.Wall)
			neighbours.Add(new Vector2(pos.x, pos.y + 1));
		
		if (map.GetColMapAt((int)pos.x, (int)pos.y - 1) != CollisionMap.CellType.Wall)
			neighbours.Add(new Vector2(pos.x, pos.y - 1));
		
		
		
		return neighbours;
	}
}
