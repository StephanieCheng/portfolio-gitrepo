﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

	public CollisionMap map;

    public Transform selectedCharacter;

    public GameObject PawnPanel;
    public GameObject PossesablePanel;
    public GameObject StatsPanel;
	public GameObject TurnEndPanel;

    public GameObject PossetionSpellsPanel;

    public GameObject ButtonPrefab;

    public GameObject[] PossetionSpells;
    private List<GameObject> PossetionButtons;

	//should be private
    public Text Stat_Name;
    public Text Stat_Move;
    public Text Stat_Attack;
    public Slider HPBar;
	private Text HPtext;


	private Button MoveButton;
	private Button AttackButton;
	private Button End_Turn;

    private Pawn charScipt;


	private Text EndTurnText;
	private Text NoOfSouls;

	// Use this for initialization
	void Start () {

		PawnPanel.SetActive(true);
		PossesablePanel.SetActive(true);
		StatsPanel.SetActive(true);
		TurnEndPanel.SetActive (true);
        

        PossetionButtons = new List<GameObject>();
        for (int i = 0; i < PossetionSpells.Length; i++) {
            GameObject button;

			button = Instantiate(PossetionSpells[i], new Vector3(0,-40*(i+1),0), Quaternion.identity) as GameObject;

			if(!button){
				Debug.Log("No button made");
			}

            button.transform.SetParent(PossetionSpellsPanel.transform, false);
		

			Button buttonButton = button.GetComponent<Button>();
			PossesSpell buttonscr = button.GetComponent<PossesSpell>();
			buttonButton.onClick.AddListener(() => map.Posses(buttonscr));

            PossetionButtons.Add(button);
            button.SetActive(true);
        }

		MoveButton = GameObject.Find("MoveButton").GetComponent<Button>();
		AttackButton = GameObject.Find("AttackButton").GetComponent<Button>();
		End_Turn = GameObject.Find("EndTurn").GetComponent<Button>();


		HPtext = GameObject.Find ("HPText").GetComponent<Text> ();


		//TurnEndPanel = GameObject.Find ("TurnEndPanel");
		EndTurnText = TurnEndPanel.GetComponentInChildren<Text> ();



		TurnEndPanel.SetActive (false);
		PawnPanel.SetActive(false);
		PossesablePanel.SetActive(false);
		StatsPanel.SetActive(false);
        
    }
	
	// Update is called once per frame
	void Update () {

      
	
	}

    public void SetActiveCharacter(Transform ch) {
        selectedCharacter = ch;
        StatsPanel.SetActive(true);
        PawnPanel.SetActive(false);
        PossesablePanel.SetActive(false);
        charScipt = selectedCharacter.GetComponent<Pawn>();

        if (selectedCharacter.tag == "Pawn") {
            PawnPanel.SetActive(true);
            StatsPanel.SetActive(true);

			if(charScipt.GetMoveAction() > 0){
				MoveButton.interactable = true;
			}else{
				MoveButton.interactable = false;
			}

			if(charScipt.GetAttackAction() > 0){
				AttackButton.interactable = true;
			}else{
				AttackButton.interactable = false;
			}
        }
        else if (selectedCharacter.tag == "Possesable") {
            PossesablePanel.SetActive(true);
            StatsPanel.SetActive(true);
        }            
        else if (selectedCharacter.tag == "Enemy") {
            StatsPanel.SetActive(true);
        }

        Stat_Name.text = selectedCharacter.name;
        Stat_Move.text = charScipt.move.ToString();
        Stat_Attack.text = charScipt.attack.ToString();

        HPBar.maxValue = charScipt.HP;
        HPBar.value = charScipt.currentHP;
		HPtext.text = string.Format ("{0}/{1}", charScipt.currentHP, charScipt.HP);
    }

    public void UpdateAvailablePossetionSpells(string Name) {
        
    }

	public void SetMoveButtonInteractivity(bool i){
		MoveButton.interactable = i;
	}
	public void SetAttackButtonInteractivity(bool i){
		AttackButton.interactable = i;
	}

	public void RemoveHud(){
		StatsPanel.SetActive(false);
		PawnPanel.SetActive (false);
		PossesablePanel.SetActive(false);
		return;
	}

	public void EndTurn(string turn){
		StatsPanel.SetActive(false);
		PawnPanel.SetActive(false);
		PossesablePanel.SetActive(false);
		selectedCharacter = null;
		charScipt = null;

		EndTurnText.text = turn;

		StopCoroutine ("EndTurnAnimation");
		StartCoroutine ("EndTurnAnimation");
	}

	IEnumerator EndTurnAnimation(){

		//replace with animation

		TurnEndPanel.SetActive (true);

		yield return new WaitForSeconds (1);

		TurnEndPanel.SetActive (false);

		MoveButton.interactable = true;
		AttackButton.interactable = true;

	}

	private void TogglePossetionSpells(bool tog){
		if (PossetionButtons == null)
			return;
		foreach(GameObject pos in PossetionButtons){
			if(pos){
				Button but = pos.GetComponent<Button>();
				but.interactable = tog;
			}

		}
	}

	public void SetSoulNo(int no){
		if (NoOfSouls) {
			NoOfSouls.text = no.ToString ();
			if (no <= 0) {
				TogglePossetionSpells (false);
			}
			else{
				TogglePossetionSpells(true);
			}
		} else {
			NoOfSouls = GameObject.Find("No.Souls").GetComponent<Text>();
			if(NoOfSouls)
				SetSoulNo(no);
		}
			


	}
}
