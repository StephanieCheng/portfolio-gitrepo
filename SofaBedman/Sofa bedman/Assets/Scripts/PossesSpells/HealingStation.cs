﻿using UnityEngine;
using System.Collections;

public class HealingStation : PossesSpell {

	private int healPower = 2;
	private int radius = 3;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reset(){
		MoveBonus = -5;
		AttackBonus = -1;
		hpBonus = -3;
		spellName = "Healing Station";
		description = string.Format( "give a pawn ability: heal all friendly pawns in a radius of %d for %dhp at the end of turn", radius, healPower );
	}

	public void HealStation1(){
		//get every1 in a radius and heal healpower hp

	}

	public override void Posses(){
		base.Posses ();
		Debug.Log("new posses called");
		PassiveHeal healScr = possesedPawn.gameObject.AddComponent<PassiveHeal> ();
		healScr.Radius = radius;
		healScr.HealPower = healPower;
		possesedPawn.PassiveSkills.Add (healScr);
	} 
}
