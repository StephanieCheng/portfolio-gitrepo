﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class PossesSpell : MonoBehaviour {


    public string spellName = "Posses";
	public int cost = 1;

    public int attackBonus = 0;
    public int moveBonus = 0;
	public string description = "Turn an object into a pawn";

	public int hpBonus = 0;

	protected Pawn possesedPawn;
	



	// Use this for initialization
	void Start () {


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public virtual void Posses(){
		if (!possesedPawn) {
			Debug.Log("spell tried to posses but has no pawn");
			return;
		}
		Debug.Log("original posses called");
		possesedPawn.attack += attackBonus;
		possesedPawn.move += moveBonus;
		if (possesedPawn.move <= 0)
			possesedPawn.move = 1;
		possesedPawn.HP += hpBonus;
		if (possesedPawn.HP <= 0)
			possesedPawn.HP = 1;
		possesedPawn.currentHP = possesedPawn.HP;

	}

	public int Cost {
		get {
			return cost;
		}
		set {
			cost = value;
		}
	}

	public string SpellName {
		get {
			return spellName;
		}
		set {
			spellName = value;
		}
	}

	public int AttackBonus {
		get {
			return attackBonus;
		}
		set {
			attackBonus = value;
		}
	}

	public int MoveBonus {
		get {
			return moveBonus;
		}
		set {
			moveBonus = value;
		}
	}

	public string Description {
		get {
			return description;
		}
		set {
			description = value;
		}
	}

	public int HPbonus {
		get {
			return hpBonus;
		}
		set {
			hpBonus = value;
		}
	}

	public Pawn PossesedPawn {
		get {
			return possesedPawn;
		}
		set {
			possesedPawn = value;
		}
	}
}
