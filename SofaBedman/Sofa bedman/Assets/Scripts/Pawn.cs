﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pawn : MonoBehaviour {
    //selectable and moveable

    public int move = 3;
    public int level = 1;
    public int attack = 3;
    public int HP = 6;
    public int currentHP = 6;


    private Collider col;

	private int moveAction = 1; //how many times can a pawn move in a turn
	private int attackAction = 1; //how many times can a pawn atatck/use a skill in a turn


	private List<PassiveSkill> passiveSkills = new List<PassiveSkill>();


    // Use this for initialization
    void Start() {

        col = GetComponent<Collider>() as Collider;
		currentHP = HP;

    }

    // Update is called once per frame
    void Update() {
        
    }

    public int GetMove() {
        return move;
    }

    public Vector2 GetPos() {
        return new Vector2(transform.localPosition.x, transform.localPosition.z);
    }

    

    //void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawSphere(transform.position, move);
    //}
	public int GetMoveAction(){
		return moveAction;
	}
	public void SetMoveAction(int m){
		moveAction = m;
	}
	public int GetAttackAction(){
		return attackAction;
	}
	public void SetAttackAction(int a){
		attackAction = a;
	}

	public void ResetActions(){
		moveAction = 1;
		attackAction = 1;

		//if special stuff then increase move and attack
	}
    

	public List<PassiveSkill> PassiveSkills {
		get {
			return passiveSkills;
		}
		set {
			passiveSkills = value;
		}
	}
}
