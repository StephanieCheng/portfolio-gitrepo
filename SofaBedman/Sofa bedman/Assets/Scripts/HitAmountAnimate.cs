﻿using UnityEngine;
using System;

public class HitAmountAnimate : MonoBehaviour {

    public float TTL = 0.7f;
    public float FadeDelay = 0.45f;
    public float RiseSpeed = 1.0f;

    public Color ColorNeutral;
    public Color ColorPositive;
    public Color ColorNegative;

    public Camera LevelCamera;

    private float FadeDuration = 1.0f;

    private TextMesh DisplayText;
    private Color Color;

    private bool animating = false;

    // Use this for initialization
    void Start () {
        DisplayText.color = Color.clear;
    }

    public void StartAnimation() {
        
        FadeDuration = TTL - FadeDelay;

        DisplayText = GetComponent<TextMesh>();
        int amount = Int32.Parse(DisplayText.text);
        Debug.Log(amount);
        if (amount > 0)
            Color = ColorPositive;
        else if (amount < 0)
            Color = ColorNegative;
        else
            Color = ColorNeutral;

        animating = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (animating)
        {
            //Debug.Log(Color.a);
            if (TTL <= 0.0f)
                Destroy(gameObject);

            if (FadeDelay <= 0.0f)
                Color.a -= Time.deltaTime / (FadeDuration);
            else
                FadeDelay -= Time.deltaTime;

            TTL -= Time.deltaTime;
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + Time.deltaTime * RiseSpeed, transform.localPosition.z);
            DisplayText.color = Color;
        }

        transform.LookAt(LevelCamera.transform.localPosition, LevelCamera.transform.up);
        transform.Rotate(0.0f, 180.0f, 0.0f);
	}
}
