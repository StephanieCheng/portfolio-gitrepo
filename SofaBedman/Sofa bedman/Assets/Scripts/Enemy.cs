﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
//using System.Collections.SortedList;

public class Enemy : Pawn {

	private CollisionMap map;
	private bool active;
	private int turnID;
	private List<GameObject> pawns;
	public int recursionDepth;

	// Use this for initialization
	void Start () {

		map = GameObject.FindGameObjectWithTag("Map").GetComponent<CollisionMap>();
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

	private void AI(){
		//enemy AI
		
		//move 
		//move thoward the player
		
		//iteare through list, get all locations and find the nearest one
		List<Node> path = EnemyFieldSearch ();

		if (path == null) {
			return;
		}

		map.UpdateMap ((int)transform.position.x, (int)transform.position.z, CollisionMap.CellType.Empty);

		//move 1 by 1 should be animated
		//TODO animate moving
		MoveAlongPath (path, move);

		map.UpdateMap ((int)transform.position.x, (int)transform.position.z, CollisionMap.CellType.Pawn);

		//Debug.Log ("attacking");
		//attack 
		//raycast for player pawns
		List<Vector3> raycastStarts = new List<Vector3> ();
		raycastStarts.Add (new Vector3 (transform.position.x + 1, 30, transform.position.z));
		raycastStarts.Add (new Vector3 (transform.position.x - 1, 30, transform.position.z));
		raycastStarts.Add (new Vector3 (transform.position.x, 30, transform.position.z + 1));
		raycastStarts.Add (new Vector3 (transform.position.x, 30, transform.position.z - 1));


		for (int i = 0; i < 4; i++) {
			RaycastHit hit;
			Ray ray = new Ray(raycastStarts[i], -Vector3.up);

			Debug.DrawRay(raycastStarts[i], -Vector3.up, Color.red);
			if (Physics.Raycast (ray, out hit)) {
				if (hit.transform.tag == "Pawn")
				{
					//Debug.Log("found who to attack");
					Pawn pawnScript = hit.transform.GetComponent<Pawn>();
					map.DealDamage(attack, pawnScript);
					return;
				}
			}

		}




	}

	public void ActivateEnemy(int id, List<GameObject> pawnlist){

		Debug.Log ("Activated: " + this.name);

		//turned off enemy AI temp
		//active = true; // used for Update, not needed
		turnID = id;
		pawns = pawnlist;


		StartCoroutine ("AICoroutine", id);


	}

	private void EndAction(){
		active = false;
		map.ActivateEnemy (turnID + 1);
	}

	IEnumerator AICoroutine(int id){


		yield return new WaitForSeconds (0.5f);

		AI ();

		//Debug.Log("next enemy");
		active = false;

		map.ActivateEnemy (id + 1);
	}

	private int CalcHeuristic(Vector2 pos, Vector2 goal){
		return 1;
	}

	private List<Node> EnemyFieldSearch(){
		//find the closest pawn and the path to him

		Vector2 startPos = new Vector2 (transform.position.x, transform.position.z);

		SortedList<int, Node> open = new SortedList<int,Node>(new DuplicateKeyComparer<int>());
		List<Node> closed = new List<Node>();

		Node start = new Node (-1, null, 0, startPos); //not using heuristic


		open.Add (0,start);


		Node goal = EnemyFieldSearchRecursion (open, closed);

		if(goal == null){
			Debug.Log("no1 was found");

			return null;
		}

		//experimental -- once the enemy finds u it chases u
		recursionDepth = 8; 

		//Debug.Log("goal found:");
		//Debug.Log (goal.Pos);



		List<Node> path = ReconstructPath (goal);

		return path;


	}

	private Node EnemyFieldSearchRecursion(SortedList<int,Node> open, List<Node> closed){

		if (open.Count == 0) {
			return null;
		}
		Node cur = open.Values[0];
		open.RemoveAt (0);
		closed.Add (cur);

		//Debug.Log("current node:");
		//Debug.Log(cur.Pos);

		List<Node> neighbours = GetNodeNeighbours (cur);


		if(cur.Cost < recursionDepth)
			for(int i = 0; i < neighbours.Count; i++){


				if(!open.ContainsValue(neighbours[i]) && !closed.Contains(neighbours[i])){
					CollisionMap.CellType whatsOnTheCell = map.GetColMapAt((int)neighbours[i].Pos.x, (int)neighbours[i].Pos.y);
					if(whatsOnTheCell == CollisionMap.CellType.Pawn){

						//what if pawn is enemy?
						//TODO pawn can only be playerpawn
						RaycastHit hit;
						Ray ray = new Ray(new Vector3(neighbours[i].Pos.x, 30 ,neighbours[i].Pos.y), -Vector3.up);
						if (Physics.Raycast (ray, out hit)) {
							if (hit.transform.tag == "Pawn")
							{
								////found it!
								return neighbours[i];
							}
						}



					} //else it can only be a wall

					//if neighbour has a pawn on it
					//u have found the closest pawn
					
					//else add it to the open if not wall
					open.Add(cur.Cost+1,neighbours[i]);


				}

			}

		Node goal = EnemyFieldSearchRecursion (open, closed);

		return goal;
	}

	private List<Node> GetNodeNeighbours(Node n){
		List<Node> neighbours = new List<Node> ();
		
		if (map.GetColMapAt ((int)n.Pos.x+1,(int) n.Pos.y) != CollisionMap.CellType.Wall) { //pawn or empty
			//pos, heuristics, cost and parent
			Node newnode = new Node(-1, n, n.Cost+1, new Vector2((int)n.Pos.x+1,(int) n.Pos.y));
			neighbours.Add(newnode);
		}
		if (map.GetColMapAt ((int)n.Pos.x-1,(int) n.Pos.y) != CollisionMap.CellType.Wall) { //pawn or empty
			//pos, heuristics, cost and parent
			Node newnode = new Node(-1, n, n.Cost+1, new Vector2((int)n.Pos.x-1,(int) n.Pos.y));
			neighbours.Add(newnode);
		}

		if (map.GetColMapAt ((int)n.Pos.x,(int) n.Pos.y+1) != CollisionMap.CellType.Wall) { //pawn or empty
			//pos, heuristics, cost and parent
			Node newnode = new Node(-1, n, n.Cost+1, new Vector2((int)n.Pos.x, (int)n.Pos.y+1));
			neighbours.Add(newnode);
		}

		if (map.GetColMapAt ((int)n.Pos.x,(int) n.Pos.y-1) != CollisionMap.CellType.Wall) { //pawn or empty
			//pos, heuristics, cost and parent
			Node newnode = new Node(-1, n, n.Cost+1, new Vector2((int)n.Pos.x,(int) n.Pos.y-1));
			neighbours.Add(newnode);
		}
		
		return neighbours;
	}

	private List<Node> ReconstructPath(Node end){
		Node start = end;
		List<Node> path = new List<Node> ();

		//Debug.Log ("reconstructing");
		//do not put in the goal end
		

		while (start.Parent != null) {
			start = start.Parent;
			//Debug.Log(start.Pos);
			path.Insert(0,start);
		}
		//path.Insert(0, start); // one node left

		//Debug.Log("path reconstructed, start:");
		//Debug.Log (path [0].Pos);

		//remove the start aswell
		path.RemoveAt (0);

		return path;

	}

	private void MoveAlongPath(List<Node> path, int moveleft){
		if (path.Count == 0)
			return;

		if (moveleft == 0)
			return;

		//Debug.Log (moveleft);

		Node cur = path [0];
		path.RemoveAt (0);
		transform.position = new Vector3 (cur.Pos.x, transform.position.y, cur.Pos.y);

		MoveAlongPath (path, moveleft - 1);
	}


	/// <summary>
	/// Comparer for comparing two keys, handling equality as beeing greater
	/// Use this Comparer e.g. with SortedLists or SortedDictionaries, that don't allow duplicate keys
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	public class DuplicateKeyComparer<TKey>
		:
			IComparer<TKey> where TKey : IComparable
	{
		#region IComparer<TKey> Members
		
		public int Compare(TKey x, TKey y)
		{
			int result = x.CompareTo(y);
			
			if (result == 0)
				return 1;   // Handle equality as beeing greater
			else
				return result;
		}
		
		#endregion
	}
	


//	private void EnemyPathFinding(Vector3 closestPlayer){ //only for already known goal
//		Vector2 startPos = new Vector2 (transform.position.x, transform.position.z);
//
//		Vector2 goalPos = new Vector2 (closestPlayer.x, closestPlayer.z);
//		//cannot land on closest player but the algorithm works anyway
//
//		int startHeuristic = CalcHeuristic (startPos, goalPos);
//		
//		Node start = new Node (startHeuristic, null, 0, startPos);
//		Node goal = new Node (0, null, 0, goalPos);
//
//		SortedList<int,Node> open = new SortedList<int,Node>();
//		List<Node> closed = new List<Node>();
//
//		open.Add (startHeuristic,start);
//
//
//
//		EnemyPathFindingRecursion(open, closed, goal);
//
//		//goal can now backtrack to start
//
//	}
//
//	private void EnemyPathFindingRecursion(SortedList<int,Node> open, List<Node> closed, Node goal){
//
//		//path to goal impossible
//		if (open.Count == 0)
//			return;
//
//		int count = open.Count; //wont increase as we add stuff
//
//
//		//open set needs to look at nodes with lowest total score- - done with sorted list
//
//		for (int i = 0; i < count; i++){
//
//			Node current = open[0];
//			closed.Add (current);
//			open.RemoveAt(0);
//
//			List<Node> neighbours = GetNodeNeighbours(open[0]);
//
//			//found goal
//			if(neighbours.Contains(goal)){
//				Debug.Log("found goal");
//				goal.Parent = current;
//				return;
//			}
//
//			for (int j = 0; j < neighbours.Count; j++)
//			{
//				if(neighbours[j].Pos == goal.Pos){
//					goal.Parent = open[0];
//					goal.Cost = open[0].Cost + 1;
//					return;
//				}
//				
//				if (!open.ContainsValue(neighbours[j]) && !closed.Contains(neighbours[j])) {
//					//igonore certian neighbours
//					open.Add(neighbours[j].Heurtistic + current.Cost + 1, neighbours[j]);
//				}
//					
//			}
//
//
//		}
//
//
//		EnemyPathFindingRecursion (open, closed, goal);
//
//		return;
//
//
//	}
//
//	private List<Node> GetNodeNeighbours(Node n){
//		List<Node> neighbours = new List<Node> ();
//
//		if (map.GetColMapAt ((int)n.Pos.x,(int) n.Pos.y) == CollisionMap.CellType.Empty) {
//			//pos, heuristics, cost and parent
//		}
//
//		return neighbours;
//	}

}
