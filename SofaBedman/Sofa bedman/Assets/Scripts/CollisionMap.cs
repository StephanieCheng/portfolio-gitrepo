﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CollisionMap : MonoBehaviour {

    public enum CellType
    {
        Empty,
        Wall,
        Pawn
    }

    private enum Phase {
        Move,
        Select,
        Attack
    }

    public enum Turn {
        Player,
        Enemy
    }

    private Turn turn;
    private Phase gamePhase;

    private ArrayList ColMap;

    public TextAsset mapFile;

    public int xSize;
    public int zSize;

	public int souls = 1;

    public GameObject TestCube;
    public ArrayList Cubes;
    public Transform selectedCharacter;
    private Pawn pawnScript;
    //private Possesable posessableScript;
    private BoxCollider col;

    public HUD HUD;

    //holds sprite instances
    public GameObject HighlightField;
    public float HighlighFieldFloatHeight = 0.04f;
    private List<GameObject> HighlightFields;
    private GameObject[,] AllHighlightFields;


    private List<Vector2> ActiveFields;

    public GameObject HitSprite;

    private List<GameObject> allPawns;
	private List<GameObject> enemies;
	private List<GameObject> playerPawns;


    //

    // Use this for initialization
    void Start() {
        HighlightFields = new List<GameObject>();
        ActiveFields = new List<Vector2>();

		HUD.gameObject.SetActive(true);

        gamePhase = Phase.Select;

        selectedCharacter = null;
        //pawnScript = selectedCharacter.GetComponent<Pawn>();

        ReadTextFile();

        
        //TestMap();

        AllHighlightFields = InstantiateHighlightFields();

		playerPawns = new List<GameObject> ();

        MarkPawnPositions();
        turn = Turn.Player;

		HUD.SetSoulNo (souls);

		//find all enemies
		GameObject[] enemylist = GameObject.FindGameObjectsWithTag("Enemy");

		enemies = new List<GameObject> ();
		enemies.AddRange(enemylist);

    }

    // Update is called once per frame
    void Update() {
		if(turn == Turn.Player)
        	GetMousePos();

    }

    private GameObject[,] InstantiateHighlightFields() {
        GameObject[,] Fields = new GameObject[xSize,zSize];

        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < zSize; j++) {
                Vector3 HighlightPos = new Vector3(i, HighlighFieldFloatHeight, j);
                Quaternion Rot = Quaternion.Euler(90, 0, 0);

                GameObject Highlight = Instantiate(HighlightField, HighlightPos, Rot) as GameObject;
                Highlight.transform.SetParent(this.transform);
                Highlight.SetActive(false);
                Fields[i,j] = (Highlight);
            }
        }

        return Fields;

    }

    private void TestMap() {
        ArrayList Cubes = new ArrayList();
        Cubes.Clear();
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < zSize; j++) {
                GameObject Cub;
                if (GetColMapAt(i, j) == CellType.Empty)
                {
                    Cub = Instantiate(TestCube, new Vector3(i, 0, j), Quaternion.identity) as GameObject;
                }
                else {
                    Cub = Instantiate(TestCube, new Vector3(i, 1, j), Quaternion.identity) as GameObject;
                }

                if (Cub == null)
                    Debug.Log("Didnt instantiate");
                else
                    Cubes.Add(Cub);

                Cub.transform.SetParent(gameObject.transform);
            }
        }
    }

    public CellType GetColMapAt(int x, int z)
    {
        if (x < 0 || x >= xSize || z < 0 || z >= zSize)
            return CellType.Wall;
        return (CellType)ColMap[z * xSize + x];
    }


    private void GetMousePos() {
        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {

                Debug.Log(hit.transform.tag);

                switch (gamePhase)
                {
                    case Phase.Move:

					if (hit.transform.tag == "Map" && selectedCharacter != null)
                        {

                            Vector3 newLocation = new Vector3(Mathf.Round(hit.point.x), selectedCharacter.localPosition.y, Mathf.Round(hit.point.z));
                            Vector2 cellLocation = new Vector2(newLocation.x, newLocation.z);


                            // if (GetColMapAt((int)newLocation.x, (int)newLocation.z) == CellType.Empty)

                            if (ActiveFields.Contains(cellLocation))
                            {
                                UpdateMap((int)selectedCharacter.localPosition.x, (int)selectedCharacter.localPosition.z, CellType.Empty);
                                selectedCharacter.localPosition = newLocation;
                                UpdateMap((int)newLocation.x, (int)newLocation.z, CellType.Pawn);
								
								pawnScript.SetMoveAction(pawnScript.GetMoveAction()-1);
								if(pawnScript.GetMoveAction() <= 0)
									HUD.SetMoveButtonInteractivity(false);

                                ActiveFields.Clear();
                                DestroyHighlightedFields();
                                gamePhase = Phase.Select;

                                
                            }

                        }


                        break;
                    case Phase.Select:

                        if (hit.transform.tag == "Pawn")
                        {
                            DestroyHighlightedFields();
                            selectedCharacter = hit.transform;
                            pawnScript = selectedCharacter.GetComponent<Pawn>();
							
							
                            HUD.SetActiveCharacter(selectedCharacter);


                        }
                        else if (hit.transform.tag == "Possesable")
                        {
                            DestroyHighlightedFields();
                            selectedCharacter = hit.transform;
                            pawnScript = null;
                            HUD.SetActiveCharacter(selectedCharacter);

                        }
                        else if (hit.transform.tag == "Enemy")
                        {
                            DestroyHighlightedFields();
                            selectedCharacter = hit.transform;
                            pawnScript = null;
                            HUD.SetActiveCharacter(selectedCharacter);
                        }

                        break;
                    case Phase.Attack:

						

						if (ActiveFields.Contains(new Vector2(hit.transform.localPosition.x,hit.transform.localPosition.z)) &&
						   (hit.transform.tag == "Enemy" || hit.transform.tag == "Pawn" || hit.transform.tag == "Possesable")) {

                            Pawn targetScript = hit.transform.GetComponent<Pawn>();

                            int attackresult = DealDamage(pawnScript.attack, targetScript);
							
							pawnScript.SetAttackAction(pawnScript.GetAttackAction()-1);
							if(pawnScript.GetAttackAction() <= 0 ){
								HUD.SetAttackButtonInteractivity(false);
							}
						
							if(attackresult == -1){
								souls += 1;
								HUD.SetSoulNo(souls);
							}             
                            

                            //end the attackphase

                            DestroyHighlightedFields();
                            gamePhase = Phase.Select;
                            
                        }
					else{
						//clicked outside attack field
						gamePhase = Phase.Select;
						goto case Phase.Select;
					}

                        break;
                    default:
                        break;
                }
            

            }

        }
    }

    public void UpdateMap(int x, int z, CellType newCellType) {
        if (x < 0 || x >= xSize || z < 0 || z >= zSize)
            return;
        ColMap[z * xSize + x] = newCellType;
    }

    private void DestroyHighlightedFields() {
        for (int i = 0; i < HighlightFields.Count; i++)
        {
            HighlightFields[i].SetActive(false);
            //Destroy((GameObject)HighlightFields[i]);
        }
        HighlightFields.Clear();
    }

    public void AllowMovement(string msg) {
		Debug.Log("Movementr phase");
        DestroyHighlightedFields();
        gamePhase = Phase.Move;

        DefineAllowedMovement(pawnScript.move, pawnScript.GetPos());
       
    }

    private void DefineAllowedMovement(int move, Vector2 pos) {
        //TODO make pathfinding
        //List<Vector2> moveOK = new List<Vector2>();

        ActiveFields.Clear();
        
        //CanMoveTo.Add(pos);
        ActiveFields = PathFinding(pos, move);
        //for (int i = -move; i <= move; i++) {
        //    for (int j = -move; j <= move; j++) {
        //        if (new Vector2(i, j).magnitude <= move && GetColMapAt((int)pos.x + i, (int)pos.y + j) == CellType.Empty) {
        //            moveOK.Add(new Vector2(pos.x + i, pos.x + j));
        //        }
        //    }
        //}
        MarkWalkable(ActiveFields);
        //CanMoveTo = moveOK;


    }

    private void MarkWalkable(List<Vector2> moveOK) {
        for (int i = 0; i < moveOK.Count; i++) {
            //Vector2 p = moveOK[i];
            //Vector3 HighlightPos = new Vector3(p.x, HighlighFieldFloatHeight, p.y);
            //Quaternion Rot = Quaternion.Euler(90, 0, 0);

            //GameObject Highlight = Instantiate(HighlightField, HighlightPos, Rot) as GameObject;
            //Highlight.transform.SetParent(this.transform);


            GameObject Highlight = AllHighlightFields[(int)moveOK[i].x ,  (int)moveOK[i].y];
            Highlight.SetActive(true);
            HighlightFields.Add(Highlight);
        }
    }


    private List<Vector2> PathFinding(Vector2 startPosition, int move)
    {
        List<Vector2> open = new List<Vector2>();
        List<Vector2> closed = new List<Vector2>();

        open.Add(startPosition);

        Debug.Log(open[0]);

        PathFindingRecursion(open, closed, move);

        //Debug.Log(closed[0]);

        //Debug.Log(open[0]);

        closed.RemoveAt(0); //your position

        return closed;
    }
    private void PathFindingRecursion(List<Vector2> open, List<Vector2> closed, int move) {
        if (move == 0) //leaf
        {
            closed.AddRange(open);
            open.Clear();
            return ;
        }

        int count = open.Count;

        for (int i = 0; i < count; i++) {
            //open.AddRange(GetNodeNeighBours(open[0]));

            List<Vector2> neighbours = GetNodeNeighbours(open[0]);


            for (int j = 0; j < neighbours.Count; j++)
            {
                
                if (!open.Contains(neighbours[j]) && !closed.Contains(neighbours[j])) 
                    open.Add(neighbours[j]);
            }
            

            closed.Add(open[0]);
            open.RemoveAt(0);
        }

        PathFindingRecursion(open, closed, move - 1);

        return;
    }

    private List<Vector2> GetNodeNeighbours(Vector2 pos) {

        List<Vector2> neighbours = new List<Vector2>();

        if (gamePhase == Phase.Attack) {
            if (GetColMapAt((int)pos.x + 1, (int)pos.y) == CellType.Pawn)
                neighbours.Add(new Vector2(pos.x + 1, pos.y));

            if (GetColMapAt((int)pos.x - 1, (int)pos.y) == CellType.Pawn)
                neighbours.Add(new Vector2(pos.x - 1, pos.y));

            if (GetColMapAt((int)pos.x, (int)pos.y + 1) == CellType.Pawn)
                neighbours.Add(new Vector2(pos.x, pos.y + 1));

            if (GetColMapAt((int)pos.x, (int)pos.y - 1) == CellType.Pawn)
                neighbours.Add(new Vector2(pos.x, pos.y - 1));
        }
        

        if (GetColMapAt((int)pos.x + 1, (int)pos.y) == CellType.Empty)
            neighbours.Add(new Vector2(pos.x + 1, pos.y));

        if (GetColMapAt((int)pos.x - 1, (int)pos.y) == CellType.Empty)
            neighbours.Add(new Vector2(pos.x - 1, pos.y));

        if (GetColMapAt((int)pos.x, (int)pos.y + 1) == CellType.Empty)
            neighbours.Add(new Vector2(pos.x, pos.y + 1));

        if (GetColMapAt((int)pos.x, (int)pos.y - 1) == CellType.Empty)
            neighbours.Add(new Vector2(pos.x, pos.y - 1));



        return neighbours;
    }


    public void Posses(PossesSpell spell) {

		if (souls <= 0) {
			return;
		}

        //pawnScript = selectedCharacter.gameObject.AddComponent<Pawn>();
        pawnScript = selectedCharacter.GetComponent<Pawn>();
        selectedCharacter.tag = "Pawn";
        HUD.SetActiveCharacter(selectedCharacter);
        //Destroy(posessableScript);
		souls -= 1;
		HUD.SetSoulNo (souls);


		spell.PossesedPawn = pawnScript;
		spell.Posses ();
		HUD.SetActiveCharacter (selectedCharacter);

    }

    public void InititateAttackMode() {
        gamePhase = Phase.Attack;
		DestroyHighlightedFields();
		ActiveFields.Clear ();
		ActiveFields = PathFinding(pawnScript.GetPos(), 1);
		MarkWalkable(ActiveFields);
    }

    private void ReadTextFile() {

        string textfile = mapFile.text;

        List<string> lines = new List<string>();
        lines.AddRange(
                    textfile.Split("\n"[0]));

        //Debug.Log(lines.Count);

        xSize = Int32.Parse(lines[0]);
        zSize = Int32.Parse(lines[1]);

        lines.RemoveAt(0);
        lines.RemoveAt(0);


        //foreach (string line in lines) {
        //    Debug.Log(line);
        //}



        ColMap = new ArrayList();
        for (int i = 0; i < xSize; i++)
        {
            for (int j = 0; j < zSize; j++)
            {
                if (lines[i][j] == '.')
                    ColMap.Add(CellType.Empty);
                else if(lines[i][j] == '#')
                {
                    ColMap.Add(CellType.Wall);
                }
            }
        }

        col = gameObject.AddComponent<BoxCollider>();

        col.size = new Vector3(xSize, 0.03f, zSize);
        col.center = new Vector3(xSize / 2 - 0.5f, 0.5f, zSize / 2 - 0.5f);
    }

    private void MarkPawnPositions() {
        GameObject[] pawnlist = GameObject.FindGameObjectsWithTag("Pawn");
        GameObject[] enemylist = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] possesablelist = GameObject.FindGameObjectsWithTag("Possesable");

        allPawns = new List<GameObject>();
        allPawns.AddRange(pawnlist);
        allPawns.AddRange(enemylist);
        allPawns.AddRange(possesablelist);

		playerPawns.AddRange (pawnlist);

        


        int x;
        int z;

        foreach (GameObject pawn in pawnlist)  {
            x = (int)pawn.transform.localPosition.x;
            z = (int)pawn.transform.localPosition.z;
            UpdateMap(x, z, CellType.Pawn);
        }

        foreach (GameObject enemy in enemylist)
        {
            x = (int)enemy.transform.localPosition.x;
            z = (int)enemy.transform.localPosition.z;
            UpdateMap(x, z, CellType.Pawn);
        }

        foreach (GameObject possesable in possesablelist)
        {
            x = (int)possesable.transform.localPosition.x;
            z = (int)possesable.transform.localPosition.z;
            UpdateMap(x, z, CellType.Pawn);
        }
    }

	/// <summary>
	/// Deals the damage. Negative amount heals
	/// </summary>
	/// <returns>The damage.</returns>
	/// <param name="amount">Amount.</param>
	/// <param name="defender">Defender.</param>
    public int DealDamage(int amount, Pawn defender)
    {
       defender.currentHP -= amount;

		Mathf.Clamp (defender.currentHP, 0, defender.HP);

        GameObject hitAmount = Instantiate(HitSprite, defender.transform.localPosition, Quaternion.identity) as GameObject;
        hitAmount.transform.Translate(new Vector3(0.0f, 1.0f, 0.0f));
        hitAmount.GetComponent<TextMesh>().text = (-amount).ToString();
        hitAmount.GetComponent<HitAmountAnimate>().StartAnimation();



        if (defender.currentHP <= 0) {

			//enemy was killed 
			Debug.Log("enemy killed");
			//remove him from the map
			UpdateMap((int)defender.transform.position.x, (int)defender.transform.position.z, CellType.Empty);
			Destroy(defender.gameObject);

            return -1;

        }

        return 0; //no chnage
    }
	

	public void EndTurn(){
		Debug.Log("EndingTurn");

		//call HUD and make a end turn graphic
		if (turn == Turn.Player) {
			//turn = Turn.Enemy;
			//StartCoroutine ("ActivateEnemies");
		
			//both take 1 s
			HUD.RemoveHud();
			StartCoroutine("TurnSwitch");
			//ActivateEnemy(0);

		} else {

			StartCoroutine("TurnSwitch");
			//turn = Turn.Player;
		}

		//while cooroutine executes

		//unselect character
		selectedCharacter = null;
		pawnScript = null;
		DestroyHighlightedFields ();

		//reset erryones Move and Att
		foreach (GameObject pawn in allPawns) {
			if(pawn){
				Pawn pawnscr = pawn.GetComponent<Pawn>();
				pawnscr.ResetActions();
			}

		}


			

	}

	private IEnumerator TurnSwitch(){
		if (turn == Turn.Player) {
			turn = Turn.Enemy; //must be b4 yield
			//yield return new WaitForSeconds (1);
			//execute passive abilities
			yield return StartCoroutine("ExecutePassiveAblilities");
			//Debug.Log("execute exited");

			HUD.EndTurn ("Enemy Turn");
			yield return new WaitForSeconds (1);
			ActivateEnemy(0);
		} else {
			HUD.EndTurn ("Player Turn");
			yield return new WaitForSeconds (1);
			turn = Turn.Player;
		}
	}


	public void ActivateEnemy(int enemyNO){

		if (enemyNO >= enemies.Count) {
			EndTurn();
			return;
		}
		if (enemies [enemyNO]) {
			enemies [enemyNO].GetComponent<Enemy> ().ActivateEnemy (enemyNO, playerPawns);
		} else {
			enemies.RemoveAt(enemyNO);
			ActivateEnemy(enemyNO);
		}

	}

	private IEnumerator ExecutePassiveAblilities(){
		for(int i = 0; i < allPawns.Count; i++){
			if(allPawns[i] == null){
				allPawns.RemoveAt(i);
				i--;
				continue;
			}
			Pawn ps = allPawns[i].GetComponent<Pawn>();
			List<PassiveSkill> passives = ps.PassiveSkills;
			if(passives != null){
				for(int j = 0; j < passives.Count; j++){
					passives[j].Execute();
					yield return new WaitForSeconds(1f);
				}
			}
				

		}
		//Debug.Log("execute finished");
	}
}

