<b style="color:red">Controller required!!</b>

* Controls (ps controller):   
    * left stick - move  
    * right stick - camera  
    * R1 - lock on  
    * L3 - change lock on target  
    * cross - jump  
    * square - light attack  
    * triangle - heavly attack (chargable)  

# Third Person Camera

* Features  
    - basic mode:   
        - rotates horizontally full 360  
        * the vertical rotation has hard limits and lerps back to the default statring angle  
        * collision detection prevents the character from being out of sight if possible
        * collision detection on the camera borders prevents sides of the camera to stick out of the environment  
        * close object fade from view  
        * camera will smooth into position
    - lock on  
        * first target is the closest to the character  
        * target switching 
        * point of interest is in between the target and character  
        * the character is prioritized  
        * switching between lock on and basic has to use the least camera repositioning possible when close  
        * lock on and basic calculate their starting horizontal angle based on each other  
        * the rotation is limited to horizontal only  
        * the y rotation is based on the positions of the characeter and the target (height and ground distance) so that both are in view if possible  
        * the y rotation is locked when jumping and free when standing or falling, and can be locked for other actions  
        * the characters feet should be in view if possible (and the surrounding ground)  
        * switching on lock on is instant, switching off lock on is timed to prevent dizziness when button mashing  
        * the horizontal rotation is limited with far distances  

### Basic Camera Rotation

![gif1](https://imgur.com/kculDNd.gif)




### Camera Fade Objects

![gif4](https://imgur.com/iTwQdEM.gif)



### Camera Collision

![gif2](https://imgur.com/Dv95ldc.gif)


### Lock On Switching

![gif3](https://imgur.com/W6zCew6.gif)

* the angle is computed every switch to be as close as possible to the previous mode  
* when the character and target are close camera repositioning should be minimal

### Lock On Rotation

![gif5](https://imgur.com/4rY08NB.gif)

* only horizontaly, vertical rotation is automatic


### Lock On Lock Rotation At Distance

![gif5](https://imgur.com/jgHYj37.gif)

* at longer distances horizontal rotation is limited  

### Angle Control 

![gif5](https://imgur.com/EVlpF7F.gif)

* automatic vertical rotation

### Jumping Angle Lock

![gif5](https://imgur.com/kOUIXES.gif)




### Distant Enemy

![gif5](https://imgur.com/jgHYj37.gif)

### Enemy Below

![gif5](https://imgur.com/rR1jVIo.gif)

- the character has a ground radius and head radius that should be visible if possible

### EnemyAbove

![gif5](https://imgur.com/KpMqzYU.gif)

