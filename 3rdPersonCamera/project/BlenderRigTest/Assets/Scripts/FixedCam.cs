﻿using UnityEngine;
using System.Collections;

public class FixedCam : MonoBehaviour {

	public Vector3 camPosition = Vector3.zero;
	public Transform camLookAt = null; 

	private CameraFollow cf;
	private Vector3 smoothPosition;
	private Vector3 smoothlookat;

	private Vector3 velocityref = Vector3.zero;

	// Use this for initialization
	void Start () {
		cf = this.GetComponent<CameraFollow> ();
		smoothlookat = cf.getLookAt ();
		//camLookAt = smoothlookat;
		smoothPosition = this.transform.position;

	}
	
	// Update is called once per frame
	void LateUpdate () {
		//Debug.Log("ExternalScript");


		//this.transform.position = Vector3.SmoothDamp (this.transform.position, camPosition,ref velocityref, 1f);
		
	

		
		
		//smoothlookat = cf.SmoothLookingAt(smoothlookat, camLookAt.position, 20f);
		
		this.transform.position = camPosition;
		if(camLookAt)
			transform.LookAt (camLookAt.position);



	}

	public void camSetup(Vector3 camPos, Transform look){
		camPosition = camPos;
		camLookAt = look;
	}
	
}
