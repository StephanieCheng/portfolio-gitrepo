﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

//struct CameraPosition{
//	private Vector3 position;
//	private Transform xForm;
//	
//	public Vector3 Position { get {return position;} set { position = value;}}
//	public Transform XForm {get {return xForm;} set {xForm = value;}}
//	
//	
//	public void Init(string camName, Vector3 pos, Transform transform, Transform parent){
//		position = pos;
//		xForm = transform;
//		xForm.name = camName;
//		xForm.parent = parent;
//		xForm.localPosition = Vector3.zero;
//		xForm.localPosition = position;
//		
//	}
//}


public class CameraFollowOld3: MonoBehaviour {
	
	private enum CamStates
	{
		Free,
		FirstPerson,
		Target,
		LockOn
	}


	private CharacterControllerThirdPerson2 ccScript;


	//private GameObject[] lockOnTargets;
	private GameObject lockOnTarget = null;
	public RectTransform lockOnGraphic;
	public float lockonAnimSpee = 30f;
	private Vector3 lockOnLookAt; //between player and enemy
	//private int lockOnCurrent;
	private Vector3 smoothLookAt;
	public float lookSpeed = 20f;
	public LayerMask camObstacle;
	private RectTransform canvas;

	private Vector3 lookAt;

	public CharacterControllerThirdPerson2 playerScript;
	

	private CamStates camstate = CamStates.Free;
	
	//public float distanceAway;
	
	//public float distanceUp;
	
	
	private Vector3 lookDir;
	
	public float smooth;
	
	public Vector3 lookoffset = new Vector3(0,2.5f,0);
	private Transform follow;
	private Vector3 TargetPosition;
	

	
	private Vector3 veloityCamSmooth = Vector3.zero;
	private float camSmoothDampTime = 0.1f;
	

	
	private const float TargetING_THRESHOLD = 0.1f;
	

	
	
	private Vector3 curLookDir;
	private Vector3 velocityLookDir = Vector3.zero;
	//private float lookDirDampTime = 0.1f;
	
	
	//cam movement
	//private float distance;
	public float xSpeed = 50f;
	public float ySpeed = 50f;
	
	public float yMinLimit = -10f;
	public float yMaxLimit = 50f;
	
	//public float distanceMin = .5f;
	//public float distanceMax = 15f;
	
	
	float x = 180.0f;
	float y = 0.0f;

	float yLockOn = 0.0f;
	float xLockOn = 0.0f;

	float lockOnDistance;

	public float camCloseLimit;

	bool LNcooldown = false;

	public float LNcoolDownTime;



	public float yLerpSpeed = 0.2f;
	//private float defaultYAngle;
	
	public float defaultYAngle = 20f;
	public float distance = 6f;


	public float sigmoidFalloff;
	
	// Use this for initialization
	void Start () {
		//charcontrol = GameObject.FindWithTag ("Player").GetComponent<CharacterController> ();
		
		//distance =  Mathf.Sqrt (Mathf.Pow(distanceAway,2)  + Mathf.Pow(distanceUp,2));
		
		follow = GameObject.FindWithTag ("Player").transform;
		lookDir = follow.forward;
		curLookDir = follow.forward;
		

		//defaultYAngle = Mathf.Rad2Deg * distanceUp/distanceAway;

		//lockOnTargets = SortLockOns ();
		lockOnGraphic.sizeDelta = Vector2.zero;

		smoothLookAt = follow.position + lookoffset;
	 

	}
	

	void Update () {
		
	}
	
	void LateUpdate(){
		
		//float rightX = Input.GetAxis("RightStickX");
		//float rightY = Input.GetAxis("RightStickY");
		
		float leftX = Input.GetAxis("Horizontal");
		float leftY = Input.GetAxis("Vertical");
		
		//Vector3 characterOffset = follow.position + new Vector3 (0f, distanceUp, 0f);
		Vector3 characterOffset = follow.position + new Vector3 (0f, lookoffset.y, 0f);
		lookAt = follow.position + lookoffset;


		if (Input.GetButtonUp ("LockOn")) {
			StartCoroutine("LockOnCooldown");
			y=defaultYAngle;

			//get the x relative to xLockon
			Vector3 floorCharPos = transform.position - lookAt;
			floorCharPos.y = 0;
			
			x = Vector3.Angle(floorCharPos, -Vector3.forward);
			

			if(floorCharPos.x > 0){
				x = 360 - x;
			}

		}
		
		camstate = CamStates.Free;

		if (LNcooldown)
			camstate = CamStates.LockOn;


		if (Input.GetButton ("LockOn")) {

			camstate = CamStates.LockOn;
			//make swiching Targets available
			//if Target dies
			
		} else {
			if (Input.GetAxis ("CamFree") > TargetING_THRESHOLD) {
				camstate = CamStates.Target;
			}
		}

	


		if (Input.GetButtonDown ("LockOn")) {
	
				
			if (lockOnTarget != null){ // used to be checking for length of lockOnTargets
				StopCoroutine("LockOnCooldown");
				LNcooldown = false;
				//set inital angles and distance
				lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2;
					
				lockOnDistance = Vector3.Distance(lockOnLookAt, transform.position);

				//yLockon - angle distance and floor distance
				//xLockon - angle floordistance and floor forward
				Vector3 floorDistance = transform.position - lockOnLookAt;
				floorDistance.y = 0; //floor distance vector

				//TODO check camera below or above Target
				//yLockOn = Vector3.Angle(floorDistance, (transform.position - lockOnLookAt));
					
				xLockOn = Vector3.Angle(-Vector3.forward, floorDistance);
				if(floorDistance.x > 0){
					xLockOn = 360-xLockOn;
				}

			}

		}

		
		switch (camstate) {
		case CamStates.Free:

			Debug.DrawLine(transform.position, lookAt, Color.white);

			lockOnGraphic.sizeDelta = Vector2.zero;
			
			//analog movement
			//x += Input.GetAxis("Mouse X") * xSpeed * distance *  0.02f;
			//y -= Input.GetAxis("Mouse Y") * ySpeed * distance * 0.02f;
			
			if( Input.GetAxis("RightStickY") == 0){
				//y released needs to return to default

				//if (y < yLerpSpeed+ defaultYAngle || Math.Abs(y) > defaultYAngle - yLerpSpeed)
				if (y < yLerpSpeed+ defaultYAngle && y > defaultYAngle - yLerpSpeed){
					y = defaultYAngle;
				}else {
					y = (y > defaultYAngle ? y-yLerpSpeed : y+yLerpSpeed);
				}
			}
			else{
				y -= Input.GetAxis("RightStickY") * ySpeed * distance * 0.02f;
			}
			
			x += Input.GetAxis("RightStickX") * xSpeed * distance * 0.02f;
		
			
			y = ClampAngle(y, yMinLimit, yMaxLimit);
			
			Quaternion rotation = Quaternion.Euler(y, x, 0);

			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);

			Vector3 position = rotation * negDistance + lookAt; //rotate negdistance by rot around 000 and then translate in look Target direction
			

			TargetPosition = position;

			
			lookDir = characterOffset - this.transform.position;
			lookDir.y = 0;
			lookDir.Normalize ();

			CompensateForWalls (characterOffset, ref TargetPosition);
			SmoothPosition(this.transform.position, TargetPosition);



			smoothLookAt = SmoothLookingAt(smoothLookAt, lookAt, lookSpeed);


			transform.LookAt (smoothLookAt);

			break;
		case CamStates.Target: 
			
			x = Vector3.Angle(follow.forward, Vector3.forward);
			if(follow.forward.x < 0){ x = 360 - x;}
			//Debug.Log(follow.forward);
			//Debug.DrawRay(follow.position, follow.forward, Color.blue);
			//Debug.DrawRay(follow.position, Vector3.forward, Color.red);
			y = defaultYAngle;
			
			Quaternion rotation2 = Quaternion.Euler(y, x, 0);
			

			Vector3 negDistance2 = new Vector3(0.0f, 0.0f, -distance);	
			Vector3 position2 = rotation2 * negDistance2 + lookAt;
			TargetPosition = position2;

			CompensateForWalls (characterOffset, ref TargetPosition);
			SmoothPosition(this.transform.position, TargetPosition);
			
			
			transform.LookAt (lookAt);
			
			break;
		case CamStates.LockOn:

			//if there are enemies
			if (lockOnTarget != null){

				//standard lockon stuff
				//xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;

				float camCharTargetAngle = 180 - defaultYAngle;


				if (Vector3.Distance(lockOnTarget.transform.position, lookAt) > camCloseLimit){
					//farCam
					Debug.Log("farCam");

					//make sure that x angle is locked 15-+ degrees from the lockon direction
					Vector3 floorAngle = lockOnLookAt;
					floorAngle.y = 0;
					Vector3 floorChar = lookAt;
					floorChar.y = 0;
					floorAngle -= floorChar;

					float centerAngle = Vector3.Angle(Vector3.forward, floorAngle);

					if(floorAngle.x < 0){
						centerAngle = 360 - centerAngle;
					}

					//xlockon lerp to centerAngle
					if( Input.GetAxis("RightStickX") == 0){
						//y released needs to return to default
						
						//if (y < yLerpSpeed+ defaultYAngle || Math.Abs(y) > defaultYAngle - yLerpSpeed)
						if (xLockOn < yLerpSpeed+ centerAngle && xLockOn > centerAngle - yLerpSpeed){
							xLockOn = centerAngle;
						}else {
							xLockOn = (xLockOn > centerAngle ? xLockOn-yLerpSpeed : xLockOn+yLerpSpeed);
						}
					}
					else{
						xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;
					}

					xLockOn = ClampAngle(xLockOn, centerAngle - 15, centerAngle + 15);

					//y lockon

				}else{
					//closeCam
					//Debug.Log("closeCam");
					xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;

					//softer clamp TODO
//					if (Vector3.Distance(lockOnTarget.transform.position, lookAt) > camCloseLimit-3){
//						Debug.Log("camClampStart");
//					}

					//ylockon

				}

				//attemp 2 180-angle
				//lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 

				

				// angle between floor and char to Target
				
				Debug.DrawLine(transform.position, lockOnLookAt, Color.white);
				Debug.DrawLine(characterOffset, lockOnLookAt, Color.blue);
				
				float charToTarget = Vector3.Distance(characterOffset, lockOnLookAt);



				Vector3 charToLookVec = lockOnLookAt - characterOffset;
				
				Vector3 charToTargetAngle = charToLookVec;
				charToTargetAngle.y = 0;
				
				Vector3 crossCTT = Vector3.Cross(charToTargetAngle, charToLookVec);
				Vector3 posAngl = Quaternion.Euler(0, +90, 0) * charToTargetAngle;
				
				float charLockOnAngle = Vector3.Angle(charToTargetAngle, charToLookVec);

//				Debug.Log(Vector3.Angle(charToTargetAngle, lockOnLookAt - characterOffset));
//				Debug.DrawRay(Vector3.zero, lockOnLookAt - characterOffset, Color.cyan);
//				Debug.DrawRay(Vector3.zero, charToTargetAngle, Color.red);
//				Debug.DrawRay(Vector3.zero, crossCTT, Color.green);
//				Debug.DrawRay(Vector3.zero, posAngl , Color.magenta);
			

				if(playerScript.jumping == false)
					if( Mathf.Sign(crossCTT.x) != Mathf.Sign( posAngl.x)){ //!= looking up, == looking down
						//works for angling camera - attempt 2
						//charLockOnAngle = 0 - charLockOnAngle;


						//Debug.Log("lookingUp");
						camCharTargetAngle -= charLockOnAngle;
						lockOnDistance = Mathf.Sqrt(charToTarget * charToTarget + distance*distance - 2 * distance * charToTarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );

						float gamma = Mathf.Asin(Mathf.Sin(camCharTargetAngle *  Mathf.Deg2Rad)/lockOnDistance * distance) * Mathf.Rad2Deg;
						float gamma2 = 90 - charLockOnAngle;

						yLockOn = 90 - gamma - gamma2;
						yLockOn = 0 - yLockOn;

					}else{
						//looking down
						camCharTargetAngle = 180 + defaultYAngle - charLockOnAngle;
						lockOnDistance = Mathf.Sqrt(charToTarget * charToTarget + distance*distance - 2 * distance * charToTarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );

						float gamma = Mathf.Asin(Mathf.Sin(camCharTargetAngle *  Mathf.Deg2Rad)/lockOnDistance * distance) * Mathf.Rad2Deg;
						float gamma2 = 90 - charLockOnAngle;

						yLockOn = 90 - gamma - gamma2;

						//Free - only on ground!!
						//yLockOn = Mathf.Asin(Mathf.Sin((180-defaultYAngle)*  Mathf.Deg2Rad)/lockOnDistance * distance) * Mathf.Rad2Deg;

					}

				Debug.Log(lockOnDistance);
				//Debug.Log(Vector3.Distance(lockOnTarget.transform.position, lookAt));

				//sigmoid falloff
				float height = Mathf.Abs (lockOnLookAt.y -  characterOffset.y);
				Vector3 distV = lockOnTarget.transform.position -  lookAt;
				distV.y = 0;
				float dist = Vector3.Magnitude(distV);
				float sigmoidHeight = 1 / (1 + Mathf.Exp((-height + sigmoidFalloff)));
				float sigmoidDist = 1 / (1 + Mathf.Exp((-dist + camCloseLimit)));
				Debug.Log(height);
				Debug.Log(sigmoidHeight);
				Debug.Log(dist);
				Debug.Log(sigmoidDist);
				lockOnDistance += height * sigmoidHeight + height * sigmoidDist;
				Debug.Log(lockOnDistance);


				//attempt 3. - most precise
//				Vector3 camToTarget = transform.position - lockOnLookAt;
//				Vector3 camToTargetFlat  = camToTarget;
//				camToTargetFlat.y = 0;
//				Vector3 crossCT = Vector3.Cross(camToTarget, camToTargetFlat);
//				Vector3 posAngle = Quaternion.Euler(0, +90, 0) * camToTargetFlat;
//
//				Debug.Log(Vector3.Angle(camToTarget, camToTargetFlat));
//				Debug.DrawRay(Vector3.zero, camToTarget, Color.cyan);
//				Debug.DrawRay(Vector3.zero, camToTargetFlat, Color.red);
//				Debug.DrawRay(Vector3.zero, crossCT, Color.green);
//				Debug.DrawRay(Vector3.zero, posAngle , Color.magenta);
//
//				float camToTargetAngle = Vector3.Angle(camToTarget, camToTargetFlat);
//
//				if( Mathf.Sign(crossCT.x) != Mathf.Sign( posAngle.x)){ //!= looking up, == looking down
//					camToTargetAngle = 0 - camToTargetAngle;				
//				}
//				yLockOn = camToTargetAngle;
				//lockOnDistance = Mathf.Sqrt(charToTarget * charToTarget + distance*distance - 2 * distance * charToTarget * Mathf.Cos((180-defaultYAngle)* Mathf.Deg2Rad) );






				//solution 1 - Target same plane - horizontal cam
				//law of cos and sin to get acutal lengths
				//lockOnDistance = Mathf.Sqrt(charToTarget * charToTarget + distance*distance - 2 * distance * charToTarget * Mathf.Cos((180-defaultYAngle)* Mathf.Deg2Rad) );
				//yLockOn = Mathf.Asin(Mathf.Sin((180-defaultYAngle)*  Mathf.Deg2Rad)/lockOnDistance * distance) * Mathf.Rad2Deg;
				//Debug.Log(lockOnDistance);

				//TODO solve yLockOn problem -- zoom out cam vs angle the cam to see the char??

				//if (lockOnLookAt.y > lookAt.y + (distance * Mathf.Sin(Mathf.Deg2Rad * defaultYAngle)) ){
					//yLockOn = 0-yLockOn;
				//}
				//Debug.Log(yLockOn);


				//icon
				Vector2 lockOnPos = Camera.main.WorldToViewportPoint(lockOnTarget.transform.position);
				lockOnGraphic.anchorMin = lockOnPos;
				lockOnGraphic.anchorMax = lockOnPos;

				lockOnGraphic.sizeDelta = Vector2.Lerp(lockOnGraphic.sizeDelta, new Vector2(50f, 50f), lockonAnimSpee * Time.deltaTime);

				if(LNcooldown){
					lockOnGraphic.sizeDelta = Vector2.zero;
				}


				//setup lookat

				lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 


				Quaternion rotationL = Quaternion.Euler(yLockOn, xLockOn, 0);
				Vector3 negDistanceL = new Vector3(0.0f, 0.0f, -lockOnDistance);
				Vector3 positionL = rotationL * negDistanceL + lockOnLookAt;		
				TargetPosition = positionL;


				CompensateForWalls (lockOnLookAt, ref TargetPosition);
				SmoothPosition(this.transform.position, TargetPosition);

				smoothLookAt = SmoothLookingAt(smoothLookAt, lockOnLookAt, lookSpeed);

				transform.LookAt (smoothLookAt);


				//Debug.DrawLine(transform.position, lockOnLookAt, Color.blue);

			} else{
				//no emenies free cam
				goto case CamStates.Free;
			}
			break;
		
			
		}
		
		
		//Debug.Log (lookDir );
		
//		CompensateForWalls (characterOffset, ref TargetPosition);
//		SmoothPosition(this.transform.position, TargetPosition);
//		
//		
//		transform.LookAt (lookAt);

	}
	

	private Vector3 SmoothLookingAt(Vector3 smoothLookAt, Vector3 lockOnLookAt, float lookSpeed){
		if (!CompareVectors(smoothLookAt, lockOnLookAt, 1))
					smoothLookAt = Vector3.Lerp(smoothLookAt, lockOnLookAt, lookSpeed * Time.deltaTime);
		else smoothLookAt = lockOnLookAt;
		return smoothLookAt;
	}
	
	private void SmoothPosition(Vector3 fromPos, Vector3 toPos){
		this.transform.position = Vector3.SmoothDamp (fromPos, toPos, ref veloityCamSmooth, camSmoothDampTime);
	}
	
	
	private void CompensateForWalls(Vector3 fromObject, ref Vector3 toTarget){
		//Debug.DrawLine (fromObject, toTarget, Color.cyan);
		
		RaycastHit wallHit = new RaycastHit ();
		//add thickness
		Vector3 direction = toTarget - fromObject;
		direction.Normalize ();
		direction *= 0.5f; 
		//

		if (Physics.Linecast (fromObject, toTarget+direction, out wallHit, camObstacle)) {
			Debug.DrawRay(wallHit.point, Vector3.left, Color.red);
			toTarget = new Vector3(wallHit.point.x - direction.x, toTarget.y, wallHit.point.z - direction.z);
		}

		//Debug.DrawLine (fromObject, toTarget, Color.white);
		//Debug.DrawLine (toTarget, toTarget+direction, Color.green);


	}
	
	//	private void FreeCamera(){
	//		lookWeight = Mathf.Lerp (lookWeight, 0f, Time.deltaTime * firstPersonLookSpeed);
	//		transform.localRotation = Quaternion.Lerp (transform.localRotation, Quaternion.identity, Time.deltaTime);
	//	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}
	

	public void NewTarget (GameObject Target){
		lockOnTarget = Target;
	}

	public void NoTarget (){
		lockOnTarget = null;
	}


	IEnumerator LockOnCooldown(){
		float t = 0;
		while (t < LNcoolDownTime){
			LNcooldown = true;
			t += Time.deltaTime;
			yield return 0;
		}
		LNcooldown = false;


	}

	bool CompareVectors(Vector3 a,Vector3 b, float angleError){
		//if they aren't the same length, don't bother checking the rest.
		if(!Mathf.Approximately(a.magnitude, b.magnitude))
			return false;
		float cosAngleError = Mathf.Cos(angleError * Mathf.Deg2Rad);
		//A value between -1 and 1 corresponding to the angle.
		float cosAngle = Vector3.Dot(a.normalized, b.normalized);
		//The dot product of normalized Vectors is equal to the cosine of the angle between them.
		//So the closer they are, the closer the value will be to 1.  Opposite Vectors will be -1
		//and orthogonal Vectors will be 0.
		
		if(cosAngle >= cosAngleError) {
			//If angle is greater, that means that the angle between the two vectors is less than the error allowed.
			return false;
		}
		else 
			return true;
	}


	
}
