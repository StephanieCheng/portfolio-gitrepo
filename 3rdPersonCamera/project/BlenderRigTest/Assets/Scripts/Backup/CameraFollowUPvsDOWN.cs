﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

struct CameraPositionUPvsDOWN{
	private Vector3 position;
	private Transform xForm;
	
	public Vector3 Position { get {return position;} set { position = value;}}
	public Transform XForm {get {return xForm;} set {xForm = value;}}
	
	
	public void Init(string camName, Vector3 pos, Transform transform, Transform parent){
		position = pos;
		xForm = transform;
		xForm.name = camName;
		xForm.parent = parent;
		xForm.localPosition = Vector3.zero;
		xForm.localPosition = position;
		
	}
}


public class CameraFollowUPvsDOWN : MonoBehaviour {
	
	private enum CamStates
	{
		FreeCam,
		FirstPerson,
		ResetCam,
		LockOn
	}


	private CharacterControllerThirdPerson2 ccScript;


	//private GameObject[] lockOnTargets;
	private GameObject lockOnTarget = null;
	public RectTransform lockOnGraphic;
	public float lockonAnimSpee = 30f;
	private Vector3 lockOnLookAt; //between player and enemy
	//private int lockOnCurrent;
	private Vector3 smoothLookAt;
	public float lookSpeed = 20f;
	public LayerMask camObstacle;
	private RectTransform canvas;

	private Vector3 lookAt;

	public CharacterControllerThirdPerson2 playerScript;
	

	private CamStates camstate = CamStates.FreeCam;
	
	//public float distanceAway;
	
	//public float distanceUp;
	
	
	private Vector3 lookDir;
	
	public float smooth;
	
	public Vector3 lookoffset = new Vector3(0,2.5f,0);
	private Transform follow;
	private Vector3 targetPosition;
	

	
	private Vector3 veloityCamSmooth = Vector3.zero;
	private float camSmoothDampTime = 0.1f;
	

	
	private const float TARGETING_THRESHOLD = 0.1f;
	

	
	
	private Vector3 curLookDir;
	private Vector3 velocityLookDir = Vector3.zero;
	//private float lookDirDampTime = 0.1f;
	
	
	//cam movement
	//private float distance;
	public float xSpeed = 50f;
	public float ySpeed = 50f;
	
	public float yMinLimit = -10f;
	public float yMaxLimit = 50f;
	
	//public float distanceMin = .5f;
	//public float distanceMax = 15f;
	
	
	float x = 180.0f;
	float y = 0.0f;

	float yLockOn = 0.0f;
	float xLockOn = 0.0f;

	float lockOnDistance;

	public float camCloseLimit;

	bool LNcooldown = false;

	public float LNcoolDownTime;



	public float yLerpSpeed = 0.2f;
	//private float defaultYAngle;
	
	public float defaultYAngle = 20f;
	public float distance = 6f;


	public float sigmoidFalloff;

	private float heightDistance;
	private float widthDistance;
	private float calibratedDistance;

	private float camBottomExtend;

	private Camera cam;
	private Vector3 characterOffset;
	
	// Use this for initialization
	void Start () {
		//charcontrol = GameObject.FindWithTag ("Player").GetComponent<CharacterController> ();
		
		//distance =  Mathf.Sqrt (Mathf.Pow(distanceAway,2)  + Mathf.Pow(distanceUp,2));
		
		follow = GameObject.FindWithTag ("Player").transform;
		lookDir = follow.forward;
		curLookDir = follow.forward;

		cam = gameObject.GetComponent("Camera") as Camera;
		

		//defaultYAngle = Mathf.Rad2Deg * distanceUp/distanceAway;

		//lockOnTargets = SortLockOns ();
		lockOnGraphic.sizeDelta = Vector2.zero;

		smoothLookAt = follow.position + lookoffset;

		//calc camBottomExtend

		Vector3 camFreePos = lookoffset + (Quaternion.AngleAxis(defaultYAngle, Vector3.right) * (- Vector3.forward * distance));

		Vector3 groundToCam = camFreePos - Vector3.zero;

		float gamma = Vector3.Angle (groundToCam, Vector3.up);
		//Debug.Log (gamma);

		float beta = 180 - (defaultYAngle + 90) - gamma;
		float beta2 = cam.fieldOfView / 2 - beta;

		float alfa = 180 - (90 - gamma) - beta2; 

		//sin 


		camBottomExtend = Mathf.Sin(beta2 *  Mathf.Deg2Rad) / Mathf.Sin(alfa *  Mathf.Deg2Rad) * groundToCam.magnitude; 
		//Debug.Log (camBottomExtend);

		camBottomExtend /= 6;
	}
	

	void Update () {
		
	}

	void LateUpdate(){
	


		//float rightX = Input.GetAxis("RightStickX");
		//float rightY = Input.GetAxis("RightStickY");
		
		float leftX = Input.GetAxis("Horizontal");
		float leftY = Input.GetAxis("Vertical");
		
		//Vector3 characterOffset = follow.position + new Vector3 (0f, distanceUp, 0f);
		characterOffset = follow.position + new Vector3 (0f, lookoffset.y, 0f);
		lookAt = follow.position + lookoffset;


		if (Input.GetButtonUp ("LockOn")) {
			StartCoroutine("LockOnCooldown");
			y=defaultYAngle;

			//get the x relative to xLockon
			Vector3 floorCharPos = transform.position - lookAt;
			floorCharPos.y = 0;
			
			x = Vector3.Angle(floorCharPos, -Vector3.forward);
			

			if(floorCharPos.x > 0){
				x = 360 - x;
			}

		}
		
		camstate = CamStates.FreeCam;

		if (LNcooldown)
			camstate = CamStates.LockOn;


		if (Input.GetButton ("LockOn")) {

			camstate = CamStates.LockOn;
			//make swiching targets available
			//if target dies
			
		} else {
			if (Input.GetAxis ("CamReset") > TARGETING_THRESHOLD) {
				camstate = CamStates.ResetCam;
			}
		}

	


		if (Input.GetButtonDown ("LockOn")) {
	
				
			if (lockOnTarget != null){ // used to be checking for length of lockontargets
				StopCoroutine("LockOnCooldown");
				LNcooldown = false;
				//set inital angles and distance
				lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2;
					
				lockOnDistance = Vector3.Distance(lockOnLookAt, transform.position);

				//yLockon - angle distance and floor distance
				//xLockon - angle floordistance and floor forward
				Vector3 floorDistance = transform.position - lockOnLookAt;
				floorDistance.y = 0; //floor distance vector

				//TODO check camera below or above target
				//yLockOn = Vector3.Angle(floorDistance, (transform.position - lockOnLookAt));
					
				xLockOn = Vector3.Angle(-Vector3.forward, floorDistance);
				if(floorDistance.x > 0){
					xLockOn = 360-xLockOn;
				}

			}

		}

		
		switch (camstate) {
		case CamStates.FreeCam:

			Debug.DrawLine(transform.position, lookAt, Color.blue);

			lockOnGraphic.sizeDelta = Vector2.zero;
			
			//analog movement
			//x += Input.GetAxis("Mouse X") * xSpeed * distance *  0.02f;
			//y -= Input.GetAxis("Mouse Y") * ySpeed * distance * 0.02f;
			
			if( Input.GetAxis("RightStickY") == 0){
				//y released needs to return to default

				//if (y < yLerpSpeed+ defaultYAngle || Math.Abs(y) > defaultYAngle - yLerpSpeed)
				if (y < yLerpSpeed+ defaultYAngle && y > defaultYAngle - yLerpSpeed){
					y = defaultYAngle;
				}else {
					y = (y > defaultYAngle ? y-yLerpSpeed : y+yLerpSpeed);
				}
			}
			else{
				y -= Input.GetAxis("RightStickY") * ySpeed * distance * 0.02f;
			}
			
			x += Input.GetAxis("RightStickX") * xSpeed * distance * 0.02f;
		
			
			y = ClampAngle(y, yMinLimit, yMaxLimit);
			
			Quaternion rotation = Quaternion.Euler(y, x, 0);

			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);

			Vector3 position = rotation * negDistance + lookAt; //rotate negdistance by rot around 000 and then translate in look target direction
			

			targetPosition = position;

			
			lookDir = characterOffset - this.transform.position;
			lookDir.y = 0;
			lookDir.Normalize ();

			CompensateForWalls (characterOffset, ref targetPosition);
			SmoothPosition(this.transform.position, targetPosition);



			smoothLookAt = SmoothLookingAt(smoothLookAt, lookAt, lookSpeed);


			transform.LookAt (smoothLookAt);



			break;
		case CamStates.ResetCam: 
			
			x = Vector3.Angle(follow.forward, Vector3.forward);
			if(follow.forward.x < 0){ x = 360 - x;}
//			Debug.Log(follow.forward);
			//Debug.DrawRay(Vector3.zero, follow.forward, Color.blue);
			//Debug.DrawRay(Vector3.zero, Vector3.forward, Color.red);
			y = defaultYAngle;
			
			Quaternion rotation2 = Quaternion.Euler(y, x, 0);
			

			Vector3 negDistance2 = new Vector3(0.0f, 0.0f, -distance);	
			Vector3 position2 = rotation2 * negDistance2 + lookAt;
			targetPosition = position2;

			CompensateForWalls (characterOffset, ref targetPosition);
			SmoothPosition(this.transform.position, targetPosition);
			
			
			transform.LookAt (lookAt);
			
			break;
		case CamStates.LockOn:

			//if there are enemies
			if (lockOnTarget != null){

				//setup lookat
				
				lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 

				//standard lockon stuff
				//xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;

				float camCharTargetAngle = 180 - defaultYAngle;

				float targetDistance = Vector3.Distance(lockOnTarget.transform.position, lookAt);

				if (targetDistance > camCloseLimit){
					//farCam
					Debug.Log("farCam");

					//make sure that x angle is locked 15-+ degrees from the lockon direction
					Vector3 floorAngle = lockOnLookAt;
					floorAngle.y = 0;
					Vector3 floorChar = lookAt;
					floorChar.y = 0;
					floorAngle -= floorChar;

					float centerAngle = Vector3.Angle(Vector3.forward, floorAngle);

					if(floorAngle.x < 0){
						centerAngle = 360 - centerAngle;
					}

					//xlockon lerp to centerAngle
					if( Input.GetAxis("RightStickX") == 0){
						//y released needs to return to default
						
						//if (y < yLerpSpeed+ defaultYAngle || Math.Abs(y) > defaultYAngle - yLerpSpeed)
						if (xLockOn < yLerpSpeed+ centerAngle && xLockOn > centerAngle - yLerpSpeed){
							xLockOn = centerAngle;
						}else {
							xLockOn = (xLockOn > centerAngle ? xLockOn-yLerpSpeed : xLockOn+yLerpSpeed);
						}
					}
					else{
						xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;
					}

					xLockOn = ClampAngle(xLockOn, centerAngle - 15, centerAngle + 15);

					//y lockon

				}else{
					//closeCam
					//Debug.Log("closeCam");
					xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;

					//softer clamp TODO
//					if (Vector3.Distance(lockOnTarget.transform.position, lookAt) > camCloseLimit-3){
//						Debug.Log("camClampStart");
//					}

					//ylockon

				}

				//attemp 2 180-angle
				//lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 

				

				// angle between floor and char to target
				
				Debug.DrawLine(transform.position, lockOnLookAt, Color.red);
				Debug.DrawLine(characterOffset, lockOnLookAt, Color.blue);
				Debug.DrawLine(lockOnLookAt, lockOnTarget.transform.position, Color.green);
				Debug.DrawLine(characterOffset, transform.position, Color.yellow);
				
				float charTotarget = Vector3.Distance(characterOffset, lockOnLookAt);



				Vector3 charToLookVec = lockOnLookAt - characterOffset;
				
				Vector3 charToTargetAngle = charToLookVec;
				charToTargetAngle.y = 0;
				
				Vector3 crossCTT = Vector3.Cross(charToTargetAngle, charToLookVec);
				Vector3 posAngl = Quaternion.Euler(0, +90, 0) * charToTargetAngle;
				
				float charLockOnAngle = Vector3.Angle(charToTargetAngle, charToLookVec);



//				Debug.Log(Vector3.Angle(charToTargetAngle, lockOnLookAt - characterOffset));
//				Debug.DrawRay(Vector3.zero, lockOnLookAt - characterOffset, Color.cyan);
//				Debug.DrawRay(Vector3.zero, charToTargetAngle, Color.red);
//				Debug.DrawRay(Vector3.zero, crossCTT, Color.green);
//				Debug.DrawRay(Vector3.zero, posAngl , Color.magenta);



				if(playerScript.jumping == false){
					heightDistance = Mathf.Abs(characterOffset.y - lockOnLookAt.y); //lockOnLookAt.y
					//Debug.Log(heightDistance);
					widthDistance = 0;



					//distance increase
					// log(x - closecamlimit+1)
					if(targetDistance > camCloseLimit){
						Vector3 groundlookAt = lookAt;
						groundlookAt.y =0;
						Vector3 groundLockOn = lockOnLookAt;
						groundLockOn.y = 0;
						float groundDistanceLimit = Mathf.Sqrt (Mathf.Pow(camCloseLimit/2,2) - Mathf.Pow(Mathf.Min(heightDistance, camCloseLimit/2 - 0.01f),2) );
						//Debug.Log(groundDistanceLimit);

						float groundDistance = Vector3.Distance(groundlookAt, groundLockOn);

						//Debug.Log(groundDistance);
						if(groundDistance > groundDistanceLimit){
							widthDistance =  Mathf.Log(groundDistance - groundDistanceLimit + 1);
							//Debug.Log("groundInfluence");
							//Debug.Log(Mathf.Log(groundDistance - groundDistanceLimit + 1));
						}

					}
					calibratedDistance = distance + Mathf.Max(heightDistance, widthDistance);
						

				}

				//Debug.Log (heightDistance);


			

				if( Mathf.Sign(crossCTT.x) != Mathf.Sign( posAngl.x)){ //!= looking up, == looking down


					Debug.Log("lookingUp");
					camCharTargetAngle -= charLockOnAngle;



					float trueLockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + distance*distance - 2 * distance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );
					float gamma = Mathf.Asin(Mathf.Sin(camCharTargetAngle *  Mathf.Deg2Rad)/trueLockOnDistance * distance) * Mathf.Rad2Deg;
		

					if(playerScript.jumping == false){


						float gamma2 = 90 - charLockOnAngle;



						yLockOn = 90 - gamma - gamma2;
						yLockOn = 0 - yLockOn;



					}
					lockOnDistance = FOVDistance(180 - gamma - camCharTargetAngle);
					camCharTargetAngle += defaultYAngle;
					//lockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + calibratedDistance*calibratedDistance - 2 * calibratedDistance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );
					camCharTargetAngle -= defaultYAngle;


				}else{
					//looking down
					Debug.Log("lookingDown");

					camCharTargetAngle = 180 + defaultYAngle - charLockOnAngle;

					if(playerScript.jumping == false){
						lockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + distance*distance - 2 * distance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );

						float gamma = Mathf.Asin(Mathf.Sin(camCharTargetAngle *  Mathf.Deg2Rad)/lockOnDistance * distance) * Mathf.Rad2Deg;
						float gamma2 = 90 - charLockOnAngle;

						yLockOn = 90 - gamma - gamma2;

						calibratedDistance = distance + widthDistance;

					}
				
					camCharTargetAngle -= defaultYAngle;
					lockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + calibratedDistance*calibratedDistance - 2 * calibratedDistance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );
					camCharTargetAngle += defaultYAngle;

				}

				//Debug.Log(lockOnDistance);


				//icon
				Vector2 lockOnPos = Camera.main.WorldToViewportPoint(lockOnTarget.transform.position);
				lockOnGraphic.anchorMin = lockOnPos;
				lockOnGraphic.anchorMax = lockOnPos;

				lockOnGraphic.sizeDelta = Vector2.Lerp(lockOnGraphic.sizeDelta, new Vector2(50f, 50f), lockonAnimSpee * Time.deltaTime);

				if(LNcooldown){
					lockOnGraphic.sizeDelta = Vector2.zero;
				}


				//setup lookat

				//lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 


				Quaternion rotationL = Quaternion.Euler(yLockOn, xLockOn, 0);
				Vector3 negDistanceL = new Vector3(0.0f, 0.0f, -lockOnDistance);
				Vector3 positionL = rotationL * negDistanceL + lockOnLookAt;		
				targetPosition = positionL;


				//CompensateForWalls (lockOnLookAt, ref targetPosition);
				SmoothPosition(this.transform.position, targetPosition);

				smoothLookAt = SmoothLookingAt(smoothLookAt, lockOnLookAt, lookSpeed);

				transform.LookAt (smoothLookAt);


				//Debug.DrawLine(transform.position, lockOnLookAt, Color.blue);

			} else{
				//no emenies free cam
				goto case CamStates.FreeCam;
			}
			break;
		
			
		}
		
		
		//Debug.Log (lookDir );
		
//		CompensateForWalls (characterOffset, ref targetPosition);
//		SmoothPosition(this.transform.position, targetPosition);
//		
//		
//		transform.LookAt (lookAt);

	}

	private float FOVDistance(float freeCamAngle){

		Vector3 charToTargetProject = lockOnLookAt - characterOffset;
		charToTargetProject.y = 0; 


		Vector3 floorExtend = follow.transform.position + (-charToTargetProject.normalized * camBottomExtend);

		Vector3 camFreePos = characterOffset + (Quaternion.AngleAxis(defaultYAngle,Quaternion.AngleAxis(90, follow.up) * charToTargetProject) * (- charToTargetProject.normalized * distance));

		Vector3 floorToTarget = (lockOnLookAt - follow.transform.position);
		Vector3 floorToTargetfloor = floorToTarget;
		floorToTargetfloor.y = 0;

		float floorToTargetAngle = Vector3.Angle (floorToTarget, floorToTargetfloor);
//		Debug.DrawRay (Vector3.zero, floorToTarget, Color.red);
//		Debug.DrawRay (Vector3.zero , floorToTargetfloor, Color.blue);


		float camfloorToTargetAngle = 180 - floorToTargetAngle;


		Vector3 freeCamToChar =  characterOffset - camFreePos;
		Vector3 freeCamToFloorOffset = floorExtend - camFreePos;

		float beta = Vector3.Angle (freeCamToChar, freeCamToFloorOffset);

		//Debug.Log (beta);

		float beta2 = 180 - beta - freeCamAngle;

		//sinus 

		float x = Mathf.Sin (beta2 * Mathf.Deg2Rad) * freeCamToFloorOffset.magnitude / Mathf.Sin (30 * Mathf.Deg2Rad);

		//cos

		float floorSide = x + camBottomExtend;


		float fovDist = Mathf.Sqrt(floorSide * floorSide + floorToTarget.magnitude*floorToTarget.magnitude - 2 * floorToTarget.magnitude * floorSide * Mathf.Cos(camfloorToTargetAngle* Mathf.Deg2Rad) );

		//if (fovDist < distance)
		//	fovDist = distance;


		return fovDist;



	}



	private Vector3 SmoothLookingAt(Vector3 smoothLookAt, Vector3 lockOnLookAt, float lookSpeed){
		if (!CompareVectors(smoothLookAt, lockOnLookAt, 1))
					smoothLookAt = Vector3.Lerp(smoothLookAt, lockOnLookAt, lookSpeed * Time.deltaTime);
		else smoothLookAt = lockOnLookAt;
		return smoothLookAt;
	}
	
	private void SmoothPosition(Vector3 fromPos, Vector3 toPos){
		this.transform.position = Vector3.SmoothDamp (fromPos, toPos, ref veloityCamSmooth, camSmoothDampTime);
	}
	
	
	private void CompensateForWalls(Vector3 fromObject, ref Vector3 toTarget){
		//Debug.DrawLine (fromObject, toTarget, Color.cyan);
		
		RaycastHit wallHit = new RaycastHit ();
		//add thickness
		Vector3 direction = toTarget - fromObject;
		direction.Normalize ();
		direction *= 0.5f; 
		//

		if (Physics.Linecast (fromObject, toTarget+direction, out wallHit, camObstacle)) {
			Debug.DrawRay(wallHit.point, Vector3.left, Color.red);
			toTarget = new Vector3(wallHit.point.x - direction.x, toTarget.y, wallHit.point.z - direction.z);
		}

		//Debug.DrawLine (fromObject, toTarget, Color.white);
		//Debug.DrawLine (toTarget, toTarget+direction, Color.green);


	}
	
	//	private void Targetera(){
	//		lookWeight = Mathf.Lerp (lookWeight, 0f, Time.deltaTime * firstPersonLookSpeed);
	//		transform.localRotation = Quaternion.Lerp (transform.localRotation, Quaternion.identity, Time.deltaTime);
	//	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}
	

	public void NewTarget (GameObject target){
		lockOnTarget = target;
	}

	public void NoTarget (){
		lockOnTarget = null;
	}


	IEnumerator LockOnCooldown(){
		float t = 0;
		while (t < LNcoolDownTime){
			LNcooldown = true;
			t += Time.deltaTime;
			yield return 0;
		}
		LNcooldown = false;


	}

	bool CompareVectors(Vector3 a,Vector3 b, float angleError){
		//if they aren't the same length, don't bother checking the rest.
		if(!Mathf.Approximately(a.magnitude, b.magnitude))
			return false;
		float cosAngleError = Mathf.Cos(angleError * Mathf.Deg2Rad);
		//A value between -1 and 1 corresponding to the angle.
		float cosAngle = Vector3.Dot(a.normalized, b.normalized);
		//The dot product of normalized Vectors is equal to the cosine of the angle between them.
		//So the closer they are, the closer the value will be to 1.  Opposite Vectors will be -1
		//and orthogonal Vectors will be 0.
		
		if(cosAngle >= cosAngleError) {
			//If angle is greater, that means that the angle between the two vectors is less than the error allowed.
			return false;
		}
		else 
			return true;
	}


	
}
