﻿using UnityEngine;
using System.Collections;

//struct CameraPosition{
//	private Vector3 position;
//	private Transform xForm;
//	
//	public Vector3 Position { get {return position;} set { position = value;}}
//	public Transform XForm {get {return xForm;} set {xForm = value;}}
//	
//	
//	public void Init(string camName, Vector3 pos, Transform transform, Transform parent){
//		position = pos;
//		xForm = transform;
//		xForm.name = camName;
//		xForm.parent = parent;
//		xForm.localPosition = Vector3.zero;
//		xForm.localPosition = position;
//		
//	}
//}

public class CameraFollowOld: MonoBehaviour {
	
	private enum CamStates
	{
		Free,
		FirstPerson,
		Target,
		LockOn
	}
	
	//private CharacterController charcontrol;
	private CamStates camstate = CamStates.Free;
	
	//public float distanceAway;
	
	//public float distanceUp;
	
	
	public Vector3 lookDir;
	
	public float smooth;
	
	public Vector3 lookoffset;
	public Transform follow;
	private Vector3 TargetPosition;
	
	//private float widescreen = 0.2f;
	
	//private float TargetingTime = 0.5f;
	
	private Vector3 veloityCamSmooth = Vector3.zero;
	public float camSmoothDampTime = 0.1f;
	
	//	private CameraPosition firstPersonCamPos;
	//
	//	public float firstPersonThreshold = 0.5f;
	
	
	//private float xAxisRot = 0f;
	//private float lookWeight;
	
	private const float TargetING_THRESHOLD = 0.1f;
	
	//public float firstPersonLookSpeed = 3f;
	
	//public Vector2 firstPersonXAxisClamp = new Vector2(-70f, 60f);
	//private float fPSRotationDegreePerSecond = 120f;
	
	
	private Vector3 curLookDir;
	private Vector3 velocityLookDir = Vector3.zero;
	//private float lookDirDampTime = 0.1f;
	
	
	//cam movement
	//private float distance;
	public float xSpeed = 50f;
	public float ySpeed = 50f;
	
	public float yMinLimit = -10f;
	public float yMaxLimit = 50f;
	
	//public float distanceMin = .5f;
	//public float distanceMax = 15f;
	
	
	float x = 0.0f;
	float y = 0.0f;
	
	public float yLerpSpeed = 0.2f;
	//private float defaultYAngle;
	
	public float defaultYAngle = 20f;
	public float distance = 6f;
	
	//cam movement end
//	
//	public CamStates Camstate {
//		get {
//			return camstate;
//		}
//	}
	
	// Use this for initialization
	void Start () {
		//charcontrol = GameObject.FindWithTag ("Player").GetComponent<CharacterController> ();
		
		//distance =  Mathf.Sqrt (Mathf.Pow(distanceAway,2)  + Mathf.Pow(distanceUp,2));
		
		follow = GameObject.FindWithTag ("Player").transform;
		lookDir = follow.forward;
		curLookDir = follow.forward;
		
		
		//defaultYAngle = Mathf.Rad2Deg * distanceUp/distanceAway;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void LateUpdate(){
		
		//float rightX = Input.GetAxis("RightStickX");
		//float rightY = Input.GetAxis("RightStickY");
		
		float leftX = Input.GetAxis("Horizontal");
		float leftY = Input.GetAxis("Vertical");
		
		//Vector3 characterOffset = follow.position + new Vector3 (0f, distanceUp, 0f);
		Vector3 characterOffset = follow.position + new Vector3 (0f, lookoffset.y, 0f);
		Vector3 lookAt = follow.position + lookoffset;
		
		camstate = CamStates.Free;
		
		if (Input.GetAxis ("CamFree") > TargetING_THRESHOLD) {
			camstate = CamStates.Target;
		} //else {
		
		
		
		switch (camstate) {
		case CamStates.Free:
			
			//analog movement
			//x += Input.GetAxis("Mouse X") * xSpeed * distance *  0.02f;
			//y -= Input.GetAxis("Mouse Y") * ySpeed * distance * 0.02f;
			
			if( Input.GetAxis("RightStickY") == 0){
				//y released needs to return to default
				//y = Mathf.Lerp(y, 0, Time.deltaTime * yLerpSpeed);
				if (y < yLerpSpeed+ defaultYAngle || Mathf.Abs(y) > defaultYAngle - yLerpSpeed){
					y = defaultYAngle;
				}else {
					y = (y > defaultYAngle ? y-yLerpSpeed : y+yLerpSpeed);
				}
			}
			else{
				y -= Input.GetAxis("RightStickY") * ySpeed * distance * 0.02f;
				//Debug.Log(y);
			}
			
			x += Input.GetAxis("RightStickX") * xSpeed * distance * 0.02f;
			//y -= Input.GetAxis("RightStickY") * ySpeed * distance * 0.02f;
			
			
			y = ClampAngle(y, yMinLimit, yMaxLimit);
			
			Quaternion rotation = Quaternion.Euler(y, x, 0);
			
			//distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel")*5, distanceMin, distanceMax);
			
			//						RaycastHit hit;
			//						if (Physics.Linecast (lookAt, transform.position, out hit)) 
			//						{
			//							distance -=  hit.distance;
			//						}
			//distance =  Mathf.Sqrt (Mathf.Pow(distanceAway,2)  + Mathf.Pow(distanceUp,2));
			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
			//Vector3 negDistance = new Vector3(0.0f, 0.0f, Mathf.Sqrt(3^2 + 1));
			Vector3 position = rotation * negDistance + lookAt;
			
			//transform.rotation = rotation;
			//transform.position = position;
			TargetPosition = position;
			
			
			
			lookDir = characterOffset - this.transform.position;
			lookDir.y = 0;
			lookDir.Normalize ();
			
			//TargetPosition = characterOffset + follow.up * distanceUp - lookDir * distanceAway;
			break;
		case CamStates.Target: 
			
			x = Vector3.Angle(follow.forward, Vector3.forward);
			if(follow.forward.x < 0){ x = 360 - x;}
			//Debug.Log(follow.forward);
			//Debug.DrawRay(follow.position, follow.forward, Color.blue);
			//Debug.DrawRay(follow.position, Vector3.forward, Color.red);
			y = defaultYAngle;
			
			Quaternion rotation2 = Quaternion.Euler(y, x, 0);
			
			
			//distance =  Mathf.Sqrt (Mathf.Pow(distanceAway,2)  + Mathf.Pow(distanceUp,2));
			Vector3 negDistance2 = new Vector3(0.0f, 0.0f, -distance);	
			Vector3 position2 = rotation2 * negDistance2 + lookAt;
			TargetPosition = position2;
			
			break;
		case CamStates.LockOn:
			break;
			
		}
		
		
		//Debug.Log (lookDir );
		
		CompensateForWalls (characterOffset, ref TargetPosition);
		SmoothPosition(this.transform.position, TargetPosition);
		
		
		transform.LookAt (lookAt);

	}
	
	private void SmoothPosition(Vector3 fromPos, Vector3 toPos){
		this.transform.position = Vector3.SmoothDamp (fromPos, toPos, ref veloityCamSmooth, camSmoothDampTime);
	}
	
	
	private void CompensateForWalls(Vector3 fromObject, ref Vector3 toTarget){
		//Debug.DrawLine (fromObject, toTarget, Color.cyan);
		
		RaycastHit wallHit = new RaycastHit ();
		//add thickness
		Vector3 direction = toTarget - fromObject;
		direction.Normalize ();
		direction *= 0.5f; 
		//

		if (Physics.Linecast (fromObject, toTarget+direction, out wallHit)) {
			Debug.DrawRay(wallHit.point, Vector3.left, Color.red);
			toTarget = new Vector3(wallHit.point.x - direction.x, toTarget.y, wallHit.point.z - direction.z);
		}

		Debug.DrawLine (fromObject, toTarget, Color.white);
		Debug.DrawLine (toTarget, toTarget+direction, Color.green);


	}
	
	//	private void FreeCamera(){
	//		lookWeight = Mathf.Lerp (lookWeight, 0f, Time.deltaTime * firstPersonLookSpeed);
	//		transform.localRotation = Quaternion.Lerp (transform.localRotation, Quaternion.identity, Time.deltaTime);
	//	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}
	
}
