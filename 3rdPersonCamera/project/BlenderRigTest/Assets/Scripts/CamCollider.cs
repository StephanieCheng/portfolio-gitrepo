﻿using UnityEngine;
using System.Collections;

public class CamCollider : MonoBehaviour {

	private CameraFollow camfollow;

	// Use this for initialization
	void Start () {
		camfollow = transform.parent.GetComponent<CameraFollow> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col){
		//Debug.Log("Triggered");
		//camfollow.OnChildTriggerStay (col);
		camfollow.SetInCamBounds (true);
	}
	void OnTriggerExit(Collider col){
		camfollow.SetInCamBounds (false);
	}
	
}
