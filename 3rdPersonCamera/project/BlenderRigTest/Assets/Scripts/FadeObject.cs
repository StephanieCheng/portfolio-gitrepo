﻿using UnityEngine;
using System.Collections;

public class FadeObject : MonoBehaviour {

	private float opacity;

	private Renderer renderer;

	private Color c;

	private float fallOff = 0.1f;

	private float targetTransparency;

	private float originalTransparency;

	// Use this for initialization
	void Start () {
		renderer = this.GetComponent<Renderer> ();
		originalTransparency = renderer.material.color.a;

	}
	
	// Update is called once per frame
	void Update () {

		if (opacity < originalTransparency)
		{
			Color C = renderer.material.color;
			C.a = opacity;
			renderer.material.color = C;
		}
		else
		{
			Color c = renderer.material.color;
			c.a = originalTransparency;
			renderer.material.color = c;
			// And remove this script
			Destroy(this);
		}
		opacity += ((1.0f-targetTransparency)*Time.deltaTime) / fallOff;


	
	}

	public void SetTransparency(float newTP){
		targetTransparency = newTP;
		//opacity -= ((1.0f-targetTransparency)*Time.deltaTime) / fallOff*2;
		opacity = targetTransparency;
	}
}
