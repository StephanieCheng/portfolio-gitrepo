﻿using UnityEngine;
using System.Collections;

public class SpawnChair : MonoBehaviour {

	public GameObject spawnObject;
	public GameObject instance;
	public float spawnRange;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown("Circle")) {

			Vector3 place = gameObject.transform.position + new Vector3(Random.Range(-spawnRange,spawnRange), 0,Random.Range(-spawnRange,spawnRange));
			instance = Instantiate(spawnObject,place, Quaternion.identity) as GameObject;

			//instance.AddComponent<Rigidbody>();

		}
	
	}
}
