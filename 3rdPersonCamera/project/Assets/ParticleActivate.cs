﻿using UnityEngine;
using System.Collections;

public class ParticleActivate : MonoBehaviour {

	public ParticleSystem[] ps;
	public MeshRenderer chair;
	private bool enableSim = true;
	private Collider col;

	// Use this for initialization
	void Start () {

		//ps = GetComponentsInChildren(typeof(ParticleSystem)) as ParticleSystem[];
		col = gameObject.GetComponentInChildren(typeof(MeshCollider)) as Collider;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Weapon") && enableSim) {
			ps[0].Play();
			ps[1].Play();
			chair.enabled = false;
			enableSim = false;
			col.enabled = false;
			foreach(Transform child in transform){
				if(child.CompareTag("LockOnTarget")){
					Destroy(child.gameObject);
				}
			}
			StartCoroutine(Destroy(ps[0].duration));


		}
			
			
	}

	IEnumerator Destroy(float duration){
		yield return new WaitForSeconds(duration);
		Destroy (gameObject);
	}
}
