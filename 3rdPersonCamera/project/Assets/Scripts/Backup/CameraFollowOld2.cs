﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

//struct CameraPosition{
//	private Vector3 position;
//	private Transform xForm;
//	
//	public Vector3 Position { get {return position;} set { position = value;}}
//	public Transform XForm {get {return xForm;} set {xForm = value;}}
//	
//	
//	public void Init(string camName, Vector3 pos, Transform transform, Transform parent){
//		position = pos;
//		xForm = transform;
//		xForm.name = camName;
//		xForm.parent = parent;
//		xForm.localPosition = Vector3.zero;
//		xForm.localPosition = position;
//		
//	}
//}


public class CameraFollowOld2 : MonoBehaviour {
	
	private enum CamStates
	{
		Free,
		FirstPerson,
		Target,
		LockOn
	}


	private CharacterControllerThirdPerson2 ccScript;


	private GameObject[] lockOnTargets;
	private GameObject lockOnTarget = null;
	public float timeToTargetSort = 2f;
	public RectTransform lockOnGraphic;
	public float lockonAnimSpee = 30f;
	private Vector3 lockOnLookAt; //between player and enemy
	//private int lockOnCurrent;
	private Vector3 smoothLookAt;
	public float lookSpeed = 20f;
	public LayerMask camObstacle;
	private RectTransform canvas;
	

	private CamStates camstate = CamStates.Free;
	
	//public float distanceAway;
	
	//public float distanceUp;
	
	
	private Vector3 lookDir;
	
	public float smooth;
	
	public Vector3 lookoffset = new Vector3(0,2.5f,0);
	private Transform follow;
	private Vector3 TargetPosition;
	

	
	private Vector3 veloityCamSmooth = Vector3.zero;
	public float camSmoothDampTime = 0.1f;
	

	
	private const float TargetING_THRESHOLD = 0.1f;
	

	
	
	private Vector3 curLookDir;
	private Vector3 velocityLookDir = Vector3.zero;
	//private float lookDirDampTime = 0.1f;
	
	
	//cam movement
	//private float distance;
	public float xSpeed = 50f;
	public float ySpeed = 50f;
	
	public float yMinLimit = -10f;
	public float yMaxLimit = 50f;
	
	//public float distanceMin = .5f;
	//public float distanceMax = 15f;
	
	
	float x = 0.0f;
	float y = 0.0f;

	float yLockOn = 0.0f;
	float xLockOn = 0.0f;

	float lockOnDistance;

	public float yLerpSpeed = 0.2f;
	//private float defaultYAngle;
	
	public float defaultYAngle = 20f;
	public float distance = 6f;
	

	
	// Use this for initialization
	void Start () {
		//charcontrol = GameObject.FindWithTag ("Player").GetComponent<CharacterController> ();
		
		//distance =  Mathf.Sqrt (Mathf.Pow(distanceAway,2)  + Mathf.Pow(distanceUp,2));
		
		follow = GameObject.FindWithTag ("Player").transform;
		lookDir = follow.forward;
		curLookDir = follow.forward;
		

		//defaultYAngle = Mathf.Rad2Deg * distanceUp/distanceAway;

		//lockOnTargets = SortLockOns ();
		lockOnGraphic.sizeDelta = Vector2.zero;

		smoothLookAt = follow.position + lookoffset;
	 

	}
	

	void Update () {
		
	}
	
	void LateUpdate(){
		
		//float rightX = Input.GetAxis("RightStickX");
		//float rightY = Input.GetAxis("RightStickY");
		
		float leftX = Input.GetAxis("Horizontal");
		float leftY = Input.GetAxis("Vertical");
		
		//Vector3 characterOffset = follow.position + new Vector3 (0f, distanceUp, 0f);
		Vector3 characterOffset = follow.position + new Vector3 (0f, lookoffset.y, 0f);
		Vector3 lookAt = follow.position + lookoffset;
		
		camstate = CamStates.Free;


		if (Input.GetButton ("LockOn")) {

			camstate = CamStates.LockOn;
			//make swiching Targets available
			//if Target dies
			
		} else {
			if (Input.GetAxis ("CamFree") > TargetING_THRESHOLD) {
				camstate = CamStates.Target;
			}
		}





		if (Input.GetButtonDown ("LockOn")) {
			lockOnTargets = SortLockOns ();
			//lockOnCurrent = 0;
	
				
			if (lockOnTargets.Length > 0){ // used to be checking for length of lockOnTargets
				lockOnTarget = lockOnTargets[0];


				//set inital angles and distance
				lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2;
					
				lockOnDistance = Vector3.Distance(lockOnLookAt, transform.position);

				//yLockon - angle distance and floor distance
				//xLockon - angle floordistance and floor forward
				Vector3 floorDistance = transform.position - lockOnLookAt;
				floorDistance.y = 0; //floor distance vector

				yLockOn = Vector3.Angle(floorDistance, (transform.position - lockOnLookAt));
					
				xLockOn = Vector3.Angle(-Vector3.forward, floorDistance);
				if(floorDistance.x > 0){
					xLockOn = 360-xLockOn;
				}

			}

		}




		
		
		switch (camstate) {
		case CamStates.Free:

			lockOnGraphic.sizeDelta = Vector2.zero;
			
			//analog movement
			//x += Input.GetAxis("Mouse X") * xSpeed * distance *  0.02f;
			//y -= Input.GetAxis("Mouse Y") * ySpeed * distance * 0.02f;
			
			if( Input.GetAxis("RightStickY") == 0){
				//y released needs to return to default

				//if (y < yLerpSpeed+ defaultYAngle || Math.Abs(y) > defaultYAngle - yLerpSpeed)
				if (y < yLerpSpeed+ defaultYAngle && y > defaultYAngle - yLerpSpeed){
					y = defaultYAngle;
				}else {
					y = (y > defaultYAngle ? y-yLerpSpeed : y+yLerpSpeed);
				}
			}
			else{
				y -= Input.GetAxis("RightStickY") * ySpeed * distance * 0.02f;
			}
			
			x += Input.GetAxis("RightStickX") * xSpeed * distance * 0.02f;
		
			
			y = ClampAngle(y, yMinLimit, yMaxLimit);
			
			Quaternion rotation = Quaternion.Euler(y, x, 0);

			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);

			Vector3 position = rotation * negDistance + lookAt; //rotate negdistance by rot around 000 and then translate in look Target direction
			

			TargetPosition = position;
			
			
			
			lookDir = characterOffset - this.transform.position;
			lookDir.y = 0;
			lookDir.Normalize ();

			CompensateForWalls (characterOffset, ref TargetPosition);
			SmoothPosition(this.transform.position, TargetPosition);

			smoothLookAt = SmoothLookingAt(smoothLookAt, lookAt, lookSpeed);


			transform.LookAt (smoothLookAt);

			break;
		case CamStates.Target: 
			
			x = Vector3.Angle(follow.forward, Vector3.forward);
			if(follow.forward.x < 0){ x = 360 - x;}
			//Debug.Log(follow.forward);
			//Debug.DrawRay(follow.position, follow.forward, Color.blue);
			//Debug.DrawRay(follow.position, Vector3.forward, Color.red);
			y = defaultYAngle;
			
			Quaternion rotation2 = Quaternion.Euler(y, x, 0);
			

			Vector3 negDistance2 = new Vector3(0.0f, 0.0f, -distance);	
			Vector3 position2 = rotation2 * negDistance2 + lookAt;
			TargetPosition = position2;

			CompensateForWalls (characterOffset, ref TargetPosition);
			SmoothPosition(this.transform.position, TargetPosition);
			
			
			transform.LookAt (lookAt);
			
			break;
		case CamStates.LockOn:

			
			//if there are enemies
			if (lockOnTargets.Length > 0){

				//two modes -  far and close TODO
				//far if distance > screen width

				//icon
				Vector2 lockOnPos = Camera.main.WorldToViewportPoint(lockOnTarget.transform.position);
				lockOnGraphic.anchorMin = lockOnPos;
				lockOnGraphic.anchorMax = lockOnPos;

				lockOnGraphic.sizeDelta = Vector2.Lerp(lockOnGraphic.sizeDelta, new Vector2(50f, 50f), lockonAnimSpee * Time.deltaTime);



				//standard lockon stuff
				xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;
				x += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;

				//y always default angle
				if (y < yLerpSpeed+ defaultYAngle && y > defaultYAngle - yLerpSpeed){
					y = defaultYAngle;
					yLockOn = (y > defaultYAngle ? yLockOn-(y-defaultYAngle) : yLockOn+(defaultYAngle-y));
				}else {
					y = (y > defaultYAngle ? y-yLerpSpeed : y+yLerpSpeed);
					yLockOn = (y > defaultYAngle ? yLockOn-yLerpSpeed : yLockOn+yLerpSpeed);
				}

				//setup lookat

				lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 


				Quaternion rotationL = Quaternion.Euler(yLockOn, xLockOn, 0);
				Vector3 negDistanceL = new Vector3(0.0f, 0.0f, -lockOnDistance);
				Vector3 positionL = rotationL * negDistanceL + lockOnLookAt;		
				TargetPosition = positionL;


				//TargetPosition = transform.position;


				CompensateForWalls (lockOnLookAt, ref TargetPosition);
				SmoothPosition(this.transform.position, TargetPosition);

				smoothLookAt = SmoothLookingAt(smoothLookAt, lockOnLookAt, lookSpeed);

				transform.LookAt (smoothLookAt);


				//Debug.DrawLine(transform.position, lockOnLookAt, Color.blue);

			} else{
				//no emenies free cam
				goto case CamStates.Free;
			}
			break;
		
			
		}
		
		
		//Debug.Log (lookDir );
		
//		CompensateForWalls (characterOffset, ref TargetPosition);
//		SmoothPosition(this.transform.position, TargetPosition);
//		
//		
//		transform.LookAt (lookAt);

	}

	private Vector3 SmoothLookingAt(Vector3 smoothLookAt, Vector3 lockOnLookAt, float lookSpeed){
		if (!CompareVectors(smoothLookAt, lockOnLookAt, 1))
					smoothLookAt = Vector3.Lerp(smoothLookAt, lockOnLookAt, lookSpeed * Time.deltaTime);
		else smoothLookAt = lockOnLookAt;
		return smoothLookAt;
	}
	
	private void SmoothPosition(Vector3 fromPos, Vector3 toPos){
		this.transform.position = Vector3.SmoothDamp (fromPos, toPos, ref veloityCamSmooth, camSmoothDampTime);
	}
	
	
	private void CompensateForWalls(Vector3 fromObject, ref Vector3 toTarget){
		//Debug.DrawLine (fromObject, toTarget, Color.cyan);
		
		RaycastHit wallHit = new RaycastHit ();
		//add thickness
		Vector3 direction = toTarget - fromObject;
		direction.Normalize ();
		direction *= 0.5f; 
		//

		if (Physics.Linecast (fromObject, toTarget+direction, out wallHit, camObstacle)) {
			Debug.DrawRay(wallHit.point, Vector3.left, Color.red);
			toTarget = new Vector3(wallHit.point.x - direction.x, toTarget.y, wallHit.point.z - direction.z);
		}

		//Debug.DrawLine (fromObject, toTarget, Color.white);
		//Debug.DrawLine (toTarget, toTarget+direction, Color.green);


	}
	
	//	private void FreeCamera(){
	//		lookWeight = Mathf.Lerp (lookWeight, 0f, Time.deltaTime * firstPersonLookSpeed);
	//		transform.localRotation = Quaternion.Lerp (transform.localRotation, Quaternion.identity, Time.deltaTime);
	//	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}

	private GameObject[] SortLockOns(){

		lockOnTargets = GameObject.FindGameObjectsWithTag ("lockOnTarget");

		lockOnTargets = lockOnTargets.OrderBy(t => Vector3.Distance(t.transform.position, follow.position + lookoffset)).ToArray();

		return lockOnTargets;

	}

//	public void NewTarget (GameObject Target){
//		lockOnTarget = Target;
//	}


	bool CompareVectors(Vector3 a,Vector3 b, float angleError){
		//if they aren't the same length, don't bother checking the rest.
		if(!Mathf.Approximately(a.magnitude, b.magnitude))
			return false;
		float cosAngleError = Mathf.Cos(angleError * Mathf.Deg2Rad);
		//A value between -1 and 1 corresponding to the angle.
		float cosAngle = Vector3.Dot(a.normalized, b.normalized);
		//The dot product of normalized Vectors is equal to the cosine of the angle between them.
		//So the closer they are, the closer the value will be to 1.  Opposite Vectors will be -1
		//and orthogonal Vectors will be 0.
		
		if(cosAngle >= cosAngleError) {
			//If angle is greater, that means that the angle between the two vectors is less than the error allowed.
			return false;
		}
		else 
			return true;
	}


	
}
