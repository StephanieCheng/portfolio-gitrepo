﻿using UnityEngine;
using System.Collections;

public class Hallway1 : MonoBehaviour {

	private Collider col;
	private CharacterControllerThirdPerson2 cc;
	private GameObject cam;
	private CameraFollow cf;

	public FixedCam fx;

	public Vector3 camPosition;
	public Transform camLookAt;

	private bool preventLoop = false;

	public float lockDuration;


	// Use this for initialization
	void Start () {
		col = gameObject.GetComponent<Collider> ();
		cc = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterControllerThirdPerson2>();
		cam = GameObject.FindGameObjectWithTag ("MainCamera") as GameObject;
		cf = cam.GetComponent<CameraFollow> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {

	}

	void OnTriggerEnter(Collider col){

		if (col.CompareTag ("Hurtbox")) {
			Debug.Log ("AreaEnter");

			if(!preventLoop){
				cf.enterArea();
				preventLoop = true;

				fx = cam.AddComponent<FixedCam>();

				fx.camSetup(camPosition, camLookAt);
				cc.LockMovement(lockDuration);
			}



		}
			


	}

	void OnTriggerExit(Collider col){

		if (col.CompareTag ("Hurtbox")) {
			Debug.Log ("AreaExit");
			//Debug.Log (fx);
			cf.exitArea ();
			Destroy (fx);
			cc.LockMovement (lockDuration);
			preventLoop = false;
		} 
			
	}
}
