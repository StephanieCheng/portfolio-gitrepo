﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

struct CameraPosition{
	private Vector3 position;
	private Transform xForm;
	
	public Vector3 Position { get {return position;} set { position = value;}}
	public Transform XForm {get {return xForm;} set {xForm = value;}}
	
	
	public void Init(string camName, Vector3 pos, Transform transform, Transform parent){
		position = pos;
		xForm = transform;
		xForm.name = camName;
		xForm.parent = parent;
		xForm.localPosition = Vector3.zero;
		xForm.localPosition = position;
		
	}
}


public class CameraFollow : MonoBehaviour {
	
	private enum CamStates
	{
		FreeCam,
		FirstPerson,
		ResetCam,
		LockOn,
		FixedCam
	}


	private CharacterControllerThirdPerson2 ccScript;


	//private GameObject[] lockOnTargets;
	private GameObject lockOnTarget = null;
	public RectTransform lockOnGraphic;
	public float lockonAnimSpee = 30f;
	private Vector3 lockOnLookAt; //between player and enemy
	//private int lockOnCurrent;
	private Vector3 smoothLookAt;
	public float lookSpeed = 20f;
	public LayerMask camObstacle;
	private RectTransform canvas;

	private Vector3 lookAt;

	public CharacterControllerThirdPerson2 playerScript;
	

	private CamStates camstate = CamStates.FreeCam;
	
	//public float distanceAway;
	
	//public float distanceUp;
	
	
	private Vector3 lookDir;
	
	public float smooth;
	
	public Vector3 lookoffset = new Vector3(0,2.5f,0);
	private Transform follow;
	private Vector3 targetPosition;
	

	
	private Vector3 veloityCamSmooth = Vector3.zero;
	private float camSmoothDampTime = 0.1f;
	

	
	private const float TARGETING_THRESHOLD = 0.1f;
	

	
	
	private Vector3 curLookDir;
	private Vector3 velocityLookDir = Vector3.zero;
	//private float lookDirDampTime = 0.1f;
	
	
	//cam movement
	//private float distance;
	public float xSpeed = 50f;
	public float ySpeed = 50f;
	
	public float yMinLimit = -10f;
	public float yMaxLimit = 50f;
	
	//public float distanceMin = .5f;
	//public float distanceMax = 15f;
	
	
	float x = 180.0f;
	float y = 0.0f;

	float yLockOn = 0.0f;
	float xLockOn = 0.0f;

	float lockOnDistance;

	public float camCloseLimit;

	bool LNcooldown = false;

	public float LNcoolDownTime;



	public float yLerpSpeed = 0.2f;
	//private float defaultYAngle;
	
	public float defaultYAngle = 20f;
	public float distance = 6f;


	public float sigmoidFalloff;

	private float heightDistance;
	private float widthDistance;
	private float calibratedDistance;

	public float camBottomExtend;
	public float farCamRangePercent;
	private float hFOV;

	private Camera cam;
	private Vector3 characterOffset;

	private GameObject camcollider;

	private bool inCamBounds = false;

	public LayerMask camFade;
	public float spherecastRadius;
	public float fullTransparencyDistance;
	
	// Use this for initialization
	void Start () {
		//charcontrol = GameObject.FindWithTag ("Player").GetComponent<CharacterController> ();
		
		//distance =  Mathf.Sqrt (Mathf.Pow(distanceAway,2)  + Mathf.Pow(distanceUp,2));
		
		follow = GameObject.FindWithTag ("Player").transform;
		lookDir = follow.forward;
		curLookDir = follow.forward;


		cam = gameObject.GetComponent("Camera") as Camera;


		float cameraHeightAt1 = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad *.5f);
		hFOV = Mathf.Atan(cameraHeightAt1 * cam.aspect) * 2 * Mathf.Rad2Deg; //needs recalc on screen change
		
		float camhorizontal = Mathf.Tan(hFOV * Mathf.Deg2Rad *0.5f) + 0.01f; //* 0.5 * 2

		camcollider = gameObject.GetComponentsInChildren<Transform>()[1].gameObject;
		camcollider.transform.localScale = new Vector3(camhorizontal,Mathf.Tan (cam.fieldOfView * Mathf.Deg2Rad * 0.5f), 0.1f);
		camcollider.transform.localPosition = new Vector3 (0, 0, cam.nearClipPlane + 0.2f);

		

		//defaultYAngle = Mathf.Rad2Deg * distanceUp/distanceAway;

		//lockOnTargets = SortLockOns ();
		lockOnGraphic.sizeDelta = Vector2.zero;

		smoothLookAt = follow.position + lookoffset;

		//calc camBottomExtend

//		Vector3 camFreePos = lookoffset + (Quaternion.AngleAxis(defaultYAngle, Vector3.right) * (- Vector3.forward * distance));
//
//		Vector3 groundToCam = camFreePos - Vector3.zero;
//
//		float gamma = Vector3.Angle (groundToCam, Vector3.up);
//		//Debug.Log (gamma);
//
//		float beta = 180 - (defaultYAngle + 90) - gamma;
//		float beta2 = cam.fieldOfView / 2 - beta;
//
//		float alfa = 180 - (90 - gamma) - beta2; 
//
//		//sin 
//
//
//		camBottomExtend = Mathf.Sin(beta2 *  Mathf.Deg2Rad) / Mathf.Sin(alfa *  Mathf.Deg2Rad) * groundToCam.magnitude; 
//		//Debug.Log (camBottomExtend);

	
	}
	

	void Update () {
		
	}

	void LateUpdate(){
		if (camstate == CamStates.FixedCam) {
			lookAt = follow.position + lookoffset;
			lockOnGraphic.sizeDelta = Vector2.zero;
			//attach external script to handle code
			return;
		}


        //float rightX = Input.GetAxis("RightStickX");
        //float rightY = Input.GetAxis("RightStickY");

        float leftX = Input.GetAxis("Horizontal");
        float leftY = Input.GetAxis("Vertical");

        //float leftX = Input.GetAxis("Mouse X");
        //float leftY = Input.GetAxis("Mouse Y");

        //Vector3 characterOffset = follow.position + new Vector3 (0f, distanceUp, 0f);
        characterOffset = follow.position + new Vector3 (0f, lookoffset.y, 0f);
		//characterOffset = follow.position + lookoffset;
		lookAt = follow.position + lookoffset;


		if (Input.GetButtonUp ("LockOn")) {
			StartCoroutine("LockOnCooldown");
			y=defaultYAngle;

			//get the x relative to xLockon
			Vector3 floorCharPos = transform.position - lookAt;
			floorCharPos.y = 0;
			
			x = Vector3.Angle(floorCharPos, -Vector3.forward);
			

			if(floorCharPos.x > 0){
				x = 360 - x;
			}

		}
		
		camstate = CamStates.FreeCam;

		if (LNcooldown)
			camstate = CamStates.LockOn;


		if (Input.GetButton ("LockOn")) {

			camstate = CamStates.LockOn;
			//make swiching targets available
			//if target dies
			
		} else {
			if (Input.GetAxis ("CamReset") > TARGETING_THRESHOLD) {
				camstate = CamStates.ResetCam;
			}
		}

	


		if (Input.GetButtonDown ("LockOn")) {
	
				
			if (lockOnTarget != null){ // used to be checking for length of lockontargets
				StopCoroutine("LockOnCooldown");
				LNcooldown = false;
				//set inital angles and distance
				lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2;
					
				//lockOnDistance = Vector3.Distance(lockOnLookAt, transform.position);

				//yLockon - angle distance and floor distance
				//xLockon - angle floordistance and floor forward
				Vector3 floorDistance = transform.position - lockOnLookAt;
				floorDistance.y = 0; //floor distance vector


				//yLockOn = Vector3.Angle(floorDistance, (transform.position - lockOnLookAt));
					
				xLockOn = Vector3.Angle(-Vector3.forward, floorDistance);
				if(floorDistance.x > 0){
					xLockOn = 360-xLockOn;
				}

			}

		}

		
		switch (camstate) {
		case CamStates.FreeCam:

			Debug.DrawLine(transform.position, lookAt, Color.blue);

			lockOnGraphic.sizeDelta = Vector2.zero;
			
			//mouse analog movement
			//x += Input.GetAxis("Mouse X") * xSpeed * distance *  0.02f;
			//y -= Input.GetAxis("Mouse Y") * ySpeed * distance * 0.02f;
			
			if( Input.GetAxis("RightStickY") == 0){
				//y released needs to return to default

				//if (y < yLerpSpeed+ defaultYAngle || Math.Abs(y) > defaultYAngle - yLerpSpeed)
				if (y < yLerpSpeed+ defaultYAngle && y > defaultYAngle - yLerpSpeed){
					y = defaultYAngle;
				}else {
					y = (y > defaultYAngle ? y-yLerpSpeed : y+yLerpSpeed);
				}
			}
			else{
				y -= Input.GetAxis("RightStickY") * ySpeed * distance * 0.02f;
			}
			
			x += Input.GetAxis("RightStickX") * xSpeed * distance * 0.02f;
		
			y = ClampAngle(y, yMinLimit, yMaxLimit);
			
			Quaternion rotation = Quaternion.Euler(y, x, 0);

			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);

			Vector3 position = rotation * negDistance + lookAt; //rotate negdistance by rot around 000 and then translate in look target direction
			

			targetPosition = position;

			
			lookDir = characterOffset - this.transform.position;
			lookDir.y = 0;
			lookDir.Normalize ();

			CompensateForWalls (characterOffset, ref targetPosition);
			SmoothPosition(this.transform.position, targetPosition);

//			if (inCamBounds){
//
//				}



			smoothLookAt = SmoothLookingAt(smoothLookAt, lookAt, lookSpeed);


			transform.LookAt (smoothLookAt);



			break;
		case CamStates.ResetCam: 
			
			x = Vector3.Angle(follow.forward, Vector3.forward);
			if(follow.forward.x < 0){ x = 360 - x;}
//			Debug.Log(follow.forward);
			//Debug.DrawRay(Vector3.zero, follow.forward, Color.blue);
			//Debug.DrawRay(Vector3.zero, Vector3.forward, Color.red);
			y = defaultYAngle;
			
			Quaternion rotation2 = Quaternion.Euler(y, x, 0);
			

			Vector3 negDistance2 = new Vector3(0.0f, 0.0f, -distance);	
			Vector3 position2 = rotation2 * negDistance2 + lookAt;
			targetPosition = position2;

			CompensateForWalls (characterOffset, ref targetPosition);
			SmoothPosition(this.transform.position, targetPosition);
			
			
			transform.LookAt (lookAt);
			
			break;
		case CamStates.LockOn:

			//if there are enemies
			if (lockOnTarget != null){

				//setup lookat
				
				lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 

				//standard lockon stuff
				//xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;

				float camCharTargetAngle = 180 - defaultYAngle;

				//float targetDistance = Vector3.Distance(lockOnTarget.transform.position, lookAt);

				float targetDistance = new Vector3(lockOnTarget.transform.position.x - lookAt.x, 0 ,lockOnTarget.transform.position.z - lookAt.z).magnitude;


				if (targetDistance > camCloseLimit){
					//farCam
					//Debug.Log("farCam");

					Vector3 floorAngle = lockOnLookAt;
					floorAngle.y = 0;
					Vector3 floorChar = lookAt;
					floorChar.y = 0;
					floorAngle -= floorChar;


					float centerAngle = Vector3.Angle(Vector3.forward, floorAngle);

					if(floorAngle.x < 0){
						centerAngle = 360 - centerAngle;
					}
					//Debug.Log(centerAngle);
					if(Mathf.Abs(xLockOn - centerAngle) > 180){
						if(xLockOn -centerAngle > 180){
							float newAngle = 360 - (xLockOn-centerAngle);
							centerAngle = xLockOn + newAngle;
						}
						else if(xLockOn -centerAngle < -180){
							float newAngle = 360 + (xLockOn - centerAngle);
							centerAngle = xLockOn - newAngle;
						}
					}
					//Debug.Log(centerAngle);



					//xlockon lerp to centerAngle
					if( Input.GetAxis("RightStickX") == 0){
						//y released needs to return to default
						
						//if (y < yLerpSpeed+ defaultYAngle || Math.Abs(y) > defaultYAngle - yLerpSpeed)
						if (xLockOn < yLerpSpeed+ centerAngle && xLockOn > centerAngle - yLerpSpeed){
							xLockOn = centerAngle;
						}else {
							xLockOn = (xLockOn > centerAngle ? xLockOn-yLerpSpeed : xLockOn+yLerpSpeed);
						}
					}
					else{
						xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;
					}

					//calculate allowed angle range


					float cameraHeightAt1 = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad *.5f);
					hFOV = Mathf.Atan(cameraHeightAt1 * cam.aspect) * 2 * Mathf.Rad2Deg;
					//Debug.Log(hFOV);
					//Debug.Log(cam.aspect);

					float horizontal = Mathf.Tan(hFOV * Mathf.Deg2Rad *.5f) * distance * farCamRangePercent * 0.01f;
					float angleLimit = Mathf.Atan(horizontal / lockOnDistance) * Mathf.Rad2Deg; //old lockondistance
					//Debug.Log(farCamRangePercent * 0.01f);
					//Debug.Log(horizontal);
					//Debug.Log(angleLimit);
					//Debug.Log(centerAngle - 15);
					//Debug.Log(centerAngle + 15);
					xLockOn = ClampAngle(xLockOn, centerAngle - angleLimit, centerAngle + angleLimit);
					//Debug.Log(xLockOn);
					//y lockon

				}else{
					//closeCam
					//Debug.Log("closeCam");
					xLockOn += Input.GetAxis("RightStickX") * xSpeed/2 * distance * 0.02f;

					//softer clamp TODO
//					if (Vector3.Distance(lockOnTarget.transform.position, lookAt) > camCloseLimit-3){
//						Debug.Log("camClampStart");
//					}

					//ylockon

				}

				//Debug.Log(xLockOn);


				//attemp 2 180-angle
				//lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 

				

				// angle between floor and char to target
				
//				Debug.DrawLine(transform.position, lockOnLookAt, Color.red);
//				Debug.DrawLine(characterOffset, lockOnLookAt, Color.blue);
//				Debug.DrawLine(lockOnLookAt, lockOnTarget.transform.position, Color.green);
//				Debug.DrawLine(characterOffset, transform.position, Color.yellow);
				
				float charTotarget = Vector3.Distance(characterOffset, lockOnLookAt);



				Vector3 charToLookVec = lockOnLookAt - characterOffset;
				
				Vector3 charToTargetAngle = charToLookVec;
				charToTargetAngle.y = 0;
				
				Vector3 crossCTT = Vector3.Cross(charToTargetAngle, charToLookVec);
				Vector3 posAngl = Quaternion.Euler(0, +90, 0) * charToTargetAngle;
				
				float charLockOnAngle = Vector3.Angle(charToTargetAngle, charToLookVec);

//				Debug.Log(Vector3.Angle(charToTargetAngle, lockOnLookAt - characterOffset));
//				Debug.DrawRay(Vector3.zero, lockOnLookAt - characterOffset, Color.cyan);
//				Debug.DrawRay(Vector3.zero, charToTargetAngle, Color.red);
//				Debug.DrawRay(Vector3.zero, crossCTT, Color.green);
//				Debug.DrawRay(Vector3.zero, posAngl , Color.magenta);



//				if(playerScript.jumping == false){
//					heightDistance = Mathf.Abs(characterOffset.y - lockOnLookAt.y); //lockOnLookAt.y
//					//Debug.Log(heightDistance);
//					widthDistance = 0;
//
//
//
//					//distance increase
//					// log(x - closecamlimit+1)
//					if(targetDistance > camCloseLimit){
//						Vector3 groundlookAt = lookAt;
//						groundlookAt.y =0;
//						Vector3 groundLockOn = lockOnLookAt;
//						groundLockOn.y = 0;
//						float groundDistanceLimit = Mathf.Sqrt (Mathf.Pow(camCloseLimit/2,2) - Mathf.Pow(Mathf.Min(heightDistance, camCloseLimit/2 - 0.01f),2) );
//						//Debug.Log(groundDistanceLimit);
//
//						float groundDistance = Vector3.Distance(groundlookAt, groundLockOn);
//
//						//Debug.Log(groundDistance);
//						if(groundDistance > groundDistanceLimit){
//							widthDistance =  Mathf.Log(groundDistance - groundDistanceLimit + 1);
//							//Debug.Log("groundInfluence");
//							//Debug.Log(Mathf.Log(groundDistance - groundDistanceLimit + 1));
//						}
//
//					}
//					calibratedDistance = distance + Mathf.Max(heightDistance, widthDistance);
//						
//
//				}

				//Debug.Log (heightDistance);

				// using virtual Free cam position

				Vector3 charToTargetProject = lockOnLookAt - characterOffset;
				charToTargetProject.y = 0; 
				Vector3 camFreePos = characterOffset + (Quaternion.AngleAxis(defaultYAngle,Quaternion.AngleAxis(90, follow.up) * charToTargetProject) * (- charToTargetProject.normalized * distance));
				Vector3 lookAtToFreeCam = camFreePos - lockOnLookAt;

				if(playerScript.jumping == false){
					Vector3 crossFreexFloor = Vector3.Cross(lookAtToFreeCam, charToTargetProject);
					Vector3 negativeAngle = Quaternion.Euler(0, -90, 0) * charToTargetProject.normalized; //lookingUp
					
					//				Debug.Log(Vector3.Angle(lookAtToFreeCam, -charToTargetProject)); //yLockOn positive
					//				Debug.DrawLine(Vector3.zero, lookAtToFreeCam, Color.red);
					//				Debug.DrawLine(Vector3.zero, -charToTargetProject, Color.blue);
					//				Debug.DrawLine(Vector3.zero, crossFreexFloor, Color.magenta);
					//				Debug.DrawLine(Vector3.zero, negativeAngle, Color.green);
					
					
					yLockOn = Vector3.Angle(lookAtToFreeCam, -charToTargetProject);
					if(Mathf.Sign(crossFreexFloor.x) == Mathf.Sign( negativeAngle.x)){
						yLockOn = 0 - yLockOn;
					}


				}
				Vector3 freeCamToChar =  characterOffset - camFreePos;
				
				Vector3 targetToChar = lockOnLookAt - characterOffset;  
				
				Vector3 crossFCTC = Vector3.Cross(freeCamToChar, targetToChar);
				Vector3 upTriangle = Quaternion.Euler(0, -90, 0) * charToTargetProject.normalized;
				
				lockOnDistance = FOVLockOnDistance(Mathf.Sign (crossFCTC.x) == Mathf.Sign (upTriangle.x));



				//if( Mathf.Sign(crossCTT.x) != Mathf.Sign( posAngl.x)){ //!= looking up, == looking down


					//Debug.Log("lookingUp");
					//camCharTargetAngle -= charLockOnAngle;



					//float trueLockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + distance*distance - 2 * distance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );
					//float gamma = Mathf.Asin(Mathf.Sin(camCharTargetAngle *  Mathf.Deg2Rad)/trueLockOnDistance * distance) * Mathf.Rad2Deg;
		

//					if(playerScript.jumping == false){
//
//
//						float gamma2 = 90 - charLockOnAngle;
//
//
//
//						yLockOn = 90 - gamma - gamma2;
//						yLockOn = 0 - yLockOn;
//
//
//
//					}
					//lockOnDistance = FOVLockOnDistance(true);
					//lockOnDistance = FOVDistance(180 - gamma - camCharTargetAngle);


					//camCharTargetAngle += defaultYAngle;
					//lockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + calibratedDistance*calibratedDistance - 2 * calibratedDistance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );
					//camCharTargetAngle -= defaultYAngle;


				//}else{
					//looking down
					//Debug.Log("lookingDown");
					//float gamma;
					//float gamma2;
				
	
					//determine triangle type

//					Vector3 charToTargetProject = lockOnLookAt - characterOffset;
//					charToTargetProject.y = 0; 
					
					//Vector3 camFreePos = characterOffset + (Quaternion.AngleAxis(defaultYAngle,Quaternion.AngleAxis(90, follow.up) * charToTargetProject) * (- charToTargetProject.normalized * distance));
//					Vector3 freeCamToChar =  characterOffset - camFreePos;
//			
//					Vector3 targetToChar = lockOnLookAt - characterOffset;  
//					
//					Vector3 crossFCTC = Vector3.Cross(freeCamToChar, targetToChar);
//					Vector3 upTriangle = Quaternion.Euler(0, -90, 0) * charToTargetProject.normalized;
//
//					lockOnDistance = FOVLockOnDistance(Mathf.Sign (crossFCTC.x) == Mathf.Sign (upTriangle.x));

					//if (Mathf.Sign (crossFCTC.x) == Mathf.Sign (upTriangle.x)) {
						//Debug.Log ("upsideTriangle");
						//camCharTargetAngle = 180 + charLockOnAngle - defaultYAngle;
						//lockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + distance*distance - 2 * distance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );



						//gamma = Mathf.Asin(Mathf.Sin(camCharTargetAngle *  Mathf.Deg2Rad)/lockOnDistance * distance) * Mathf.Rad2Deg;
						//gamma2 = 90 - charLockOnAngle - gamma - gamma;

						//Debug.Log(lockOnDistance);
						//lockOnDistance = FOVLockOnDistance(true);
						//lockOnDistance = Mathf.Max(lockOnDistance, FOVLockOnDistance());
						//lockOnDistance = Mathf.Max(lockOnDistance, FOVDistance(180 - gamma - camCharTargetAngle));
						//Debug.Log(lockOnDistance);

					//} else {
						//Debug.Log ("downsideTraingle");
						//camCharTargetAngle = 180 + defaultYAngle - charLockOnAngle;
						//lockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + distance*distance - 2 * distance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );



						//gamma = Mathf.Asin(Mathf.Sin(camCharTargetAngle *  Mathf.Deg2Rad)/lockOnDistance * distance) * Mathf.Rad2Deg;
						//gamma2 = 90 - charLockOnAngle;
						//Debug.Log(lockOnDistance);
						//lockOnDistance = FOVLockOnDistance(false);
						//lockOnDistance = Mathf.Max(lockOnDistance, FOVDistance(-(180 - gamma - camCharTargetAngle)));
						//Debug.Log(lockOnDistance);
					//}



		

					//lockOnDistance = Mathf.Max(lockOnDistance, FOVDistance(-(180 - gamma - camCharTargetAngle)));
				
//					camCharTargetAngle -= defaultYAngle;
//					lockOnDistance = Mathf.Sqrt(charTotarget * charTotarget + calibratedDistance*calibratedDistance - 2 * calibratedDistance * charTotarget * Mathf.Cos(camCharTargetAngle* Mathf.Deg2Rad) );
//					camCharTargetAngle += defaultYAngle;

				//}




				//icon
				Vector2 lockOnPos = Camera.main.WorldToViewportPoint(lockOnTarget.transform.position);
				lockOnGraphic.anchorMin = lockOnPos;
				lockOnGraphic.anchorMax = lockOnPos;

				lockOnGraphic.sizeDelta = Vector2.Lerp(lockOnGraphic.sizeDelta, new Vector2(50f, 50f), lockonAnimSpee * Time.deltaTime);

				if(LNcooldown){
					lockOnGraphic.sizeDelta = Vector2.zero;
				}


				//setup lookat

				//lockOnLookAt = (characterOffset + lockOnTarget.transform.position) / 2; 


				Quaternion rotationL = Quaternion.Euler(yLockOn, xLockOn, 0);
				Vector3 negDistanceL = new Vector3(0.0f, 0.0f, -lockOnDistance);
				Vector3 positionL = rotationL * negDistanceL + lockOnLookAt;		
				targetPosition = positionL;

				LockOnWallCollision (characterOffset, ref targetPosition);
				SmoothPosition(this.transform.position, targetPosition);

//
//				if (!WallCollision(characterOffset,targetPosition)){
//					//CompensateForWalls (lockOnLookAt, ref targetPosition);
//					//LockOnWallCollision (characterOffset, ref targetPosition);
//					//SmoothPosition(this.transform.position, targetPosition);
//
//				}

				smoothLookAt = SmoothLookingAt(smoothLookAt, lockOnLookAt, lookSpeed);
				
				transform.LookAt (smoothLookAt);


				//Debug.DrawLine(transform.position, lockOnLookAt, Color.blue);

			} else{
				//no emenies free cam
				goto case CamStates.FreeCam;
			}
			break;
		
			
		}

		camFadeObjects (this.transform.position);
		
		//Debug.Log (lookDir );
		
//		CompensateForWalls (characterOffset, ref targetPosition);
//		SmoothPosition(this.transform.position, targetPosition);
//		
//		
//		transform.LookAt (lookAt);

	}

	private float FOVLockOnDistance (bool upsideTri){
		//using angles and vectors



		Vector3 charToTargetProject = lockOnLookAt - characterOffset;
		charToTargetProject.y = 0; 
		Vector3 camFreePos = characterOffset + (Quaternion.AngleAxis(defaultYAngle,Quaternion.AngleAxis(90, follow.up) * charToTargetProject) * (- charToTargetProject.normalized * distance));
		Vector3 camToLookAt = lockOnLookAt - camFreePos; //true lock on distance

		Vector3 floorToTarget = (lockOnLookAt - follow.transform.position);
		float camfloorToTargetAngle;
		Vector3 floorExtend;

		if (upsideTri) {


			Vector3 floorToTargetfloor = floorToTarget;
			floorToTargetfloor.y = 0;
			float floorToTargetAngle = Vector3.Angle (floorToTarget, floorToTargetfloor);
			
			
			camfloorToTargetAngle = 180 - floorToTargetAngle;
			
			
			floorExtend = follow.transform.position + (-charToTargetProject.normalized * camBottomExtend);

		} else {
			floorExtend = characterOffset + (Vector3.up * camBottomExtend);

			camfloorToTargetAngle = Vector3.Angle(Vector3.up, floorToTarget);

		}



		Vector3 freeCamToChar = characterOffset - camFreePos;

		Vector3 freeCamToFloorOffset = floorExtend - camFreePos;

		float freeCamLookAtAngle = Vector3.Angle (freeCamToChar, camToLookAt);

		float freeCamToFloorOffsetAngle = Vector3.Angle (freeCamToFloorOffset, freeCamToChar);

		float beta = 180 - freeCamLookAtAngle - freeCamToFloorOffsetAngle;

		//Debug.Log (freeCamLookAtAngle);
		//Debug.Log (freeCamToFloorOffsetAngle);
		//Debug.DrawLine (characterOffset, camFreePos, Color.green);
		//Debug.DrawLine(follow.position)


		//sinus 
		
		float x = Mathf.Sin (beta * Mathf.Deg2Rad) * freeCamToFloorOffset.magnitude / Mathf.Sin (cam.fieldOfView/2 * Mathf.Deg2Rad);
		
		//cos

		float floorSide = x + camBottomExtend;
		if (!upsideTri) {
			floorSide += Vector3.Magnitude(characterOffset-follow.transform.position);
		}
		
		
		float fovDist = Mathf.Sqrt(floorSide * floorSide + floorToTarget.magnitude*floorToTarget.magnitude - 2 * floorToTarget.magnitude * floorSide * Mathf.Cos(camfloorToTargetAngle* Mathf.Deg2Rad) );

		if (camToLookAt.magnitude > fovDist)
			return camToLookAt.magnitude;
		
		
		return fovDist;


	}

	public Vector3 SmoothLookingAt(Vector3 smoothLookAt, Vector3 lockOnLookAt, float lookSpeed){
		if (!CompareVectors(smoothLookAt, lockOnLookAt, 1))
					smoothLookAt = Vector3.Lerp(smoothLookAt, lockOnLookAt, lookSpeed * Time.deltaTime);
		else smoothLookAt = lockOnLookAt;
		return smoothLookAt;
	}
	
	public void SmoothPosition(Vector3 fromPos, Vector3 toPos){
		this.transform.position = Vector3.SmoothDamp (fromPos, toPos, ref veloityCamSmooth, camSmoothDampTime);
	}

	private void camFadeObjects(Vector3 toTarget){
		RaycastHit[] hits;
		Vector3 start = toTarget + transform.forward * (cam.nearClipPlane - spherecastRadius);
		Vector3 direction = characterOffset - toTarget; 

		hits = Physics.SphereCastAll (start, spherecastRadius, direction, direction.magnitude + spherecastRadius, camFade); //unity 5.2 add QueryTriggerInteraction.Ignore

		for (int i = 0; i < hits.Length; i++){
			Renderer rend = hits[i].transform.GetComponent<Renderer>();
			if(rend){
				float alpha = 0f;
				if (hits[i].distance > fullTransparencyDistance)
					alpha = Mathf.Clamp01((hits[i].distance - fullTransparencyDistance)/ (direction.magnitude - fullTransparencyDistance));
				//Debug.Log(hits[i].distance);
				//Debug.Log(alpha);
//				Color transparent = rend.material.color;
				//transparent.a = alpha;
				//rend.material.color = transparent;

				FadeObject fo = rend.GetComponent<FadeObject>();
				if(fo == null)
					fo = rend.gameObject.AddComponent<FadeObject>();
				fo.SetTransparency(alpha);


			}
		}
	}
	
	
	private void CompensateForWalls(Vector3 fromObject, ref Vector3 toTarget){
		//Debug.DrawLine (fromObject, toTarget, Color.cyan);
		
		RaycastHit wallHit = new RaycastHit ();
		//add thickness
		//Vector3 direction = toTarget - fromObject;
		//direction.Normalize ();

		float camhorizontal = Mathf.Tan(hFOV * Mathf.Deg2Rad *0.5f) * 0.4f + 0.01f;
		//direction *= camhorizontal; 
		//

		if (Physics.Linecast (fromObject, toTarget, out wallHit, camObstacle)) {

			Vector3 direction = wallHit.normal * camhorizontal;

			//Debug.DrawRay(wallHit.point, Vector3.left, Color.red);
			if(direction.y <= 0.01f){
				toTarget = new Vector3(wallHit.point.x + direction.x, toTarget.y, wallHit.point.z + direction.z);
			}
			else
				toTarget = wallHit.point + direction;
		} 
		//Debug.Log (inCamBounds);
		if (inCamBounds) 
			handleCamBoundsCollision (ref toTarget);
 

	}

	private void LockOnWallCollision(Vector3 fromObject, ref Vector3 toTarget){
		Debug.DrawLine (fromObject, toTarget, Color.green);

		RaycastHit wallHit = new RaycastHit ();
		//add thickness
		//Vector3 direction = toTarget - fromObject;
		//direction.Normalize ();

		float camhorizontal = Mathf.Tan(hFOV * Mathf.Deg2Rad *0.5f) + 0.01f; //* 0.5 * 2
		//Debug.Log (camhorizontal);

		//direction *= camhorizontal;

		if (Physics.Linecast (fromObject, toTarget, out wallHit, camObstacle)) { //totarget + direction
			Vector3 direction = wallHit.normal;
			direction *= camhorizontal;

			Debug.DrawRay(wallHit.point, direction, Color.red);

			toTarget = new Vector3(wallHit.point.x - direction.x, wallHit.point.y, wallHit.point.z - direction.z);
			toTarget = wallHit.point + direction;
		}


	}

	private void handleCamBoundsCollision(ref Vector3 toTarget){
		
		float camHorizontal = Mathf.Tan(hFOV * Mathf.Deg2Rad *0.5f)*0.3f + 0.05f; //* 0.5 * 2
		float camVertical = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad *0.5f) *0.3f + 0.05f;
		
		Vector3 center = toTarget + transform.forward * (cam.nearClipPlane + 0.2f);
		Vector3 topRight = center + transform.up * camVertical + transform.right * camHorizontal;
		Vector3 bottomRight = center - transform.up * camVertical + transform.right * camHorizontal;
		Vector3 topLeft = center + transform.up * camVertical - transform.right * camHorizontal;
		Vector3 bottomLeft = center - transform.up * camVertical - transform.right * camHorizontal;
		
		Debug.DrawLine (topLeft ,bottomLeft , Color.magenta);
		Debug.DrawLine (topLeft ,topRight , Color.magenta);
		Debug.DrawLine (topRight ,bottomRight , Color.magenta);
		Debug.DrawLine (bottomRight ,bottomLeft , Color.magenta);
		
		
		RaycastHit camBorderWallHit = new RaycastHit ();
		if (Physics.Linecast (topRight, topLeft, out camBorderWallHit, camObstacle)) {//push cam to the right
			//Debug.Log(toTarget);
			//Debug.Log("left wall");
			toTarget += transform.right * (Vector3.Distance(topLeft, topRight) - camBorderWallHit.distance + 0.1f);
			//Debug.Log(toTarget);
			
		} else if (Physics.Linecast (topLeft, topRight, out camBorderWallHit, camObstacle)){
			//Debug.Log(toTarget);
			toTarget -= transform.right * (Vector3.Distance(topLeft, topRight) - camBorderWallHit.distance + 0.1f);
			//Debug.Log(toTarget);
			//Debug.Log("right wall");
		} else if (Physics.Linecast (topLeft, bottomLeft, out camBorderWallHit, camObstacle)){
			toTarget += transform.up * (Vector3.Distance(topLeft, bottomLeft) - camBorderWallHit.distance + 0.1f);
			//Debug.Log("floor");
		} else if (Physics.Linecast (bottomLeft, topLeft, out camBorderWallHit, camObstacle)){
			toTarget -= transform.up * (Vector3.Distance(bottomLeft, topLeft) - camBorderWallHit.distance + 0.1f);
			//Debug.Log("ceilling");
		} else if (Physics.Linecast (topRight, bottomRight, out camBorderWallHit, camObstacle)){
			toTarget += transform.up * (Vector3.Distance(topRight, bottomRight) - camBorderWallHit.distance + 0.1f);
			//Debug.Log("floor");
		} else if (Physics.Linecast (bottomRight, topRight, out camBorderWallHit, camObstacle)){
			toTarget -= transform.up * (Vector3.Distance(bottomRight, topRight) - camBorderWallHit.distance + 0.1f);
			//Debug.Log("cielling");
		} else if (Physics.Linecast (bottomRight, bottomLeft, out camBorderWallHit, camObstacle)){
			toTarget -= transform.right * (Vector3.Distance(bottomRight, bottomLeft) - camBorderWallHit.distance + 0.1f);
			//Debug.Log("right wall");
		} else if (Physics.Linecast (bottomLeft, bottomRight, out camBorderWallHit, camObstacle)){
			toTarget += transform.right * (Vector3.Distance(bottomLeft, bottomRight) - camBorderWallHit.distance + 0.1f);
			//Debug.Log("left wall");
		}

		
	}

	private bool WallCollision(Vector3 fromObject, Vector3 toTarget){
		RaycastHit wallHit = new RaycastHit ();
		if (Physics.Linecast (fromObject, toTarget, out wallHit, camObstacle)) {
			return true;
		} else
			return false;
	}

//	public void OnChildTriggerStay(Collider acol){
//		//Debug.Log (acol);
//
//		inCamBounds = true;
//
//
//
//	}



	//	private void Targetera(){
	//		lookWeight = Mathf.Lerp (lookWeight, 0f, Time.deltaTime * firstPersonLookSpeed);
	//		transform.localRotation = Quaternion.Lerp (transform.localRotation, Quaternion.identity, Time.deltaTime);
	//	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		angle= Mathf.Clamp(angle, min, max);

		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;

		return angle;
	}
	

	public void NewTarget (GameObject target){
		lockOnTarget = target;
	}

	public void NoTarget (){
		lockOnTarget = null;
	}


	IEnumerator LockOnCooldown(){
		float t = 0;
		while (t < LNcoolDownTime){
			LNcooldown = true;
			t += Time.deltaTime;
			yield return 0;
		}
		LNcooldown = false;


	}

	bool CompareVectors(Vector3 a,Vector3 b, float angleError){
		//if they aren't the same length, don't bother checking the rest.
		if(!Mathf.Approximately(a.magnitude, b.magnitude))
			return false;
		float cosAngleError = Mathf.Cos(angleError * Mathf.Deg2Rad);
		//A value between -1 and 1 corresponding to the angle.
		float cosAngle = Vector3.Dot(a.normalized, b.normalized);
		//The dot product of normalized Vectors is equal to the cosine of the angle between them.
		//So the closer they are, the closer the value will be to 1.  Opposite Vectors will be -1
		//and orthogonal Vectors will be 0.
		
		if(cosAngle >= cosAngleError) {
			//If angle is greater, that means that the angle between the two vectors is less than the error allowed.
			return false;
		}
		else 
			return true;
	}

	public void SetInCamBounds(bool set){
		inCamBounds = set;
	}

	public void enterArea(){
		camstate = CamStates.FixedCam;
	}

	public void exitArea(){
		camstate = CamStates.FreeCam;

		lookAt = follow.position + lookoffset;

		x = Vector3.Angle(follow.forward, Vector3.forward);
		if(follow.forward.x < 0){ x = 360 - x;}
		y = defaultYAngle;
		
		Quaternion rotation2 = Quaternion.Euler(y, x, 0);
		
		
		Vector3 negDistance2 = new Vector3(0.0f, 0.0f, -distance);	
		Vector3 position2 = rotation2 * negDistance2 + lookAt;
		targetPosition = position2;

		this.transform.position = targetPosition;

		this.transform.LookAt (lookAt);
		//Debug.Log (this.transform.position);
		smoothLookAt = lookAt;
	}

	public Vector3 getLookAt(){
		return smoothLookAt;
	}


	
}
