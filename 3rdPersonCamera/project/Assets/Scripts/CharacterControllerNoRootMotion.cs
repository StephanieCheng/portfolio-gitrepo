﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CharacterControllerNoRootMotion : MonoBehaviour {

	public CameraFollow camScript;

	private Animator animator;
	private float h;
	private float v;
	private Camera cam;
	private float angle;
	private CharacterController cc;
	private float animSpeed = 0;
	private float timeToSprint = 0;

	public float moveSpeed = 2f;
	public float sprintTime = 2f;

	private Vector3 moveDirection;


	public float jumpDuration = 1f;
	private bool grounded = false;
	//private bool doubleJump = false; 
	public Transform groundCheck;
	public float groundRadius;
	public LayerMask whatIsGround;
	private bool colliderGrounded;
	
	public bool jumping;
	Vector3 verticalPos;
	//public float jumpHeight = 3;
	public float jumpSpeed = 20;
	private bool falling;

	private float jumpVelocity;
	private float previousY;
	private float currentY;
	private float vSpeed; 
	private bool moving;

	//lockon
	private GameObject lockOnTarget;
	private GameObject[] lockOnTargets;
	private int lockOnCurrent = 0;
	private Vector3 lookOffset;
	

	private Rigidbody rb;

	private bool forwardLock = false; //fixed camera transition lock
	private bool actionLock = false; //attack animation spin lock
	private Vector3 moveLock;

	//public Collider hitBox; //temp solution!!

	public Transform RightHandHold;
	public Transform LeftHandHold;
	

	


	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		cam = Camera.main;
		cc = GetComponent<CharacterController> ();

		jumpVelocity = 0f;
		jumping = false;

		currentY = transform.position.y;

		rb = GetComponent<Rigidbody> ();

		lookOffset = camScript.lookoffset;

		//hitBox.enabled = false;



	}
	
	// Update is called once per frame
	void Update () {

		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");


		animSpeed = Mathf.Max (Mathf.Abs (h), Mathf.Abs (v));

		animator.SetFloat ("speed", animSpeed);

		moveDirection.Set (0, 0, 0);

		previousY = currentY;
		currentY = transform.position.y;


		vSpeed = !jumping ? 0 : currentY - previousY;
		animator.SetFloat ("vSpeed", cc.velocity.y);

		if (h != 0 || v != 0) {
			moving = true;

			//Debug.Log(animSpeed);

			animator.SetFloat ("speed", animSpeed);

			moveDirection.Set (h, 0, v);
			//Debug.LogFormat("md length {0}", moveDirection.magnitude);

			//Debug.DrawLine(Vector3.zero, moveDirection, Color.cyan);

			moveDirection.Normalize ();
			//Debug.LogFormat("md normal length {0}", moveDirection.magnitude);

			//Debug.LogFormat("h: {0}, v: {1}", h, v);
			moveDirection *= Mathf.Max (Mathf.Abs (h), Mathf.Abs (v));

			if(animSpeed < 0.1){
				moveDirection = Vector3.zero;
			}
			Debug.LogFormat("anim speed {0}", animSpeed);

			//Debug.DrawLine(Vector3.zero, moveDirection, Color.red);


			if(forwardLock){
				//Debug.Log(Vector3.Angle(moveLock, moveDirection));
				if (Vector3.Angle(moveLock, moveDirection) > 10f){
					forwardLock = false;
				}
			}


			moveDirection = Quaternion.Euler (0, cam.transform.rotation.eulerAngles.y, 0) * moveDirection;

			moveDirection *= Time.deltaTime * moveSpeed;

			//Debug.Log(moveDirection.magnitude);

			//get strin got hash and check if movement is available

			if(!animator.GetCurrentAnimatorStateInfo(4).IsName("Empty")){
				moveDirection = Vector3.zero;
			}


//			if (!forwardLock)
//				transform.LookAt (transform.position + moveDirection);



		} else {
			moving = false;
		}
		animator.SetBool ("Moving", moving);

		cc.center = new Vector3(cc.center.x, cc.center.y, animator.GetFloat("ColliderZ"));

		//Debug.Log (jumpVelocity);

		if (jumping) {


			//cc.height = animator.GetFloat("ColliderShrink");
			//cc.center = new Vector3(cc.center.x, animator.GetFloat("ColliderY"),cc.center.z);

			jumpVelocity = jumpSpeed * Time.deltaTime;


			if ((currentY - previousY) <= 0 ){

				falling = true;


			}


//			if((currentY - previousY) >= 0 && falling){
//				jumpVelocity = 0;
//				jumping = false;
//				falling = false;
//			}
		}

		animator.SetBool ("Jump", false);

		if ( grounded && Input.GetButtonDown ("Jump") && !jumping) {

			animator.SetBool("Jump", true);

		}



		

		if(animator.GetCurrentAnimatorStateInfo(0).IsName("GroundMovement") && animSpeed > 0.8){
			timeToSprint += Time.deltaTime;
		}else{
			timeToSprint = 0;
		}



		if (Input.GetButtonUp ("LockOn")) {
			lockOnTarget = null;

			animator.SetBool("LockOn", false);
		}

		if (Input.GetButtonDown ("LockOn")) {
			animator.SetBool("LockOn", true);


			lockOnCurrent = 0;
			lockOnTargets = SortLockOns();
			if(lockOnTargets.Length > 0){
				lockOnTarget = lockOnTargets[0];
				camScript.SendMessage("NewTarget", lockOnTarget);
			}
			else{
				camScript.SendMessage("NoTarget");
			}
		}

		if (Input.GetButton ("LockOn")) {
			//enable targetswitch
			if(Input.GetButtonDown("Switch")){
				lockOnCurrent++;
				if(lockOnCurrent < lockOnTargets.Length){
					lockOnTarget = lockOnTargets[lockOnCurrent];
					camScript.SendMessage("NewTarget", lockOnTarget);
				}
				else
					lockOnCurrent = 0;
				lockOnTarget = lockOnTargets[lockOnCurrent];
				camScript.SendMessage("NewTarget", lockOnTarget);
			}
			//dead Target remove TODO

			//distant Terget rmove TODO



//			if (lockOnTarget != null){ //always look at target
//				if(!jumping)
//				transform.LookAt(new Vector3(lockOnTarget.transform.position.x, transform.position.y , lockOnTarget.transform.position.z));
//			}
		

		}


		if (Input.GetButtonDown ("Triangle")) {
			animator.SetBool("Heavy", true);
			animator.SetTrigger("Attack");


		}
		if (Input.GetButtonUp ("Triangle")) {
			animator.SetBool("Heavy", false);

		}
		animator.SetBool("Light", false);
		if (Input.GetButtonDown ("Square")) {
			animator.SetBool("Light", true);
			animator.SetTrigger("Attack");

		}
	

		if (animator.GetCurrentAnimatorStateInfo (4).IsName ("Empty")) {
			actionLock = false;
			//hitBox.enabled = false;
		} else {
			actionLock = true;
			//hitBox.enabled = true;


			if (lockOnTarget != null){ //attack toward the locked on target
				if(!jumping)
					transform.LookAt(new Vector3(lockOnTarget.transform.position.x, transform.position.y , lockOnTarget.transform.position.z));
				Debug.DrawLine(transform.position, new Vector3(lockOnTarget.transform.position.x, transform.position.y , lockOnTarget.transform.position.z), Color.cyan);
			}
		}
			


		if (!forwardLock && !actionLock)
			transform.LookAt (transform.position + moveDirection); //where the character is facing
		
//		if(!jumping)
//			moveDirection = Vector3.zero;
		moveDirection.y = jumpVelocity;

		//Debug.Log (jumpVelocity);
		
		cc.Move(moveDirection);
	}

	void FixedUpdate(){
		grounded = Physics.OverlapSphere (groundCheck.position, groundRadius, whatIsGround).Length > 0;//whatIsGround if needed

		animator.SetBool ("Grounded", grounded);

		if (jumping && grounded && falling) {
			jumping = false;
			jumpVelocity = 0;
			falling = false;
		}

	}

	void OnDrawGizmos(){
		Gizmos.color = Color.yellow;
		if (grounded)
			Gizmos.color = Color.red;
		
		Gizmos.DrawSphere (groundCheck.position, groundRadius);
	}

//	void OnTriggerEnter(Collider col){
//
//	}
//	void OnTriggerExit(Collider collision){
//
//	}
	void Leap(){
		jumpVelocity = jumpSpeed * Time.deltaTime;
		jumping = true;
		currentY = transform.position.y;

		moveDirection.y = jumpVelocity;

		
		cc.Move(moveDirection);

		//Debug.Log("Leaped");
	}

	void OnCollisionEnter(){
		Debug.Log ("collided");

	}


	private GameObject[] SortLockOns(){
		
		lockOnTargets = GameObject.FindGameObjectsWithTag ("LockOnTarget");
		
		lockOnTargets = lockOnTargets.OrderBy(t => Vector3.Distance(t.transform.position, transform.position + lookOffset)).ToArray();

		//TODO limit distance (?)
		return lockOnTargets;
		
	}
//
//	public GameObject RetlockOnTarget(){
//		if (lockOnTargets.Length > 0)
//			return lockOnTargets [lockOnCurrent];
//		else
//			return null;
//
//	}
	

	public void LockMovement(float duration){
		//StopCoroutine ("LockMovementForward");
		//StartCoroutine("LockMovementForward", duration );

		forwardLock = true;
		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");
		moveLock = new Vector3(h,0,v);
		moveLock.Normalize ();

	}

//	public void UnlockMovement(){
//		StopCoroutine ("LockMovementForward");
//		forwardLock = false;
//	}
//
//	IEnumerator LockMovementForward(float duration){
//		float t = 0;
//		while (t < duration){
//			forwardLock = true;
//			t += Time.deltaTime;
//			yield return 0;
//		}
//		forwardLock = false;
//		
//		Debug.Log("Stopped");
//	}


	void RightHandHoldWeapon(){
		//hitBox.gameObject.transform.SetParent (RightHandHold, false);
		Debug.Log("SwitchedHands");
	}

	void LeftHandHoldWeapon(){
		//hitBox.gameObject.transform.SetParent (LeftHandHold, false);
		Debug.Log("SwitchedHands");
	}


}
