﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ZeroController : MonoBehaviour {

	public CameraFollow camScript;

	private Animator animator;
	private float h;
	private float v;
	private Camera cam;
	private float angle;
	private CharacterController cc;
	private float animSpeed = 0;
	private float timeToSprint = 0;

	public float moveSpeed = 2f;
	public float sprintTime = 2f;

	private Vector3 moveDirection;


	public float jumpDuration = 1f;
	private bool grounded = false;
	//private bool doubleJump = false; 
	public Transform groundCheck;
	public float groundRadius;
	public LayerMask whatIsGround;
	private bool colliderGrounded;
	
	public bool jumping;
	Vector3 verticalPos;
	//public float jumpHeight = 3;
	public float jumpSpeed = 20;
	private bool falling;

	private float jumpVelocity;
	private float previousY;
	private float currentY;
	private float vSpeed; 
	private bool moving;

	//lockon
	private GameObject lockOnTarget;
	private GameObject[] lockOnTargets;
	private int lockOnCurrent = 0;
	private Vector3 lookOffset;
	

	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		cam = Camera.main;
		cc = GetComponent<CharacterController> ();

		jumpVelocity = 0f;
		jumping = false;

		currentY = transform.position.y;

		rb = GetComponent<Rigidbody> ();

		lookOffset = camScript.lookoffset;

	}
	
	// Update is called once per frame
	void Update () {

		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");


		animSpeed = Mathf.Max (Mathf.Abs (h), Mathf.Abs (v));

		animator.SetFloat ("speed", animSpeed);

		moveDirection.Set (0, 0, 0);

		previousY = currentY;
		currentY = transform.position.y;

		vSpeed = !jumping ? 0 : currentY - previousY;
		animator.SetFloat ("vSpeed", cc.velocity.y);

		if (h != 0 || v != 0) {
			moving = true;


			animator.SetFloat ("speed", animSpeed);

			moveDirection.Set (h, 0, v);
			moveDirection.Normalize ();



			moveDirection = Quaternion.Euler (0, cam.transform.rotation.eulerAngles.y, 0) * moveDirection;

			moveDirection *= Time.deltaTime * moveSpeed;

			transform.LookAt (transform.position + moveDirection);



		} else {
			moving = false;
		}
		animator.SetBool ("Moving", moving);

		//cc.center = new Vector3(cc.center.x, cc.center.y, animator.GetFloat("ColliderZ"));

		if (jumping) {


			//cc.height = animator.GetFloat("ColliderShrink");
			//cc.center = new Vector3(cc.center.x, animator.GetFloat("ColliderY"),cc.center.z);

			jumpVelocity = jumpSpeed * Time.deltaTime;


			if ((currentY - previousY) <= 0 ){

				falling = true;


			}



			if((currentY - previousY) >= 0 && falling){
				jumpVelocity = 0;
				jumping = false;
				falling = false;
			}
		}

		animator.SetBool ("Jump", false);

		if ( grounded && Input.GetButtonDown ("Jump") && !jumping) {

			animator.SetBool("Jump", true);

		}

		if(!jumping)
			//moveDirection = Vector3.zero;

		if(animSpeed <= 0.1)
				moveDirection = Vector3.zero;
		moveDirection.y = jumpVelocity;

		cc.Move(moveDirection);

		

		if(animator.GetCurrentAnimatorStateInfo(0).IsName("GroundMovement") && animSpeed > 0.8){
			timeToSprint += Time.deltaTime;
		}else{
			timeToSprint = 0;
		}


		//NEW
		if (Input.GetButtonUp ("LockOn")) {
			lockOnTarget = null;

			animator.SetBool("LockOn", false);
		}

		if (Input.GetButtonDown ("LockOn")) {
			animator.SetBool("LockOn", true);


			lockOnCurrent = 0;
			lockOnTargets = SortLockOns();
			if(lockOnTargets.Length > 0){
				lockOnTarget = lockOnTargets[0];
				camScript.SendMessage("NewTarget", lockOnTarget);
			}
			else{
				camScript.SendMessage("NoTarget");
			}
		}

		if (Input.GetButton ("LockOn")) {
			//enable Targetswitch
			if(Input.GetButtonDown("Switch")){
				lockOnCurrent++;
				if(lockOnCurrent < lockOnTargets.Length){
					lockOnTarget = lockOnTargets[lockOnCurrent];
					camScript.SendMessage("NewTarget", lockOnTarget);
				}
				else
					lockOnCurrent = 0;
				lockOnTarget = lockOnTargets[lockOnCurrent];
				camScript.SendMessage("NewTarget", lockOnTarget);
			}
			//dead Target remove TODO

			//distant Target rmove TODO



			if (lockOnTarget != null){
				//if(!jumping)
					//transform.LookAt(new Vector3(lockOnTarget.transform.position.x, 0 , lockOnTarget.transform.position.z));
			}
		

		}
	


	}

	void FixedUpdate(){
		grounded = Physics.OverlapSphere (groundCheck.position, groundRadius, whatIsGround).Length > 0;//whatIsGround if needed

		animator.SetBool ("Grounded", grounded);

		if (jumping && grounded && falling) {
			jumping = false;
			jumpVelocity = 0;
			falling = false;
		}

	}

	void OnDrawGizmos(){
		Gizmos.color = Color.yellow;
		if (grounded)
			Gizmos.color = Color.red;
		
		Gizmos.DrawSphere (groundCheck.position, groundRadius);
	}

	void OnTriggerEnter(Collider collision){
		//Debug.Log (collision.gameObject.layer);
		if(collision.gameObject.layer == 9)
			colliderGrounded = true;
	}
	void OnTriggerExit(Collider collision){
		if(collision.gameObject.layer == 9)
			colliderGrounded = false;
	}
	void Leap(){
		jumpVelocity = jumpSpeed * Time.deltaTime;
		jumping = true;
		currentY = transform.position.y;

		moveDirection.y = jumpVelocity;

		
		cc.Move(moveDirection);
	}

	void OnCollisionEnter(){
		Debug.Log ("collided");

	}


	private GameObject[] SortLockOns(){
		
		lockOnTargets = GameObject.FindGameObjectsWithTag ("lockOnTarget");
		
		lockOnTargets = lockOnTargets.OrderBy(t => Vector3.Distance(t.transform.position, transform.position + lookOffset)).ToArray();

		//TODO limit distance (?)
		return lockOnTargets;
		
	}
//
//	public GameObject RetlockOnTarget(){
//		if (lockOnTargets.Length > 0)
//			return lockOnTargets [lockOnCurrent];
//		else
//			return null;
//
//	}

}
