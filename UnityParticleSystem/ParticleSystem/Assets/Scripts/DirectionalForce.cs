﻿using UnityEngine;
using System.Collections;

public class DirectionalForce : MonoBehaviour {
	public Vector3 direction;
	public float amount;
	private Vector3 force;
	//private GameObject[] emitters;
	//private GameObject[] partices;


	ArrayList particles;

	// Use this for initialization
	void Start () {
		force = direction.normalized * amount * 0.03f;
		//emitters = GameObject.FindGameObjectsWithTag("Emitter");
		particles = new ArrayList ();
	}
	
	// Update is called once per frame
	void Update () {
		force = direction.normalized * amount * 0.03f;
//		GameObject[] partices = GameObject.FindGameObjectsWithTag("Particle");
		foreach (GameObject i in particles){
			i.SendMessage("ApplyForce", force);
		}
	
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.white;
		Gizmos.DrawRay(transform.position, 10*direction);
	}

	public void GiveForce(GameObject particle){
		particles.Add (particle);
	}

	public Vector3 getForce(){
		return force;
	}
}
