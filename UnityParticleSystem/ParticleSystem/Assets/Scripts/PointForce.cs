﻿using UnityEngine;
using System.Collections;

public class PointForce : MonoBehaviour {
	public float amount;
	public float radius;
	private Vector3 force;
	private Vector3 direction;

	private ArrayList particles;

	// Use this for initialization
	void Start () {

		particles = new ArrayList ();
	
	}
	
	// Update is called once per frame
	void Update () {

		foreach(GameObject i in particles){
			direction = i.transform.position - transform.position;
			if(direction.magnitude < radius){
				force = direction.normalized * amount * 0.03f / direction.magnitude;
				i.SendMessage("ApplyForce", force);
			}
		}
	
	}

	void OnDrawGizmos(){
		Gizmos.color = new Color (1f,1f,1f, 0.2f);
		Gizmos.DrawSphere(transform.position, radius);
	}

	public void GiveForce(GameObject par){
		particles.Add (par);
	}
}
