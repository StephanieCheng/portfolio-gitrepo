﻿using UnityEngine;
using System.Collections;

public class Emitter : MonoBehaviour {
	private Mesh mesh;
	//public Particle particle;
	private SpriteRenderer sr;
	public Transform particleSprite;
	private int instanceCount;
	public int spawnCount;
	public float frameSpawn;
	private float createPerFrame;
	public GameObject[] forces;
	

	private Transform particleTrans;
	private ArrayList particles;

	// Use this for initialization
	void Start () {
		mesh = GetComponent<MeshFilter> ().mesh;
		instanceCount = 0;
		createPerFrame = frameSpawn * spawnCount;


	}
	
	// Update is called once per frame
	void Update () {


		if(instanceCount < spawnCount)
			for(int i = 0; i < createPerFrame; i++){
				Transform par = Instantiate (particleSprite, GeneratePosition (), Quaternion.identity) as Transform;
				instanceCount++;
				foreach(GameObject j in forces){
					j.SendMessage("GiveForce", par.gameObject);
				}
			}

		//Debug.Log(instanceCount);


	
	}

	public Vector3 GeneratePosition(){
		float xPos = transform.position.x + (Random.value * mesh.bounds.size.x * transform.localScale.x) - transform.localScale.x * mesh.bounds.size.x/2;
		float yPos = transform.position.y + (Random.value * mesh.bounds.size.y * transform.localScale.x) - transform.localScale.y * mesh.bounds.size.y/2;
		float zPos = transform.position.z + (Random.value * mesh.bounds.size.z * transform.localScale.x) - transform.localScale.z * mesh.bounds.size.z/2;
		return new Vector3 (xPos, yPos, zPos);
	}

	//public RebornParticle(){

	//}
}
