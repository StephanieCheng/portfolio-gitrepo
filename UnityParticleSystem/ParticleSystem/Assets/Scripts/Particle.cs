﻿using UnityEngine;
using System.Collections;

public class Particle : MonoBehaviour {
	public float lifetime;
	private Vector3 velocity;
	private Vector3 acceleration;
	private Vector3 location;
	private Vector3 initLoc;
	private SpriteRenderer sprite;

	private float initLifetime;
	public Transform lookAt;
	public float mass;
	public Vector3 gravity;
	private bool collided;
	public float meltDur;

	private bool insideMesh;
	private bool stick;

	public float collisionMargin;

	private GameObject[] surfaces;

	// Use this for initialization
	void Start () {
		location = transform.position;
		acceleration = Vector3.zero;
		velocity = Vector3.zero;
		acceleration = gravity;
		initLoc = location;
		collided = false;
		insideMesh = false;
		stick = false;

		initLifetime = lifetime;

		lookAt = GameObject.FindGameObjectWithTag("MainCamera").transform;

		surfaces = GameObject.FindGameObjectsWithTag("SnowSurface");

//		Mesh mesh = surfaces [0].GetComponent<MeshFilter> ().mesh;
//		Vector3[] ver = mesh.vertices;
//		Debug.Log (ver [0]);
//		Debug.Log (surfaces [0].transform.TransformPoint (ver [0]));

		sprite = gameObject.GetComponent<SpriteRenderer>();
	
	}
	
	// Update is called once per frame
	void Update () {
		lifetime -= Time.deltaTime;

		if (lifetime <= 0) {
			StartCoroutine("Melt");
			//emitterScript.SendMessage("RebornParticle");
			lifetime = initLifetime;
		}


		//acceleration += gravity;
		velocity += acceleration * Time.deltaTime * mass;
	
		if(!stick)
			for (int i = 0; i < surfaces.Length; i++) {
				SurfaceCollision(surfaces[i]);
			}

//		if (transform.position.y <= collisionMargin ) {
//			stickToSurface(new Vector3(transform.position.x, 0f, transform.position.z));
//		}

		if (collided) {
			velocity = Vector3.zero;
			location += Vector3.zero;
			transform.position = location;
			//transform.LookAt (Vector3.up);
		} else {
			location += velocity;
			transform.position = location;
			transform.LookAt (lookAt.position);
		}
	

		transform.LookAt (lookAt.position);
		acceleration = gravity;


	}

	void SurfaceCollision(GameObject surface){
		Mesh mesh = surface.GetComponent<MeshFilter> ().mesh;



		insideMesh = IsPointInside (mesh , transform.position, surface);
		if (insideMesh) {
			Ray ray = new Ray(transform.position, -velocity*10);//direction of movement hopefully
			//Debug.DrawRay(transform.position, -velocity*10, Color.red);
			Vector3[] verts = mesh.vertices;
			int[] tris = mesh.triangles;
			int triangleCount = tris.Length / 3;
			for (int i = 0; i < triangleCount; i++)
			{

				Vector3 V1 = surface.transform.TransformPoint(verts[ tris[ i*3     ] ]);
				Vector3 V2 = surface.transform.TransformPoint(verts[ tris[ i*3 + 1 ] ]);
				Vector3 V3 = surface.transform.TransformPoint(verts[ tris[ i*3 + 2 ] ]);

				Plane P = new Plane(V1,V2,V3);
				//if (P.GetSide(transform.position))
				float distance;
				P.Raycast(ray, out distance);
				Vector3 intersection = ray.GetPoint(distance);


//				Debug.Log(PointInTriangle(intersection ,  
//				                          intersection + new Vector3(1f, 0f, 0f), 
//				                          intersection + new Vector3(-0.5f, 0f, 0.5f ), 
//				                          intersection + new Vector3(-0.5f, 0f, -0.5f)));

//				Debug.Log("tri:");
//				Debug.Log(V1);
//				Debug.Log(V2);
//				Debug.Log(V3);

				bool hit = PointInTriangle(intersection, V1, V2, V3 );

//				Debug.Log(hit);
				if (hit){
					//Debug.Log("hit");
					//intersection is our new location
					StickToSurface(intersection);
					insideMesh = false;
					return;

				}

			}

		}
	}

	IEnumerator Melt(){
		while (sprite.color.a >= 0) {
			sprite.color -= new Color(0f,0f,0f, 1/meltDur);
			yield return 0;  
		}
		sprite.color = new Color(1f,1f,1f, 1f);;
		location = initLoc;
		//lifetime = initLifetime;
		velocity = Vector3.zero;
		collided = false;
		transform.position = location;
		stick = false;
	}
	

	void StickToSurface(Vector3 contact){
		//Debug.Log (contact);
		collided = true;
		transform.position = contact + new Vector3(0f, collisionMargin, 0f);
		location = transform.position;
		stick = true;
	}

	public void ApplyForce(Vector3 force){
		acceleration += force / mass;

	}

	public static bool IsPointInside(Mesh aMesh, Vector3 aLocalPoint, GameObject surface)
	{
		Vector3[] verts = aMesh.vertices;
		int[] tris = aMesh.triangles;
		int triangleCount = tris.Length / 3;
		for (int i = 0; i < triangleCount; i++)
		{
			Vector3 V1 = surface.transform.TransformPoint(verts[ tris[ i*3     ] ]);
			Vector3 V2 = surface.transform.TransformPoint(verts[ tris[ i*3 + 1 ] ]);
			Vector3 V3 = surface.transform.TransformPoint(verts[ tris[ i*3 + 2 ] ]);
			var P = new Plane(V1,V2,V3);
			if (P.GetSide(aLocalPoint))
				return false;
		}
		return true;
	}

	bool SameSide(Vector3 p1, Vector3 p2,Vector3 a,Vector3 b){
		Vector3 cp1 = Vector3.Cross (b - a, p1 - a);
		Vector3 cp2 = Vector3.Cross (b - a, p2 - a);
		if (Vector3.Dot (cp1, cp2) >= 0) 
			return true;
		else 
			return false;

	}
			
	bool PointInTriangle(Vector3 p,Vector3 a, Vector3 b,Vector3 c){
		if (SameSide (p, a, b, c) && SameSide (p, b, a, c) && SameSide (p, c, a, b)) 
			return true;
		else 
			return false;
	}
	


}
