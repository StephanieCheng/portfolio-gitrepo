### Low Poly Models
![img](https://i.imgur.com/Cj9vzpa.png)

![img](https://i.imgur.com/t6xFN6f.png)

![img](https://i.imgur.com/N8ohCLD.png)

![img](https://i.imgur.com/1FpEU9M.png)

![img](https://i.imgur.com/HhBURAu.png)

![img](https://i.imgur.com/fNpsclJ.png)

### Model Expressions
![img](https://i.imgur.com/jyIDlqh.png)

![img](https://i.imgur.com/RxQvLKo.png)

![img](https://i.imgur.com/TAoeYGA.png)

![img](https://i.imgur.com/vH00wKC.png)

![img](https://i.imgur.com/x9z6HaZ.png)

![img](https://i.imgur.com/iyYBaML.png)

### Zweihander

![gif1](https://imgur.com/1TdHk54.png)

### Updated WIP
![gif1](https://imgur.com/kJUrLgq.png)

### WIP

![gif1](https://imgur.com/xK3YkFG.png)

### Slime Castle Character
![gif1](https://imgur.com/e3nNioF.png)  
![gif1](https://imgur.com/TQiQ9rI.png)

### Gun

![gif1](https://imgur.com/8C35I67.jpg)


### Crab

--low poly with baked normals
![gif1](https://imgur.com/STu3zLd.png)

  