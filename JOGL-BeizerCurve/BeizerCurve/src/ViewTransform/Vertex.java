package ViewTransform;

import java.util.Vector;

import com.jogamp.opengl.GL2;

public class Vertex {
	private float x;
	private float y;
	private float z;
	private float h;
	
	private float transx;
	private float transy;
	private float transz;
	private float transh;

	public Vertex(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.h = 1;
		
		this.transx = x;
		this.transy = y;
		this.transz = z;
		this.transh = h;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float getH() {
		return h;
	}

	public void setH(float h) {
		this.h = h;
	}
	
	
	
	public float getTransx() {
		return transx;
	}

	public void setTransx(float transx) {
		this.transx = transx;
	}

	public float getTransy() {
		return transy;
	}

	public void setTransy(float transy) {
		this.transy = transy;
	}

	public float getTransz() {
		return transz;
	}

	public void setTransz(float transz) {
		this.transz = transz;
	}

	public float getTransh() {
		return transh;
	}

	public void setTransh(float transh) {
		this.transh = transh;
	}
	
	
	public Vector<Float> getVector3(){
		Vector<Float> v = new Vector<Float>();
		v.add(x);
		v.add(y);
		v.add(z);
		return v;
	}
	
	public float[] getVectorArray3(){
		float[] v = new float[3];
		v[0] =x;
		v[1] =y;
		v[2] =z;
		return v;
	}

	public void drawPoint(GL2 gl){
		gl.glVertex3f(this.transx, this.transy, this.transz);
	}

	public void transform(float[][] t, float[][] p) {
		
		//System.out.printf("Transformed: %f %f %f ->", this.x, this.y, this.z);

		float[] vec = {this.x, this.y, this.z};
		
		System.out.printf("vert - %f %f %f\n", x, y, z);
		
		Matrix.printVector(vec);
		
		vec = Matrix.multiply(t, vec);
		
		
		Matrix.printMatrix(t);
		Matrix.printVector(vec);
		
		//mnozenje s projekcijskom
		
		float[] vech = {vec[0], vec[1], vec[2], 1.0f};
		
		vech = Matrix.multiply(p, vech);
		
		//vracanje u radni prostor
		
		vec[0] = vech[0]/ vech[3];
		vec[1] = vech[1]/vech[3];
		
		this.x = (float) vec[0];
		this.y = (float) vec[1];
		this.z = (float) vec[2];
		
		
		//System.out.printf(" %f %f %f\n", this.x, this.y, this.z);
		return;
		
	}

	public void draw2dPoint(GL2 gl) {
		gl.glVertex2f(this.transx, this.transy);
		
	}
	
	

}
