package ViewTransform;

/*import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;*/

import java.util.Iterator;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
















import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

public class BasicFrame {
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
				Scene scene = ReadFile.Read();
				//Vector<Vertex> verticies = scene.getVerticies();
				//Vector<FaceTri> faces = scene.getFaces();
				//Vertex ociste = scene.getOciste();
				//Vertex glediste = scene.getGlediste();
				float h = scene.getH();
				
				GLProfile glprofile = GLProfile.getDefault();
				GLCapabilities glcapabilities = new GLCapabilities(glprofile);
				final GLCanvas glcanvas = new GLCanvas(glcapabilities);
				
				glcanvas.addGLEventListener(new GLEventListener() {
					
					@Override
					public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width,
							int height) {
						GL2 gl2 = glautodrawable.getGL().getGL2();
						GLU glu = GLU.createGLU(gl2);
						gl2.glMatrixMode (GL2.GL_PROJECTION);
						gl2.glLoadIdentity();
						
						//gl2.glFrustumf(-1.2f, 1.2f, -1.2f, 1.2f, 5.0f, 40.0f); //near?
						glu.gluOrtho2D(0.0f , width , 0.0f , height) ;
						//glu.gluPerspective(45.0, aspect, 0.1, 100.0);
						gl2.glMatrixMode(GL2.GL_MODELVIEW);
						gl2.glViewport(0, 0,  width, height);
						
						
					}
					
					@Override
					public void init(GLAutoDrawable arg0) {
					}
					
					@Override
					public void dispose(GLAutoDrawable arg0) {
					}
					
					@Override
					public void display(GLAutoDrawable glautodrawable) {
						GL2 gl2 = glautodrawable.getGL().getGL2();
						GLU glu = GLU.createGLU(gl2);
						
						Vector<Vertex> verticies = scene.getVerticies();
						Vector<FaceTri> faces = scene.getFaces();
						Vertex ociste = scene.getOciste();
						Vertex glediste = scene.getGlediste();
						float h = scene.getH();
						
						// osiguravamo bijelu pozadinu
						gl2.glClearColor(1, 1, 1, 1.0f);
						gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
						gl2.glLoadIdentity();
						
						//glu.gluLookAt( 0.0f , 0.0f , 10.0f , 0.0f , 0.0f , 0.0f , 0.0f , 1.0f , 0.0f ); //up the z 
				         //r e n d e r S c e n e ( ) ;
				        //gl u t S w a p B u f fe r s ( ) ;
						
						/*gl2.glColor3f(1.0f, 0.2f, 0.2f);
				        gl2.glPushMatrix();
				        gl2.glScalef(10.0f, 10.0f, 10.0f);
				        Objekt.kocka1(gl2);
				        gl2.glPopMatrix();*/
				        

				        /*gl2.glColor3f(0.0f, 0.2f, 1.0f);
				        gl2.glPushMatrix();
				        gl2.glTranslatef(10.0f, 0.0f, 0.0f);
				        gl2.glRotatef(30.0f, 0.0f, 0.0f, 1.0f);
				        gl2.glScalef(5.0f, 5.0f, 5.0f);
				        Objekt.kocka1(gl2);
				        gl2.glPopMatrix();*/
						
						//System.out.printf("onr\n\n");
				        
						//get matrix transforms
						 /*gl2.glColor3f(0.0f, 0.2f, 1.0f);
					     gl2.glPushMatrix();
					     gl2.glTranslatef(-ociste.getX(), -ociste.getY(), -ociste.getZ());*/
						//GLint m_viewport[4];

						//gl2.glGetIntegerv( GL.GL_VIEWPORT, m_viewport );
						
						int width = glautodrawable.getSurfaceWidth();
					    int height = glautodrawable.getSurfaceHeight();
						
						ViewTransformation.ViewTransform(gl2, ociste, glediste, verticies, scene, width, height);
						
						//draw object
						gl2.glColor3f(0.0f, 0.2f, 1.0f);
						gl2.glPushMatrix();
						Objekt.drawObjektTri(gl2, faces);
						Objekt.drawObjektTri2d(gl2, faces);
						gl2.glPopMatrix();
				        

						/*gl2.glColor3f(1.0f, 0.0f, 0.0f);
						gl2.glPointSize (4.0f);
						gl2.glPushMatrix();
						gl2.glBegin (GL.GL_LINE_LOOP);
						gl2.glVertex2i (100,50);
						gl2.glVertex2i (100,130);
						gl2.glVertex2i (150,130);
						gl2.glEnd ();
						gl2.glPopMatrix();*/
						
						/*gl2.glMatrixMode (GL2.GL_MODELVIEW) ;
				        gl2.glLoadIdentity();
				        
				        gl2.glMatrixMode (GL2.GL_PROJECTION) ;
				        gl2.glLoadIdentity();*/
				        
				        gl2.glFlush();
					}
				});
				
				final JFrame jframe = new JFrame("Projekcija");
				jframe.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				jframe.getContentPane().add(glcanvas);
				jframe.setSize(500, 500);
				jframe.setVisible(true);
				glcanvas.requestFocusInWindow();
			}
		});
	}
}