package ViewTransform;



	/*************************************************************************
	 *  Compilation:  javac Matrix.java
	 *  Execution:    java Matrix
	 *
	 *  A bare-bones collection of static methods for manipulating
	 *  matrices.
	 *
	 *************************************************************************/

	public class Matrix {

	    // return a random m-by-n matrix with values between 0 and 1
	    public static double[][] random(int m, int n) {
	        double[][] C = new double[m][n];
	        for (int i = 0; i < m; i++)
	            for (int j = 0; j < n; j++)
	                C[i][j] = Math.random();
	        return C;
	    }

	    // return n-by-n identity matrix I
	    public static double[][] identity(int n) {
	        double[][] I = new double[n][n];
	        for (int i = 0; i < n; i++)
	            I[i][i] = 1;
	        return I;
	    }

	    // return x^T y
	    public static double dot(double[] x, double[] y) {
	        if (x.length != y.length) throw new RuntimeException("Illegal vector dimensions.");
	        double sum = 0.0;
	        for (int i = 0; i < x.length; i++)
	            sum += x[i] * y[i];
	        return sum;
	    }

	    // return C = A^T
	    public static double[][] transpose(double[][] A) {
	        int m = A.length;
	        int n = A[0].length;
	        double[][] C = new double[n][m];
	        for (int i = 0; i < m; i++)
	            for (int j = 0; j < n; j++)
	                C[j][i] = A[i][j];
	        return C;
	    }

	    // return C = A + B
	    public static double[][] add(double[][] A, double[][] B) {
	        int m = A.length;
	        int n = A[0].length;
	        double[][] C = new double[m][n];
	        for (int i = 0; i < m; i++)
	            for (int j = 0; j < n; j++)
	                C[i][j] = A[i][j] + B[i][j];
	        return C;
	    }

	    // return C = A - B
	    public static double[][] subtract(double[][] A, double[][] B) {
	        int m = A.length;
	        int n = A[0].length;
	        double[][] C = new double[m][n];
	        for (int i = 0; i < m; i++)
	            for (int j = 0; j < n; j++)
	                C[i][j] = A[i][j] - B[i][j];
	        return C;
	    }

	    // return C = A * B
	    public static double[][] multiply(double[][] A, double[][] B) {
	        int mA = A.length;
	        int nA = A[0].length;
	        int mB = B.length;
	        int nB = B[0].length;
	        if (nA != mB) throw new RuntimeException("Illegal matrix dimensions.");
	        double[][] C = new double[mA][nB];
	        for (int i = 0; i < mA; i++)
	            for (int j = 0; j < nB; j++){
	            	C[i][j] = 0;
	                for (int k = 0; k < nA; k++){
	                    C[i][j] += (A[i][k] * B[k][j]);
	                    //System.out.printf("%f[%d][%d] += %f[%d][%d] * %f[%d][%d]\n", C[i][j], i, j, A[i][k], i, k,  B[k][j], k, j);
	                    }
	                }
	        return C;
	    }
	 // return C = A * B
	    public static float[][] multiply(float[][] A, float[][] B) {
	        int mA = A.length;
	        int nA = A[0].length;
	        int mB = B.length;
	        int nB = B[0].length;
	        if (nA != mB) throw new RuntimeException("Illegal matrix dimensions.");
	        float[][] C = new float[mA][nB];
	        for (int i = 0; i < mA; i++){
	            for (int j = 0; j < nB; j++){
	                for (int k = 0; k < nA; k++){
	                    C[i][j] += (A[i][k] * B[k][j]); 
	                    //System.out.printf("%f[%d][%d] += %f * %f\n", C[i][j], i, j, A[i][k], B[k][j]);
	                   
	                    }
	                }
	            }
	        return C;
	    }


	    // matrix-vector multiplication (y = A * x)
	    public static double[] multiply(double[][] A, double[] x) {
	        int m = A.length;
	        int n = A[0].length;
	        if (x.length != n) throw new RuntimeException("Illegal matrix dimensions.");
	        double[] y = new double[m];
	        for (int i = 0; i < m; i++)
	            for (int j = 0; j < n; j++)
	                y[i] += (A[i][j] * x[j]);
	        return y;
	    }
	 // matrix-vector multiplication (y = A * x)
	    public static float[] multiply(float[][] A, float[] x) {
	        int m = A.length;
	        int n = A[0].length;
	        if (x.length != n) throw new RuntimeException("Illegal matrix dimensions.");
	        float[] y = new float[m];
	        for (int i = 0; i < m; i++)
	            for (int j = 0; j < n; j++)
	                y[i] += (A[i][j] * x[j]);
	        return y;
	    }


	    // vector-matrix multiplication (y = x^T A)
	    public static double[] multiply(double[] x, double[][] A) {
	        int m = A.length;
	        int n = A[0].length;
	        if (x.length != m) throw new RuntimeException("Illegal matrix dimensions.");
	        double[] y = new double[n];
	        for (int j = 0; j < n; j++)
	            for (int i = 0; i < m; i++)
	                y[j] += (A[i][j] * x[i]);
	        return y;
	    }
	 // vector-matrix multiplication (y = x^T A)
	    public static float[] multiply(float[] x, float[][] A) {
	        int m = A.length;
	        int n = A[0].length;
	        if (x.length != m) throw new RuntimeException("Illegal matrix dimensions.");
	        float[] y = new float[n];
	        for (int j = 0; j < n; j++)
	            for (int i = 0; i < m; i++)
	                y[j] += (A[i][j] * x[i]);
	        return y;
	    }

	    // test client
	    /*public static void main(String[] args) {
	        System.out.println("D");
	        System.out.println("--------------------");
	        double[][] d = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
	        StdArrayIO.print(d);
	        System.out.println();

	        System.out.println("I");
	        System.out.println("--------------------");
	        double[][] c = Matrix.identity(5);
	        StdArrayIO.print(c);
	        System.out.println();

	        System.out.println("A");
	        System.out.println("--------------------");
	        double[][] a = Matrix.random(5, 5);
	        StdArrayIO.print(a);
	        System.out.println();

	        System.out.println("A^T");
	        System.out.println("--------------------");
	        double[][] b = Matrix.transpose(a);
	        StdArrayIO.print(b);
	        System.out.println();

	        System.out.println("A + A^T");
	        System.out.println("--------------------");
	        double[][] e = Matrix.add(a, b);
	        StdArrayIO.print(e);
	        System.out.println();

	        System.out.println("A * A^T");
	        System.out.println("--------------------");
	        double[][] f = Matrix.multiply(a, b);
	        StdArrayIO.print(f);
	        System.out.println();
	    }*/
	    
	    public static void printMatrix(double[][] t1){
	    	
	    	for (int i = 0; i < t1.length ; i++){
	    		for (int j = 0; j < t1[0].length; j++){
	    			System.out.printf("%f ", t1[i][j]);
	    		}
	    		System.out.printf("\n");
	    	}
	    	System.out.printf("\n");
	    }
	    
	    
	    public static void printVector(double[] t){
	    	for (int i = 0; i < t.length ; i++){
	    		System.out.printf("%f ", t[i]);
	    	}
	    	System.out.printf("\n");
	    }
	    
public static void printMatrix(float[][] t1){
	    	
	    	for (int i = 0; i < t1.length ; i++){
	    		for (int j = 0; j < t1[0].length; j++){
	    			System.out.printf("%f ", t1[i][j]);
	    		}
	    		System.out.printf("\n");
	    	}
	    	System.out.printf("\n");
	    }
	    
	    
	    public static void printVector(float[] t){
	    	for (int i = 0; i < t.length ; i++){
	    		System.out.printf("%f ", t[i]);
	    	}
	    	System.out.printf("\n\n");
	    }
	    
	    
	}



