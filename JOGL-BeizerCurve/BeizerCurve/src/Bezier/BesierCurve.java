package Bezier;

import java.util.Vector;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import FrontFace.Vertex;

public class BesierCurve {
	private Vector<Vertex> r; //vectorfloat is a vertex
	private int n;
	

	public BesierCurve(Vector<Vertex> r) {
		this.r = r;
		n = r.size() -1;
	}


	public Vector<Vertex> getR() {
		return r;
	}


	public void setR(Vector<Vertex> r) {
		this.r = r;
	}
	
	
	public int getN() {
		return n;
	}


	public void setN(int n) {
		this.n = n;
	}


	public Vertex getPt(float t){
		
		
		float[] point = null;
		
		float[] bin = new float[n+1];
		float[][] ri = new float[n+1][3];
		for (int i = 0; i < n+1; i++){
			ri[i] = r.get(i).getVectorArray3();
			bin[i] = getBin(i,t);
		}
		
		//System.out.printf("ri matrix: \n");
		//Matrix.printMatrix(ri);
		point = Matrix.multiply(bin, ri);
		
		Vertex v = new Vertex (point[0],  point[1], point[2]);
		return v;
	}
	
	public float getBin(int i, float t){
		//i - 0-n ri
		//n - stupanj
		return (float) (binomial(n,i) * Math.pow(t, i) * Math.pow(1-t, n-i));
	}

	
	public int binomial(int n, int k){
		//System.out.printf("Binominal: (%d %d)\n", n, k);
		if (k == 0)
		    return 1;
		    else if (2*k > n)
		    	return binomial(n,n-k);
		else{
		    int e = n-k+1;
		    for (int i = 2; i < k+1; i++){
		        e *= (n-k+i);
		        e /= i;
		    }
		    
		    return e;
		}

	}


	public void displayCurve(GL2 gl2) {
		gl2.glColor3f(1.0f, 0.0f, 0.0f);
		gl2.glPushMatrix();
		gl2.glBegin (GL.GL_LINE_STRIP);
		for(int i = 0; i < this.getN()+1; i++){
			this.getR().get(i).drawPoint(gl2);				
		}
		gl2.glEnd ();
		gl2.glPopMatrix();
		
		
		//get points
		
		gl2.glColor3f(0.0f, 0.0f, 1.0f);
        gl2.glPushMatrix();
        gl2.glBegin(GL.GL_POINTS);
		for(float t = 0 ; t <= 1; t+=0.01){
			//System.out.printf("i :  %f\n", t);
			Vertex p = this.getPt(t);
			p.drawPoint(gl2);
		}
		gl2.glEnd();
		gl2.glPopMatrix();
		
	}
}
