package Bezier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector;

import FrontFace.Vertex;



public class ReadBesier {

	public static BesierCurve ReadBeiserPoly() {
		String filename = "src/files/beiser1.txt";
		
		/*Scanner in = new Scanner(System.in);
		filename = in.nextLine();
		in.close();*/
		
		File filepath = new File(filename);
		
		BesierCurve c = null;
		Vector<Vertex> ve = new Vector<Vertex>();
		
		try {
			Scanner s = new Scanner(filepath);
			
			String line;
			
			while(s.hasNextLine()){
				line = s.nextLine();
				
				Scanner li = new Scanner(line);
				String part = li.next();
				if (part.equals("v")){
					Vertex v = new Vertex(li.nextFloat(), li.nextFloat(), li.nextFloat());
					ve.add(v);
					//System.out.printf("ri : %f %f %f\n", v.getX(), v.getY(), v.getZ());
				}
			}
			
			
			c = new BesierCurve(ve);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
				
		return c;

	}

}
