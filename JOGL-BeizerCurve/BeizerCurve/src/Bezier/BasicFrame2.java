package Bezier;



/*//import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;*/

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import FrontFace.Vertex;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

public class BasicFrame2 {
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
				BesierCurve curve = ReadBesier.ReadBeiserPoly();
				
				GLProfile glprofile = GLProfile.getDefault();
				GLCapabilities glcapabilities = new GLCapabilities(glprofile);
				final GLCanvas glcanvas = new GLCanvas(glcapabilities);
				
				glcanvas.addGLEventListener(new GLEventListener() {
					
					@Override
					public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width,
							int height) {
						GL2 gl2 = glautodrawable.getGL().getGL2();
						gl2.glMatrixMode (GL2.GL_PROJECTION);
						gl2.glLoadIdentity();

						GLU glu = new GLU();
						//glu.gluOrtho2D(0.0f , width , height , 0.0f) ;
						gl2.glFrustumf(-1.2f, 1.2f, -1.2f, 1.2f, 0.5f, 40.0f);
						gl2.glMatrixMode (GL2.GL_MODELVIEW);
						gl2.glLoadIdentity();
						gl2.glViewport(0, 0, width, height);
					}
					
					@Override
					public void init(GLAutoDrawable arg0) {
					}
					
					@Override
					public void dispose(GLAutoDrawable arg0) {
					}
					
					@Override
					public void display(GLAutoDrawable glautodrawable) {
						GL2 gl2 = glautodrawable.getGL().getGL2();
						GLU glu = new GLU();
						
						// osiguravamo bijelu pozadinu
						gl2.glClearColor(1, 1, 1, 1.0f);
						gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
						gl2.glLoadIdentity();
						
						glu.gluLookAt( 2.0f , 0.0f , 4.0f , 0.0f , 0.0f , 0.0f , 0.0f , 0.5f , 1.0f );
						
						/*gl2.glColor3f(1.0f, 0.0f, 0.0f);
						gl2.glPointSize (4.0f);
						gl2.glPushMatrix();
						gl2.glBegin (GL.GL_LINE_LOOP);
						gl2.glVertex2i (100,50);
						gl2.glVertex2i (100,130);
						gl2.glVertex2i (150,130);
						gl2.glEnd ();
						gl2.glPopMatrix();
						
						
						gl2.glColor3f(1.0f, 0.2f, 0.2f);
				        gl2.glPushMatrix();
				        gl2.glScalef(5.0f, 5.0f, 5.0f);
				        gl2.glBegin(GL.GL_LINE_LOOP);
						gl2.glVertex3d(-1, 1, -1);
						gl2.glVertex3d(1, 1, -1);
						gl2.glVertex3d(1, -1, -1);
						gl2.glVertex3d(-1, -1, -1);
						gl2.glEnd();
						gl2.glPopMatrix();
						*/
						
						//System.out.printf("n :  %d",  curve.getN());
						//display polygon
						
						curve.displayCurve(gl2);
						
						gl2.glFlush();
				        
					}
				});
				
				final JFrame jframe = new JFrame("beiser");
				jframe.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				jframe.getContentPane().add(glcanvas);
				jframe.setSize(500, 500);
				jframe.setVisible(true);
				glcanvas.requestFocusInWindow();
			}
		});
	}
}