package FrontFace;

import java.util.Vector;



public class FaceTri {
	private Vertex v1;
	private Vertex v2;
	private Vertex v3;

	public FaceTri(Vertex v1, Vertex v2, Vertex v3) {
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
	}

	public Vertex getV1() {
		return v1;
	}

	public void setV1(Vertex v1) {
		this.v1 = v1;
	}

	public Vertex getV2() {
		return v2;
	}

	public void setV2(Vertex v2) {
		this.v2 = v2;
	}

	public Vertex getV3() {
		return v3;
	}

	public void setV3(Vertex v3) {
		this.v3 = v3;
	}

	public FaceTri transform(float[][] t, float[][] p) {
		
		return null;
	}
	
	public boolean isFront2D(Vertex ociste){
		//normala * centar-ociste
		
		//normala v1v2 x v2v3
		float[] a = new float[3]; //v1v2
		float[] b = new float[3];//v2v3
		
		a[0] = v2.getX() - v1.getX();
		a[1] = v2.getY() - v1.getY();
		a[2] = v2.getZ() - v1.getZ();
		
		b[0] = v3.getX() - v2.getX();
		b[1] = v3.getY() - v2.getY();
		b[2] = v3.getZ() - v2.getZ();
		
		float[] normala = Matrix.cross3d(a, b); // v2-?
		//centar -ociste
		
		float[] centar = findCenter();
		
		
		float[] oc  = new float[3];
		
		oc[0] = ociste.getX() - centar[0];
		oc[1] = ociste.getY() - centar[1];
		oc[2] = ociste.getZ() - centar[2];
		
		float s = Matrix.dot(normala, oc);
		
		if (s >= 0)
			return true;
		else
			return false;
		
	}
	
	public float[] findCenter(){
		float[] centar = new float[3];
		
		centar[0] = (v1.getX()+v2.getX()+v3.getX())/3;
		centar[1] = (v1.getY()+v2.getY()+v3.getY())/3;
		centar[2] = (v1.getZ()+v2.getZ()+v3.getZ())/3;	
		
		return centar;
	}
	

}
