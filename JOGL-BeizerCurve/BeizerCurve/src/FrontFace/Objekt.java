package FrontFace;

import java.util.Vector;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;


public class Objekt {

	public static void Kocka(float w, GL2 gl){
		float wp = w/2.0f ;
		// gornja s t r a n i c a
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex3d(-wp, -wp, wp);
		gl.glVertex3d(wp, -wp, wp);
		gl.glVertex3d(wp, wp, wp);
		gl.glVertex3d(-wp, wp, wp);
		gl.glEnd();
		
		// donja s t r a n i c a
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex3d(-wp, wp, -wp);
		gl.glVertex3d(wp, wp, -wp);
		gl.glVertex3d(wp, -wp, -wp);
		gl.glVertex3d(-wp, -wp, -wp);
		gl.glEnd();
		
		// desna s t r a n i c a
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex3d(wp, wp, -wp);
		gl.glVertex3d(-wp, wp, -wp);
		gl.glVertex3d(-wp, wp, wp);
		gl.glVertex3d(wp, wp, wp);
		gl.glEnd();
		
		// l i j e v a s t r a n i c a
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex3d(wp, -wp, wp);
		gl.glVertex3d(-wp, -wp, wp);
		gl.glVertex3d(-wp,-wp, -wp);
		gl.glVertex3d(wp, -wp, -wp);
		gl.glEnd();
		
		// prednja s t r a n i c a
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex3d(wp, -wp, -wp);
		gl.glVertex3d(wp, wp, -wp);
		gl.glVertex3d(wp, wp, wp);
		gl.glVertex3d(wp, -wp, wp);
		gl.glEnd();
		
		// s t r a z n j a s t r a n i c a
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex3d(-wp, -wp, wp);
		gl.glVertex3d(-wp, wp, wp);
		gl.glVertex3d(-wp, wp, -wp);
		gl.glVertex3d(-wp, -wp, -wp);
		gl.glEnd();
		
		
	}

	public static void kocka1(GL2 gl) {
		Objekt.Kocka(1.0f, gl);
		
	}

	public static void drawObjektTri(GL2 gl, Vector<FaceTri> faces, Vertex ociste){
		for (FaceTri f : faces){
			if (f.isFront2D(ociste)){
			gl.glBegin(GL2.GL_POLYGON);
			
			f.getV1().drawPoint(gl);
			f.getV2().drawPoint(gl);
			f.getV3().drawPoint(gl);
			gl.glEnd();
			}
		}
	}

	public static void drawObjektTri2d (GL2 gl, Vector<FaceTri> faces){
		for (FaceTri f : faces){
			
			gl.glBegin(GL.GL_LINE_LOOP);
			f.getV1().draw2dPoint(gl);
			f.getV2().draw2dPoint(gl);
			f.getV3().draw2dPoint(gl);
			gl.glEnd();
			
		}
		
	}
}
