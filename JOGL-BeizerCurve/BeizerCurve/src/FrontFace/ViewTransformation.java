package FrontFace;

import java.util.Iterator;
import java.util.Vector;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

public class ViewTransformation {

	public static void ViewTransform(GL2 gl2, Vertex ociste, Vertex glediste, Vector<Vertex> verticies, Scene scene) {
		
		float h = scene.getH();
		
		//Vector<Vector<Integer>> facedependancy = scene.getFacedependancy();


		//pomicanje u ishodiste
		//float[][] T1 = {{1.0f, 0.0f, 0.0f},{0.0f, 1.0f, 0.0f},{-ociste.getX(), -ociste.getY(), -ociste.getZ()}}; 4x4mat
		
		float[] G1 = {glediste.getX() - ociste.getX(), glediste.getY() - ociste.getY(), glediste.getZ() - ociste.getZ()};
		
		//gl2.glTranslatef(-ociste.getX(), -ociste.getY(), -ociste.getZ());
		
		//rotiranje oko z osi
		
		float sina = G1[1] / ( scene.getLinearDistance(G1[0], G1[1]));
		
		//System.out.printf("sina : %f\n\n", sina);
		
		float cosa = G1[0] / ( scene.getLinearDistance(G1[0], G1[1]));
		
		float[][] T2 = {{cosa, -sina, 0.0f},{sina, cosa, 0.0f},{0.0f, 0.0f, 1.0f}};
		
		float[] G2 = {scene.getLinearDistance(G1[0], G1[1]), 0.0f, G1[2]};					
		
		//rotacija oko y osi
		
		float sinb = G2[0] / ( scene.getLinearDistance(G2[0], G2[2]));
		
		float cosb = G2[2] / ( scene.getLinearDistance(G2[0], G2[2]));
		
		float[][] T3 = {{cosb, 0.0f, sinb},{0.0f, 1.0f, 0.0f}, {-sinb, 0.0f, cosb}};
		
		float[] G3 = {0.0f, 0.0f, scene.getLinearDistance(G2[0], G2[2])};
		
		//podudaranje x osi
		float[][] T4 = {{0.0f, -1.0f, 0.0f},{1.0f, 0.0f, 0.0f},{0.0f, 0.0f, 1.0f}};
		float[][] T5 = {{-1.0f, 0.0f, 0.0f},{0.0f, 1.0f, 0.0f},{0.0f, 0.0f, 1.0f}};
		
		
		//System.out.printf("%f = %f * %f\n\n", -0.5f * 0.0f, -0.5f, 0.0f);
		
		float[][] T = Matrix.multiply(T2, T3);
		T = Matrix.multiply(T, T4);
		T = Matrix.multiply(T, T5);//bez matrice t1

		//projection matrix
		float[][] P = {{1.0f, 0.0f, 0.0f, 0.0f},{0.0f, 1.0f, 0.0f, 0.0f},{0.0f, 0.0f, 0.0f, 1/h}, {0.0f, 0.0f, 0.0f, 0.0f}};
		
		
		//print matrices
		/*Matrix.printMatrix(T1);
		Matrix.printMatrix(T2);
		Matrix.printMatrix(T3);
		Matrix.printMatrix(T4);
		Matrix.printMatrix(T5);
		Matrix.printMatrix(P);
		Matrix.printMatrix(T);*/
		
		//transform object
		
		//Vector<Vertex> verts = (Vector<Vertex>) verticies.clone();
		
		//draw ociste glediste
		
		gl2.glColor3f(0.0f, 1.0f, 1.0f);
		gl2.glPushMatrix();
		gl2.glBegin(GL.GL_LINE_STRIP);
		gl2.glVertex3f(0, 0, 0);
		gl2.glVertex3f(G3[0], G3[1], G3[2]);
		gl2.glEnd();
		gl2.glPopMatrix();
		
		//draw z axis
		/*gl2.glColor3f(1.0f, 0.0f, 1.0f);
		gl2.glPushMatrix();
		gl2.glBegin(GL.GL_LINE_STRIP);
		gl2.glVertex3f(0, 0, 0);
		gl2.glVertex3f(0, 0, 5);
		
		gl2.glEnd();
		gl2.glPopMatrix();*/
		
		
		for (Iterator<Vertex> v = verticies.iterator(); v.hasNext();){
			Vertex a = v.next();
			//System.out.printf("vert - %f %f %f\n", a.getX(), a.getY(), a.getZ());
			
			
			float[] vec = {a.getX(), a.getY(), a.getZ()};
			//System.out.printf("vert - %f %f %f\n", vec[0], vec[1], vec[2]);
			
			
			//Matrix.printVector(vec);
			
			vec[0] -= ociste.getX();
			vec[1] -= ociste.getY();
			vec[2] -= ociste.getZ();
			
			//System.out.printf("vert - %f %f %f\n", vec[0], vec[1], vec[2]);
			
			
			vec = Matrix.multiply(vec, T);
			//vec = Matrix.multiply(vec, T3);
			
			
			
			
			//Matrix.printMatrix(T3);
			//Matrix.printVector(vec);
			
			//mnozenje s projekcijskom
			
			//float[] vech = {vec[0], vec[1], vec[2], 1};
			
			float[] vech = {vec[0], vec[1], 0, vec[2]/h};
			
			//System.out.printf("vech - %f %f %f %f\n", vech[0], vech[1], vech[2], vech[3]);
			
			//vech = Matrix.multiply(P, vech);
			//Matrix.printMatrix(P);
			//Matrix.printVector(vech);		
			
			
			//vracanje u radni prostor
			
			vec[0] = vech[0]/ vech[3];
			vec[1] = vech[1]/vech[3];
			
			//normalizacija koordinata
			
			vec[0] = (( 2.0f*vec[0] / (1.2f+1.2f) ) - (1.2f-1.2f)/(1.2f+1.2f));
			vec[1] = ((2.0f * vec[1])/(1.2f+1.2f)) - (0.0f);
			
			a.setTransx(vec[0]);
			a.setTransy(vec[1]);
			a.setTransz(vec[2]);
			
			//sized do velicine prozora
			
			//[-1 1] -> [0,2]
			
			vec[0] += 1;
			vec[1] += 1;
			
			//[0,2] -> [0, 500]
			
			vec[0] *= 250;
			vec[1] *= 250;
			
			
			//a.setZ(vec[2]);
			
			
			//System.out.printf("vert - %f %f %f\n\n", vec[0], vec[1], vec[2]);
			
			
			
			//a.transform(T, P);
		}
	}

}
