package FrontFace;

import java.util.Vector;

public class Scene {
	private Vector<Vertex> verticies;
	
	private Vector<FaceTri> faces;
	
	private Vertex ociste;
	
	private Vertex glediste;
	
	private Vector<Vector<Integer>> facedependancy;

	public Scene(Vertex ociste, Vertex glediste, Vector<Vertex> verticies, Vector<FaceTri> faces, Vector<Vector<Integer>> facedependancy) {
		this.verticies = verticies;
		this.ociste =ociste;
		this.glediste = glediste;
		this.faces = faces;
		this.facedependancy = facedependancy;
		
	}

	public Vector<Vertex> getVerticies() {
		return verticies;
	}

	public void setVerticies(Vector<Vertex> verticies) {
		this.verticies = verticies;
	}

	public Vector<FaceTri> getFaces() {
		return faces;
	}

	public void setFaces(Vector<FaceTri> faces) {
		this.faces = faces;
	}

	public Vertex getOciste() {
		return ociste;
	}

	public void setOciste(Vertex ociste) {
		this.ociste = ociste;
	}

	public Vertex getGlediste() {
		return glediste;
	}

	public void setGlediste(Vertex glediste) {
		this.glediste = glediste;
	}
	
	
	
	public Vector<Vector<Integer>> getFacedependancy() {
		return facedependancy;
	}

	public void setFacedependancy(Vector<Vector<Integer>> facedependancy) {
		this.facedependancy = facedependancy;
	}

	public float getH(){
		return (float) Math.sqrt( Math.pow(ociste.getX() - glediste.getX(), 2) + Math.pow(ociste.getY() - glediste.getY(), 2) + Math.pow(ociste.getZ() - glediste.getZ(), 2));
	}
	public float getDistance(Vertex o, Vertex g){
		return (float) Math.sqrt( Math.pow(o.getX() - g.getX(), 2) + Math.pow(o.getY() - g.getY(), 2) + Math.pow(o.getZ() - g.getZ(), 2));
	}
	
	public float getLinearDistance(double g2, double g22){
		return (float) Math.sqrt( Math.pow(g2, 2) + Math.pow(g22, 2) );
	}
	
	public Vertex getCenter(){
		float[] c = new float[3];
		c[0] = 0;
		c[1] = 0;
		c[2] = 0;
		float[] tmp;
		for (int i = 0; i < faces.size(); i++){
			tmp = faces.get(i).findCenter();
			c[0] += tmp[0];
			c[1] += tmp[1];
			c[2] += tmp[2];
		}
		c[0] /= faces.size();
		c[1] /= faces.size();
		c[2] /= faces.size();
		Vertex o = new Vertex(c[0], c[1], c[2]);
		return o;
	}
	
	public void positionObj(Vertex origin){
		float[] transVec = new float[3];
		Vertex c = getCenter();
		
		transVec[0] = origin.getX() - c.getX();
		transVec[1] = origin.getY() - c.getY();
		transVec[2] = origin.getZ() - c.getZ();
		
		for (int i = 0; i < verticies.size(); i++){
			verticies.get(i).setX(verticies.get(i).getX() + transVec[0]);
			verticies.get(i).setY(verticies.get(i).getY() + transVec[1]);
			verticies.get(i).setZ(verticies.get(i).getZ() + transVec[2]);
		}
		
		return;
		
	}
	
	

}
