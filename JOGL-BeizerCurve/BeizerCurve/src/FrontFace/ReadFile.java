package FrontFace;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector;

public class ReadFile {

	public static Scene Read() {


		String filename = "src/files/kocka.txt";
		
		/*Scanner in = new Scanner(System.in);
		filename = in.nextLine();
		in.close();*/
		
		File filepath = new File(filename);
		
		
		
		Vector<Vertex> verticies = new Vector<Vertex>();
		verticies.add(new Vertex(0.0f, 0.0f, 0.0f)); //so that we start from 1
		
		Vector<FaceTri> faces = new Vector<FaceTri>();
		
		Vertex ociste = null;
		
		Vertex glediste = null;
		
		Vector<Vector<Integer>> facedependancy = new Vector<Vector<Integer>>();
		
		try {
			Scanner s = new Scanner(filepath);
			
			String line;
			
			while(s.hasNextLine()){
				line = s.nextLine();
				
				Scanner li = new Scanner(line);
				String part = li.next();
				if (part.equals("o")){
					ociste = new Vertex(li.nextFloat(), li.nextFloat(),li.nextFloat());
				}
				else if (part.equals("g")){
					glediste = new Vertex(li.nextFloat(), li.nextFloat(),li.nextFloat());
				}
				else if(part.equals("v")){
					Vertex v = new Vertex(li.nextFloat(), li.nextFloat(),li.nextFloat());
					verticies.add(v);
				}
				else if (part.equals("f")){
					Vector<Integer> dep = new Vector<Integer>();
					dep.add(li.nextInt());
					dep.add(li.nextInt());
					dep.add(li.nextInt());
					FaceTri f = new FaceTri(verticies.get(dep.get(0)), verticies.get(dep.get(1)) , verticies.get(dep.get(2)));
					faces.add(f);
					facedependancy.add(dep);
				}
			}
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Scene scene = new Scene(ociste, glediste, verticies, faces, facedependancy);
		
		
		return scene;
		
	}

}
