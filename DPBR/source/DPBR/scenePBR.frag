#version 400 core


//out vec4 FragColor;
//in vec2 TexCoords;
//in vec3 WorldPos;
//in vec3 Normal;
uniform vec3 camPos;
  
//uniform vec3  albedo;
//uniform float metallic;
//uniform float roughness;
//uniform float ao;

//OR
//uniform sampler2D albedoMap;
//uniform sampler2D normalMap;
//uniform sampler2D metallicMap;
//uniform sampler2D roughnessMap;
//uniform sampler2D aoMap;

in vec2 texcoord;

out vec3 FragColor;

uniform sampler2D gPosition;
uniform sampler2D gNormals;
uniform sampler2D gColor;
uniform sampler2D gMaterial; //r - metallic, g - roughness, b - ao

uniform samplerCube envmap;
uniform samplerCube irrmap;

uniform vec3 lightPosition;
uniform vec3 lightColor;
uniform float lightIntensity;
uniform int lightType;

const float PI = 3.14159265359;
const int u_NumSamples = 100;

float D_GGX_TR(vec3 N, vec3 H, float a);
float DistributionGGX(vec3 N, vec3 H, float roughness);
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
vec3 fresnelSchlick(float cosTheta, vec3 F0);
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness);

float GGX(float nDotV, float a);
float G_Smith(float a, float nDotV, float nDotL);
vec3 ImportanceSampleGGX( vec2 Xi, float Roughness, vec3 N );
vec3 SpecularIBL( vec3 SpecularColor, float Roughness, vec3 N, vec3 V );
vec2 Hammersley(uint i, uint N);


void main()
{
	
	//texttures needed - albedo, normal, metallic, roughness, AO
	vec3 normal = texture2D(gNormals, texcoord).xyz;
	vec3 fragPos = texture2D(gPosition, texcoord).xyz;
	vec3 albedo = texture2D(gColor, texcoord).rgb;
	float metallic = texture2D(gMaterial, texcoord).r;
	float roughness = texture2D(gMaterial, texcoord).g;
	float ao = texture2D(gMaterial, texcoord).b;
	
	//PBR textures
	//vec3 albedo     = pow(texture(albedoMap, TexCoords).rgb, 2.2);
    //vec3 normal     = getNormalFromMap();
    //float metallic  = texture(metallicMap, TexCoords).r;
    //float roughness = texture(roughnessMap, TexCoords).r;
    //float ao        = texture(aoMap, TexCoords).r;
	
	
	//TEMP
	//metallic = 1;
	//roughness = 0.1;
	//ao = 1; //white ao
	//albedo = vec3(1.0, 1.0, 1.0);
	
	metallic = clamp(metallic, 0.01, 0.99);
	roughness = clamp(roughness, 0.01, 0.99);
	
	//srgb to linear

	albedo.r = pow(albedo.r, 2.2);
	albedo.g = pow(albedo.g, 2.2);
	albedo.b = pow(albedo.b, 2.2);
	
	vec3 color = vec3(0.0);
	
	if(lightType == 1){
	

		vec3 N = normalize(normal);
		vec3 V = normalize(camPos - fragPos);
		//vec3 V = normalize(fragPos);
		vec3 L = normalize(lightPosition - fragPos);
		vec3 H = normalize(V + L);
		
		//we assume 0.04 is good enough for dielectrics, but for metals we use the albedo
		
		vec3 F0 = vec3(0.04);
		F0      = mix(F0, albedo, metallic);
		//vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness); 
		vec3 F = fresnelSchlick((max(N,H), 0.0), F0);
		
		
		vec3 kS = F; 									// reflection/specular fraction
		vec3 kD = vec3(1.0) - kS;                      // refraction/diffuse  fraction
		kD *= 1.0 - metallic;  							//metallic surfaces refract nothing
		
		//TEMP
		//vec3  lightColor  = vec3(23.47f, 21.31f, 20.79f);
		
		//vec3  wi          = normalize(lightPosition - fragPos);
		//float cosTheta    = max(dot(N, wi), 0.0f);
		//float attenuation = calculateAttenuation(fragPos, lightPosition);
		//float radiance    = lightColor * attenuation * cosTheta;
		
		
		
		//reflectance equation Lo (p,wo) = Integral ( fr(p,wi,wo) * Li(p,wi) * dot(n,wi) * dwi)
		//radial flux is simply RGB
		//solid angle w 
		//radial intensity -> I = d(radialflux)/dw
		//radiance -> L = d^2 (radial flux) / (dA dw cos theta) -- total observed energy over area A over w with radflux
		
		//float cosTheta = dot(lightDir, N);  
		
		//float cosTheta = dot(lightDir, N); -- dot(n,wi) in reflectance equation (moved out of radiance)
		//for single point -> w direction, A is point p
		//irradiance - sum of all incoming light on point p
		//Lo the result of reflectance is the refllected sum of lights irradiance on point p as viewed from wo
		//our A is a hemisphere around th normal
		//instead of an integral we use the nummerical solution Riemann sum
		
		
		//int steps = 100;
		//float sum = 0.0f;
		//vec3 P    = ...;
		//vec3 Wo   = ...;
		//vec3 N    = ...;
		//float dW  = 1.0f / steps;
		//for(int i = 0; i < steps; ++i) 
		//{
		//	vec3 Wi = getNextIncomingLightDir(i);
		//	sum += Fr(p, Wi, Wo) * L(p, Wi) * dot(N, Wi) * dW;
		//}
		
		
		//loop over all light sources
		vec3 Lo = vec3(0.0);
		//for(int i = 0; i < 4; ++i) 
		//{
		//vec3 L = normalize(lightPosition - fragPos);
		//vec3 H = normalize(V + L);
	 
		float distance    = length(lightPosition - fragPos) / lightIntensity;
		float attenuation = 1.0 / (distance * distance);
		vec3 radiance     = lightColor * attenuation; 
		
		float NDF = DistributionGGX(N, H, roughness);       
		float G   = GeometrySmith(N, V, L, roughness); 
		
		//cook-torrace
		vec3 nominator    = NDF * G * F;
		float denominator = 4 * max(dot(V, N), 0.0) * max(dot(L, N), 0.0) + 0.001; 
		vec3 brdf         = nominator / denominator;  

		//each lights contribution
	  
		float NdotL = max(dot(N, L), 0.0);        
		Lo += (kD * albedo / PI + brdf) * radiance * NdotL;
		//}
		
		color = Lo;
	
	}
	
	//ambient term 
	
	else if( lightType == 0){

		/*vec3 N = normalize(normal);
		vec3 V = normalize(camPos - fragPos);
	
		vec3 F0 = vec3(0.04);
		F0 = mix(F0, albedo, metallic);
		
		vec3 kS = vec3(0.0);

		vec3 radiance = vec3(0);
		float NdotV = clamp(dot(N, V), 0.0, 1.0);

		int sampleCount = 1;

		for (int i = 0; i < sampleCount; ++i)
		{
			vec2 rand = vec2(23.14069263277926, 2.665144142690225);
			float e1 = fract(cos(dot(texcoord,rand)) * 123456.);
			float e2 = fract(cos(dot(texcoord,rand)) * 123458.);

			float theta = atan((roughness * sqrt(e1)) / (sqrt(1-e1)));
			float phi = 2 * PI * e2;
			vec3 temp = cos(phi) * normalize(cross(N, vec3(0.0, 1.0, 0.0))) + sin(phi) * vec3(0.0, 1.0, 0.0);
			vec3 X = cos(theta) * normal + sin(theta) * temp;

			X = normalize(X);

			vec3 H = normalize(X + V);
			float cosT = clamp(dot(X, N), 0.0, 1.0);
			float sinT = sqrt(1 - cosT * cosT);

			vec3 fresnel = fresnelSchlick(clamp(dot(H, V), 0.0, 1.0), F0);
			float geometry = GeometrySmith(N, V, H, roughness);// * GeometrySmith(N, X, H, roughness);
			float denominator = clamp(4 * (NdotV * clamp(dot(H, N), 0.0, 1.0) + 0.05), 0.0, 1.0);
			kS += fresnel;

			radiance += texture(envmap, X).rgb * geometry * fresnel * sinT / denominator;
		}

		kS = clamp(kS / sampleCount, 0.0, 1.0);

		vec3 specular  = radiance / sampleCount;

		vec3 kD = vec3(1.0) - kS;
		kD *= 1.0 - metallic;

		vec3 irradiance = texture(irrmap, N).rgb;
		vec3 diffuse = albedo * irradiance;

		color = kD * diffuse + kS * specular;*/

		
		vec3 N = normalize(normal);
		vec3 V = normalize(camPos - fragPos);
		vec3 H = normalize(V + N);
		//vec3 R = -normalize(V-2*dot(V, N)*N);
		
		//color = albedo * lightColor * ao;
		//color = texture(irrmap, N).rbg * albedo * ao;
		//vec3 color   = ambient + Lo; 
	
	
		/////
		
		vec3 F0 = vec3(0.04);
		F0      = mix(F0, albedo, metallic);
		//vec3 F  = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness); 
		vec3 F = fresnelSchlick((max(N,H), 0.0), F0);
		
		vec3 kS = F; 									// reflection/specular fraction
		vec3 kD = vec3(1.0) - kS;                      // refraction/diffuse  fraction
		kD *= 1.0 - metallic;  		
		
		vec3 irradiance = texture(irrmap, N).rgb;
		
		vec3 diffuse = albedo * irradiance;
	 
		vec3 radiance = irradiance;
		
		float NDF = DistributionGGX(N, H, roughness);       
		float G   = GeometrySmith(N, V, H, roughness); 
		
		//cook-torrace
		float nominator    = NDF * G;
		float denominator = 4 * max(dot(V, N), 0.0) * max(dot(H, N), 0.0) + 0.001; 
		float specular         = nominator / denominator; 

		float NdotH = max(dot(N, H), 0.0);
		//color = (kD * diffuse / PI + kS * specular) * radiance * NdotH;
		
		//
		V = normalize(camPos - fragPos);
		N = normalize(normal);
		color = SpecularIBL(vec3(1.0, 0.6, 0.6), roughness, N, V);

	}
	
	//gamma correction
	//Lo grows high but gets clamped due to LDR, we need HDR
	
	color = color / (color + vec3(1.0));
	color = pow(color, vec3(1.0/2.2)); 
	
	//final output
	//FragColor = vec4(color, 1.0);
	FragColor = color;

	//FragColor = vec3(texture2D(gNormals, texcoord));


	//FragColor = vec3(texture2D(gNormals, texcoord));

	
	//The reflectance equation sums up the radiance of all incoming light directions ωi
	//over the hemisphere Ω scaled by fr that hit point p
	//and returns the sum of reflected light Lo in the viewer's direction. 
	
	
	//BRDF - bidirectional reflective distribution funciton
	//-- a function that takes as input the incoming (light) direction ωi
	// the outgoing (view) direction ωo,
	//the surface normal n
	//and a surface parameter a that represents the microsurface's roughness. 
	//The BRDF approximates how much each individual light ray ωi
	//contributes to the final reflected light of an opaque surface given its material properties
	//fr=kd * flambert + ks * fcook−torrance
	//kd - light that gest refracted
	//ks - light that gets reflected
	//flambert -- lambertian diffuse = c/pi -- c is surface color
	//fcook-torrace = DFG/ ( 4 * (wo dot n) * (wi dot n) )
	// D - normal distribution function - approximates the amount the surface's microfacets 
	//		are aligned to the halfway vector influenced by the roughness of the surface
	// F - fresnel equation -- the ratio of surface reflection at different surface angles
	// G - geometry function -  the self-shadowing property of the microfacets.
	
	
}


// normal distribution function D
//Trowbridge-Reitz GGX
// NDF ggxtr (n,h,alpha) = alpha^2 / ( pi * ( (n dot h)^2 * (aplha^2 -1 ) + 1 )^2 )
// h - halfway vector, a - rougness
float D_GGX_TR(vec3 N, vec3 H, float a)
{
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float nom    = a2;
    float denom  = (NdotH2 * (a2 - 1.0) + 1.0);
    denom        = PI * denom * denom;
	
    return nom / denom;
}
//roughness version
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return nom / denom;
}


// geometry function
// GSchlickGGX(n,v,k)=(n dot v)/ ((n dot v)(1−k)+k)
// k - remapping of aplha based on whether we use direct lighting or IBL
// kdirect=(α+1)^2 /8
// kIBL =α^2 / 2
// smiths method
// G(n,v,l,k)=Gsub(n,v,k) * Gsub(n,l,k)

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return nom / denom;
}


float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

// fresnel equation
//all surfaces are perfect mirrors at 90 degree angles, and all start to gain this property at the same angle
// fresnel schlick approximation -- only for dielectrics, but we use for both
//FSchlick(n,v,F0)=F0+(1−F0)(1−(n⋅v))^5
// F0 - base reflectivity, IOR, but is represened as triplet since some metals are tinted, precomputed
// paramter metallness - normaly 0 or 1



// cosTheta being the dot product result between the surface's normal n and the view direction v.

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

//below: modified to account for microfacets in rough surfaces
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

//cook torrance reflectance equation
//Lo(p,ωo)=∫Ω (kd * c/π + ks * DFG / (4(ωo dot n)(ωi dot n))  * Li(p,ωi) * n dot ωi * dωi

//IBL
float GGX(float nDotV, float a) {

    float aa = a*a;
    float oneMinusAa = 1 - aa;
    float nDotV2 = 2 * nDotV;
    float root = aa + oneMinusAa * nDotV * nDotV;
    return nDotV2 / (nDotV + sqrt(root));
}

 
float G_Smith(float a, float nDotV, float nDotL) {
    return GGX(nDotL,a) * GGX(nDotV,a);
}
 

vec3 ImportanceSampleGGX( vec2 Xi, float Roughness, vec3 N ) {
  float a = Roughness * Roughness;
  float Phi = 2 * PI * Xi.x;
  float CosTheta = sqrt( (1 - Xi.y) / ( 1 + (a*a - 1) * Xi.y ) );
  float SinTheta = sqrt( 1 - CosTheta * CosTheta );
 
  vec3 H;
  H.x = SinTheta * cos( Phi );
  H.y = SinTheta * sin( Phi );
  H.z = CosTheta;
 
  vec3 UpVector = abs(N.z) < 0.999 ? vec3(0,0,1) : vec3(1,0,0);
  vec3 TangentX = normalize( cross( UpVector, N ) );
  vec3 TangentY = cross( N, TangentX );
 
  // Tangent to world space
  return TangentX * H.x + TangentY * H.y + N * H.z;
}

vec2 Hammersley(int i, int N)
{
  return vec2(
    float(i) / float(N),
    float(bitfieldReverse(i)) * 2.3283064365386963e-10
  );
}

vec3 SpecularIBL( vec3 SpecularColor, float Roughness, vec3 N, vec3 V ) {
  vec3 SpecularLighting = vec3(0);
  for( int i = 0; i < u_NumSamples; i++ ) {
    vec2 Xi = Hammersley(i, u_NumSamples); //vec2(u_Rand[i*2], u_Rand[i*2+1]);
    vec3 H = ImportanceSampleGGX( Xi, Roughness, N );
 
    vec3 L = 2 * dot( V, H ) * H - V;
    float NoV = clamp( dot( N, V ), 0, 1 );
    float NoL = clamp( dot( N, L ), 0, 1 );
    float NoH = clamp( dot( N, H ), 0, 1 );
    float VoH = clamp( dot( V, H ), 0, 1 );
 
    if( NoL > 0 ) {
       vec3 SampleColor = pow(texture( envmap, L,0).rgb, vec3(2.2));
       float G = G_Smith( Roughness, NoV, NoL );
       float Fc = pow( 1 - VoH, 5 );
       vec3 F = (1 - Fc) * SpecularColor + Fc;
       // Incident light = SampleColor * NoL
       // Microfacet specular = D*G*F / (4*NoL*NoV)
       // pdf = D * NoH / (4 * VoH)
       SpecularLighting += SampleColor * F * G * VoH / (NoH * NoV);
    }
  }
  return SpecularLighting / float(u_NumSamples);
}
