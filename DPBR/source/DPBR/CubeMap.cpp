#include "CubeMap.h"

CubeMap::CubeMap(const std::string filename)
{
	id = 0;

	ddsLoader tex[6];

	if (!tex[0].Load("right_" + filename)) return;
	if (!tex[1].Load("left_" + filename)) return;
	if (!tex[2].Load("top_" + filename)) return;
	if (!tex[3].Load("bottom_" + filename)) return;
	if (!tex[4].Load("front_" + filename)) return;
	if (!tex[5].Load("back_" + filename)) return;

	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_CUBE_MAP, id);

	for (unsigned int i = 0; i < 6; ++i)
	{
		unsigned int blockSize = (tex[i].GetFormat() == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
		unsigned int size = ((tex[i].GetWidth() + 3) / 4) * ((tex[i].GetHeight() + 3) / 4) * blockSize;
		glCompressedTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, tex[i].GetFormat(), tex[i].GetWidth(), tex[i].GetHeight(), 0, size, tex[i].GetData());
		tex[i].Unload();
	}	

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

CubeMap::~CubeMap()
{
	glDeleteTextures(1, &id);
}

GLuint CubeMap::GetId()
{
	return id;
}