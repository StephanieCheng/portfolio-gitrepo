#version 330 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 uv;
layout (location = 2) in vec3 norm;
layout (location = 3) in vec3 tan;

out vec3 position;
out vec2 texcoord;
out vec3 normal;
out vec3 tangent;

uniform mat4 M;
uniform mat4 MVP;

void main()
{
	position = (M * vec4(pos, 1.0)).xyz;
	texcoord = uv;
	normal = transpose(inverse(mat3(M))) * norm;
	tangent = transpose(inverse(mat3(M))) * tan;
	
	gl_Position = MVP * vec4(pos, 1.0);
}