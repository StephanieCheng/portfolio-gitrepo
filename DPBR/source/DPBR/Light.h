#pragma once
#include "linmath.h"

enum LightType
{
	AMBIENT, 
	POINT,
	DIRECTIONAL
};

class Light {
public:
	Light();
	~Light();

	void GetPosition(vec3 pos);
	void SetPosition(vec3 pos);

	void GetColor(vec3 col);
	void SetColor(vec3 col);

	float GetIntensity();
	void SetIntensity(float i);

	LightType GetType();
	void SetType(LightType i);

	
private:
	vec3 position;
	vec3 color;
	float intensity;
	LightType type;
};
