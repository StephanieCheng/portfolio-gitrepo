#include "ddsLoader.h"

ddsLoader::ddsLoader()
{
	format = 0;
	mipMapCount = 0;
	width = 0;
	height = 0;

	buffer = nullptr;
}

ddsLoader::~ddsLoader()
{
	Unload();
}

bool ddsLoader::Load(const std::string filename)
{
	Unload();

	const unsigned long FOURCC_DXT1 = 0x31545844;
	const unsigned long FOURCC_DXT3 = 0x33545844;
	const unsigned long FOURCC_DXT5 = 0x35545844;

	// Open file for loading
	std::ifstream file(filename.c_str(), std::ifstream::binary);

	if (!file.good())
		return false;

	// Read image type
	char filecode[4];
	file.read(filecode, 4);

	if (filecode[0] != 'D' &&
		filecode[1] != 'D' &&
		filecode[2] != 'S' &&
		filecode[3] != ' ')
	{
		file.close();
		return false;
	}

	// Read image specification
	char header[124];
	file.read(header, 124);


	// Header data
	width = *(unsigned int*) &(header[8]);
	height = *(unsigned int*) &(header[12]);

	unsigned int linearSize = *(unsigned int*) &(header[16]);
	mipMapCount = *(unsigned int*) &(header[24]);
	unsigned int fourCC = *(unsigned int*) &(header[80]);

	unsigned int bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;

	if (bufsize < 1)
		return false;

	buffer = new char[bufsize];

	file.read(buffer, bufsize);
	file.close();

	switch (fourCC)
	{
	case FOURCC_DXT1:
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
		break;
	case FOURCC_DXT3:
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
		break;
	case FOURCC_DXT5:
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
		break;
	default:
		Unload();
		return false;
		break;
	}
	
	return true;
}

void ddsLoader::Unload()
{
	if (buffer != nullptr)
	{
		delete[] buffer;
		buffer = nullptr;
	}
}

unsigned int ddsLoader::GetFormat()
{
	return format;
}

unsigned int ddsLoader::GetMipMapCount()
{
	return mipMapCount;
}

unsigned int ddsLoader::GetWidth()
{
	return width;
}

unsigned int ddsLoader::GetHeight()
{
	return height;
}

char* ddsLoader::GetData()
{
	return buffer;
}