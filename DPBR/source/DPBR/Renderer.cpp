#include "Renderer.h"
#include <iostream>
#include <string>

Renderer::Renderer()
{
	gBuffer = 0;
	gPosition = 0;
	gNormal = 0;
	gColor = 0;
	gMaterialProperties = 0;

	cameraPos[0] = 0.0f;
	cameraPos[1] = 0.0f;
	cameraPos[2] = 2.0f;

	lookAt[0] = 0.0f;
	lookAt[1] = 0.0f;
	lookAt[2] = 0.0f;

	upVector[0] = 0.0f;
	upVector[1] = 1.0f;
	upVector[2] = 0.0f;

	cameraPitch = 0.0f;

	y_fov = 1.0f;
	aspect = 4.0f / 3.0f;
	near = 0.1f;
	far = 100.0f;

	gBufferShader = NULL;
	sceneShader = NULL;
	skyboxShader = NULL;

	envMap = NULL;
	irrMap = NULL;
}

Renderer::~Renderer()
{
	Uninit();
}

void Renderer::Init(unsigned int width, unsigned int height)
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	
	//make view matrix and projection matrix

	mat4x4 viewMatrix;
	mat4x4 projectionMatrix;

	mat4x4_look_at(viewMatrix, cameraPos, lookAt, upVector);

	scr_width = width;
	scr_height = height;
	aspect = (float)(width) / height;
	mat4x4_perspective(projectionMatrix, y_fov, aspect, near, far);
	mat4x4_mul(viewProjectionMatrix, projectionMatrix, viewMatrix);

	glGenFramebuffers(1, &gBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

	glGenTextures(1, &gPosition);
	glBindTexture(GL_TEXTURE_2D, gPosition);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0);

	glGenTextures(1, &gNormal);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);

	glGenTextures(1, &gColor);
	glBindTexture(GL_TEXTURE_2D, gColor);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gColor, 0);


	glGenTextures(1, &gMaterialProperties);
	glBindTexture(GL_TEXTURE_2D, gMaterialProperties);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, gMaterialProperties, 0);

	GLuint attachments[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
	glDrawBuffers(4, attachments);

	glGenRenderbuffers(1, &rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	gBufferShader = new Shader();
	sceneShader = new Shader();
	skyboxShader = new Shader();

	gBufferShader->Load("gbuff");
	sceneShader->Load("scenePBR");
	skyboxShader->Load("skybox");

	// QUAD
	GLfloat quad_verts[] = {
		-1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 0.0f, 1.0f, 0.0f
	};

	glGenVertexArrays(1, &quadVAO);
	glBindVertexArray(quadVAO);

	glGenBuffers(1, &quadVBO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);

	glBufferData(GL_ARRAY_BUFFER, 20 * sizeof(GLfloat), &quad_verts[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));

	// SKYBOX

	GLfloat skybox_verts[] = {
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};

	glGenVertexArrays(1, &skyboxVAO);
	glBindVertexArray(skyboxVAO);

	glGenBuffers(1, &skyboxVBO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);

	glBufferData(GL_ARRAY_BUFFER, 3 * 36 * sizeof(GLfloat), &skybox_verts[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);


	glBindVertexArray(0);

	envMap = new CubeMap("sky.dds");
	irrMap = new CubeMap("blur.dds");
}

void Renderer::Uninit()
{
	glDeleteTextures(1, &gPosition);
	glDeleteTextures(1, &gNormal);
	glDeleteTextures(1, &gColor);
	glDeleteRenderbuffers(1, &rboDepth);
	glDeleteFramebuffers(1, &gBuffer);

	if (gBufferShader) delete gBufferShader;
	if (sceneShader) delete sceneShader;

	gBufferShader = NULL;
	sceneShader = NULL;

	if (envMap) delete envMap;
	if (irrMap) delete irrMap;

	envMap = NULL;
	irrMap = NULL;
}

void Renderer::CalculateViewProjectionMatrix()
{
	mat4x4 viewMatrix;
	mat4x4 projectionMatrix;

	mat4x4_look_at(viewMatrix, cameraPos, lookAt, upVector);

	mat4x4_perspective(projectionMatrix, y_fov, aspect, near, far);
	mat4x4_mul(viewProjectionMatrix, projectionMatrix, viewMatrix);
}

void Renderer::OrbitCamera(float yrot, float xrot)
{
	if (xrot > 1.57f)
		xrot = 1.57f;

	if (xrot < -1.57f)
		xrot = -1.57f;

	if (cameraPitch + xrot > 1.5f)
		xrot = 1.5f - cameraPitch;

	if (cameraPitch + xrot < -1.5f)
		xrot = -1.5f - cameraPitch;

	cameraPitch += xrot;
	
	vec3 camForward;
	vec3 camRight;

	//std::cout << xrot << "\t" << camRight[0] << "\t" << camRight[1] << "\t" << camRight[2] << std::endl;

	//vec3_sub(cameraPos, cameraPos, lookAt);
	
	mat4x4 I;
	mat4x4_identity(I);
	mat4x4 R;
	

	vec4 camPos2;
	vec4 cameraPos4;
	cameraPos4[0] = cameraPos[0];
	cameraPos4[1] = cameraPos[1];
	cameraPos4[2] = cameraPos[2];
	cameraPos4[3] = 0;

	
	//now rotate around x, but x is camera right vector


	vec3_sub(camForward, lookAt, cameraPos);
	vec3_norm(camForward, camForward);

	vec3_mul_cross(camRight, camForward, upVector);
	vec3_mul_cross(upVector, camRight, camForward);
	vec3_norm(upVector, upVector);
	vec3_mul_cross(camRight, camForward, upVector);

	mat4x4_rotate(R, I, camRight[0], camRight[1], camRight[2], xrot);
	mat4x4_mul_vec4(camPos2, R, cameraPos4);
	

	
	cameraPos4[0] = camPos2[0];
	cameraPos4[1] = camPos2[1];
	cameraPos4[2] = camPos2[2];

	cameraPos[0] = camPos2[0];
	cameraPos[1] = camPos2[1];
	cameraPos[2] = camPos2[2];


	// rot around yaxis

	//std::cout << upVector[1] << std::endl;

	vec3 yaxis = { 0.0f, 1.0f, 0.0f };

	//if (upVector[1] <= 0) yaxis[2] = -1.0f;

	vec3_mul_cross(camRight, camForward, yaxis);
	vec3_mul_cross(upVector, camRight, camForward);
	vec3_norm(upVector, upVector);
	vec3_mul_cross(camRight, camForward, upVector);

	mat4x4_rotate_Y(R, I, yrot);
	mat4x4_mul_vec4(camPos2, R, cameraPos4);


	cameraPos[0] = camPos2[0];
	cameraPos[1] = camPos2[1];
	cameraPos[2] = camPos2[2];

	//vec3_add(cameraPos, cameraPos, lookAt);
	CalculateViewProjectionMatrix();

}


void Renderer::TranslateCamera(vec3 amount)
{


	vec3_add(cameraPos, cameraPos, amount);

	CalculateViewProjectionMatrix();
}

void Renderer::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::StartGeometryPass()
{
	glBindVertexArray(0);
	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}

void Renderer::RenderModel(Model* model)
{
	Texture* albedo = model->GetAlbedoTexture();
	Texture* materialProperties = model->GetMetallnessRoughnessAOTexture();
	Texture* normal = model->GetNormalTexture();

	if (albedo == nullptr || materialProperties == nullptr || materialProperties == nullptr) {
		std::cout << "textures missing" << std::endl;
		return;
	}

	if (albedo == nullptr)
		return;

	glBindVertexArray(model->getVAO()); //glbindvertexarray
	glUseProgram(gBufferShader->GetProgram());

	mat4x4 MVP;
	mat4x4 M;
	model->GetModelMatrix(M);
	mat4x4_mul(MVP, viewProjectionMatrix, M);

	glUniform1i(glGetUniformLocation(gBufferShader->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(gBufferShader->GetProgram(), "normalTex"), 1);
	glUniform1i(glGetUniformLocation(gBufferShader->GetProgram(), "metallicRoughnessAOTex"), 2);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, albedo->GetId());

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normal->GetId());

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, materialProperties->GetId());

	glUniformMatrix4fv(glGetUniformLocation(gBufferShader->GetProgram(), "M"), 1, GL_FALSE, &M[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(gBufferShader->GetProgram(), "MVP"), 1, GL_FALSE, &MVP[0][0]);
	
	glDrawArrays(GL_TRIANGLES, 0, model->GetPolycount() * 3);
}

void Renderer::StartLightPass()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glUseProgram(sceneShader->GetProgram());
	glBindVertexArray(quadVAO);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glDisable(GL_DEPTH_TEST);
}

void Renderer::RenderLight(Light* light)
{

	//added for PBR
	// setup relevant shader uniforms
	glUniform3fv(glGetUniformLocation(sceneShader->GetProgram(), "camPos"), 1, cameraPos);
	//glUniform1f(glGetUniformLocation(sceneShader->GetProgram(), "exposure"), 1.0f);



	glUniform1i(glGetUniformLocation(sceneShader->GetProgram(), "gPosition"), 0);
	glUniform1i(glGetUniformLocation(sceneShader->GetProgram(), "gNormals"), 1);
	glUniform1i(glGetUniformLocation(sceneShader->GetProgram(), "gColor"), 2);
	glUniform1i(glGetUniformLocation(sceneShader->GetProgram(), "gMaterial"), 3); //material properties - roughness & metallic
	glUniform1i(glGetUniformLocation(sceneShader->GetProgram(), "envmap"), 4);
	glUniform1i(glGetUniformLocation(sceneShader->GetProgram(), "irrmap"), 5);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gPosition);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, gColor);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, gMaterialProperties);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_CUBE_MAP, envMap->GetId());
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_CUBE_MAP, irrMap->GetId());

	vec3 lightPosition;
	light->GetPosition(lightPosition);

	vec3 lightColor;
	light->GetColor(lightColor);

	float lightIntensity;
	lightIntensity = light->GetIntensity();

	LightType lightType;
	lightType = light->GetType();
	
	glUniform3fv(glGetUniformLocation(sceneShader->GetProgram(), "lightPosition"), 1, lightPosition);
	glUniform3fv(glGetUniformLocation(sceneShader->GetProgram(), "lightColor"), 1, lightColor);
	glUniform1f(glGetUniformLocation(sceneShader->GetProgram(), "lightIntensity"), lightIntensity);
	glUniform1i(glGetUniformLocation(sceneShader->GetProgram(), "lightType"), lightType);

	/*for (GLuint i = 0; i < lights->size(); ++i)
	{
		vec3 lightpos;
		(*lights)[i].GetPosition(lightpos);

		vec3 lightcol;
		(*lights)[i].GetColor(lightcol);

		float lightI;
		(*lights)[i].GetIntensity(lightI);

		glUniform3fv(glGetUniformLocation(sceneShader->GetProgram(), ("lights[" + std::to_string(i) + "].Position").c_str()), 1, lightpos);
		glUniform3fv(glGetUniformLocation(sceneShader->GetProgram(), ("lights[" + std::to_string(i) + "].Color").c_str()), 1, lightcol);

		glUniform1f(glGetUniformLocation(sceneShader->GetProgram(), ("lights[" + std::to_string(i) + "].Intensity").c_str()), lightI);
	}*/

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// Copy over the depth info
	glBindFramebuffer(GL_READ_FRAMEBUFFER, gBuffer);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); // Write to default framebuffer
	glBlitFramebuffer(0, 0, scr_width, scr_height, 0, 0, scr_width, scr_height, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
}

void Renderer::RenderSkybox(CubeMap* skymap)
{
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glBindVertexArray(skyboxVAO);
	glUseProgram(skyboxShader->GetProgram());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skymap->GetId());

	glUniformMatrix4fv(glGetUniformLocation(skyboxShader->GetProgram(), "VP"), 1, GL_FALSE, &viewProjectionMatrix[0][0]);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glDepthFunc(GL_LESS);
	
	// What here???
	// The skybox should be drawn after all lighting has been done.
	// It shouldn't go into the G-buffer at all
	// and the lighting shaders shouldn't need to know anything about it.

	// Depth buffer must be in tact after light pass to render sky correctly?
	// Use GL_GEQUAL for the depth function.
	// In the vertext shader of the skybox, set the z value of gl_Position to its w value.
	// This makes sure the skybox is rendered onto the far plane.
};

void Renderer::GetCameraUp(vec3 up)
{
	up[0] = upVector[0];
	up[1] = upVector[1];
	up[2] = upVector[2];
}

void Renderer::GetCameraForward(vec3 fwd)
{
	vec3_sub(fwd, lookAt, cameraPos);
	vec3_norm(fwd, fwd);
}

void Renderer::GetCameraRight(vec3 r)
{
	vec3 up, fwd;
	GetCameraForward(fwd);
	GetCameraUp(up);
	vec3 right;
	vec3_mul_cross(right, fwd, up);
	r[0] = right[0];
	r[1] = right[1];
	r[2] = right[2];
}