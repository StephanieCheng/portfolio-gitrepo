#version 330 core

in vec3 position;
in vec2 texcoord;
in vec3 normal;
in vec3 tangent;

uniform sampler2D diffuseTex;
uniform sampler2D normalTex;
uniform sampler2D metallicRoughnessAOTex;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gColor;
layout (location = 3) out vec4 gMaterialProperties;

void main()
{ 
	vec2 uv = vec2(texcoord.x, 1.0 - texcoord.y);
	vec3 binormal = cross(normal, tangent);
	mat3 tangentToWorld = mat3(tangent.x, binormal.x, normal.x,
							   tangent.y, binormal.y, normal.y,
							   tangent.z, binormal.z, normal.z);
							   
	
	
	
	gPosition = position;
	gNormal = normal;
	//gNormal = (texture2D(normalTex, uv).rgb * 2.0 - 1.0) * tangentToWorld;
	gColor = texture(diffuseTex, uv);
	gMaterialProperties = texture(metallicRoughnessAOTex, uv);
}