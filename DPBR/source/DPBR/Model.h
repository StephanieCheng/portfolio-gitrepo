#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "linmath.h"
#include <string>
#include <vector>

#include "Texture.h"

class Model
{
	public:
		Model();
		~Model();

		void Load(const std::string& filename);
		void Unload();

		void SetPosition(vec3 pos);
		void GetPosition(vec3 pos);
		
		void SetRotation(quat rot);
		void GetRotation(quat rot);

		void SetScale(float sca);
		float GetScale();

		void GetModelMatrix(mat4x4 m);
		
		GLuint getVAO();

		unsigned int GetPolycount();

		void SetAlbedoTexture(Texture* tex);
		Texture* GetAlbedoTexture();

		void SetMetallnessRoughnessAOTexture(Texture* tex);
		Texture* GetMetallnessRoughnessAOTexture();

		void SetNormalTexture(Texture* tex);
		Texture* GetNormalTexture();


		void computeTangentBasis(
			// inputs
			std::vector<GLfloat> & vertices,
			std::vector<GLfloat> & uvs,
			std::vector<unsigned int> &vi,
			std::vector<unsigned int> &ti,
			// outputs
			std::vector<GLfloat> & tangents
			//std::vector<vec3> & bitangents
		);

	private:
		GLuint vao;
		GLuint vbo;

		vec3 position;
		quat rotation;
		float scale;

		unsigned int polycount;

		Texture* albedo;
		Texture* normal;
		Texture* metalnessRoughnessAO;
		//Texture* ao;

};