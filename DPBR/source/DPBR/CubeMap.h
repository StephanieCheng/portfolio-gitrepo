#pragma once
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "ddsLoader.h"

class CubeMap
{
	public:
		CubeMap(const std::string filename);
		~CubeMap();

		GLuint GetId();

	private:
		unsigned int id;
};