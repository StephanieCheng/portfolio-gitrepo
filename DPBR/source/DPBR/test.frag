#version 330 core

in vec3 position;
in vec2 texcoord;
in vec3 normal;

out vec3 gPosition;
out vec3 gNormal;
out vec4 gColor;

void main()
{
	gPosition = position;
	gNormal = normal;
	gColor = vec4(1.0, 0.0f, 0.0f, 1.0f);
}