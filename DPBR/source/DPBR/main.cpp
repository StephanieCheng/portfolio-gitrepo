#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>

#include "Model.h"
#include "Renderer.h"


#define ASPECTRATIO 1.77778f
#define WINDOW_WIDTH 1280.0f
#define	WINDOW_HEIGHT (WINDOW_WIDTH / ASPECTRATIO)
#define ORBITSPEED (12.5664/WINDOW_WIDTH)


int main(int argv, char *argc[])
{
	if (!glfwInit())
	{
		std::cout << "GLFW INIT ERROR" << std::endl;
		return -1;
	}



	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "DPBR", NULL, NULL);
	glfwMakeContextCurrent(window);

	GLenum err = glewInit();
	if (err)
	{
		std::cout << "GLEW INIT ERROR " << glewGetErrorString(err) << std::endl;
		return -1;
	}

	//Texture testTex("diffTex.dds");

	
	Texture albedotex("cap_albedo.DDS");
	Texture metallicRoughnessAOtex("cap_mat.DDS");
	Texture normaltex("rustediron1-alt2-Unreal-Engine/newnormal_PNG_DXT3_1.DDS");
	
	/*
	Texture albedotex("rustediron1-alt2-Unreal-Engine/newbasecol_PNG_DXT3_1.DDS");
	Texture metallicRoughnessAOtex("rustediron1-alt2-Unreal-Engine/newmetallic_PNG_DXT3_1.DDS");
	Texture normaltex("rustediron1-alt2-Unreal-Engine/newnormal_PNG_DXT3_1.DDS");
	*/

	Texture albedotex2("rustediron1-alt2-Unreal-Engine/multiplespheres_col_PNG_DXT3_1.DDS");
	Texture metallicRoughnessAOtex2("rustediron1-alt2-Unreal-Engine/multiplespheres_metallicroughness_PNG_DXT3_1.DDS");
	Texture normaltex2("rustediron1-alt2-Unreal-Engine/multiplespheres_norm_PNG_DXT3_1.DDS");


	CubeMap sky("sky.dds");

	vec3 pos;
	//Model model; model.Load("sphere.obj");
	Model model; model.Load("capsule.obj");
	model.SetAlbedoTexture(&albedotex);
	model.SetMetallnessRoughnessAOTexture(&metallicRoughnessAOtex);
	model.SetNormalTexture(&normaltex);
	pos[0] = 0.0f; pos[1] = 0.0f; pos[2] = 0.0f; model.SetPosition(pos);
	Model cube2; cube2.Load("multiplespheres.obj");
	cube2.SetAlbedoTexture(&albedotex2);
	cube2.SetMetallnessRoughnessAOTexture(&metallicRoughnessAOtex2);
	cube2.SetNormalTexture(&normaltex2);
	pos[0] = 0.0f; pos[1] = 0.0f; pos[2] = 0.0f; cube2.SetPosition(pos);
	/*Model cube3; cube3.Load("sphere.obj");
	cube3.SetAlbedoTexture(&albedotex);
	cube3.SetMetallnessRoughnessAOTexture(&metallicRoughnessAOtex);
	cube3.SetNormalTexture(&normaltex);
	pos[0] = 0.0f; pos[1] = 1.0f; pos[2] = 0.0f; cube3.SetPosition(pos);*/
	


	model.SetScale(0.75f);
	//cube2.SetScale(0.5f);
	//cube3.SetScale(0.5f);

	vec3 lightpos;
	vec3 lightcol;
	float lightIntensity;

	Light ambient;
	lightcol[0] = 1.0f, lightcol[1] = 1.0f; lightcol[2] = 1.0f;
	ambient.SetColor(lightcol);
	ambient.SetType(AMBIENT);

	Light light1;
	lightpos[0] = 9.0f, lightpos[1] = 20.0f; lightpos[2] = -10.0f;
	lightcol[0] = 150.0f, lightcol[1] = 150.0f; lightcol[2] = 150.0f;
	lightIntensity = 150;
	light1.SetPosition(lightpos);
	light1.SetColor(lightcol);
	//light1.SetIntensity(lightIntensity);

	Light light2;
	lightpos[0] = 10.0f, lightpos[1] = 0.0f; lightpos[2] = 1.0f;
	lightcol[0] = 15.0f, lightcol[1] = 0.0f; lightcol[2] = 0.0f;
	light2.SetPosition(lightpos);
	light2.SetColor(lightcol);

	Light light3;
	lightpos[0] = 1.0f, lightpos[1] = 0.0f; lightpos[2] = 10.0f;
	lightcol[0] = 0.0f, lightcol[1] = 15.0f; lightcol[2] = 0.0f;
	light3.SetPosition(lightpos);
	light3.SetColor(lightcol);

	Renderer renderer;
	renderer.Init(WINDOW_WIDTH, WINDOW_HEIGHT);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	double xpos = 0;
	double ypos = 0;
	double xpos_prev = 0;
	double ypos_prev = 0;

	double dt = 0;


	while (!glfwWindowShouldClose(window))
	{
		renderer.StartGeometryPass();
		renderer.Clear();
		renderer.RenderModel(&model);
		renderer.RenderModel(&cube2);
		//renderer.RenderModel(&cube3);
		renderer.StartLightPass();
		renderer.Clear();
		renderer.RenderLight(&ambient);
		renderer.RenderLight(&light1);
		renderer.RenderLight(&light2);
		renderer.RenderLight(&light3);
		//renderer.RenderSkybox(&sky);
		
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) glfwSetWindowShouldClose(window, GLU_TRUE);
		
		//rot
		xpos_prev = xpos;
		ypos_prev = ypos;

		glfwGetCursorPos(window, &xpos, &ypos);
		xpos = xpos - WINDOW_WIDTH / 2;
		ypos = ypos - WINDOW_HEIGHT / 2;

		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS){ 

				//calculate angle from x and z
				//x angle - ypos, z angle - xpos

				float xdelta = (xpos_prev - xpos) * ORBITSPEED;
				float ydelta = (ypos_prev - ypos) * ORBITSPEED;
			

				//mov horizontaly full width = 360 * 2  aka 0.555 deg per pixel or 0.00969 rad per pixel

				renderer.OrbitCamera(xdelta, ydelta);

				//std::cout << xdelta << std::endl;

				//double xangle = xdelta * 
		}

		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS){


			float xdelta = -(xpos_prev - xpos) * ORBITSPEED;
			float ydelta = -(ypos_prev - ypos) * ORBITSPEED;

			vec3 axis;
			

			quat o, r, t;
			model.GetRotation(o);

			renderer.GetCameraUp(axis);
			quat_rotate(r, xdelta, axis);

			quat_mul(t, r, o);

			renderer.GetCameraRight(axis);
			quat_rotate(r, ydelta, axis);

			quat_mul(o, r, t);
			
			model.SetRotation(o);
			cube2.SetRotation(o);
		}

		//tran
		vec3 move;
		move[0] = 0.0f; move[1] = 0.0f; move[2] = 0.0f;

		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) move[0] -= dt;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) move[0] += dt;
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) move[1] += dt;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) move[1] -= dt;
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) move[2] += dt;
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) move[2] -= dt;
		
		renderer.TranslateCamera(move);

		glfwSwapBuffers(window);
		glfwPollEvents();

		dt = glfwGetTime();
		glfwSetTime(0);

		if (dt > 0.2)
			dt = 0.2;

	}
	
	renderer.Uninit();
	model.Unload();
	cube2.Unload();
	//cube3.Unload();

	glfwTerminate();
	return 0;
}