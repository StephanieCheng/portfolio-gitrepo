#include "Light.h"

Light::Light()
{
	position[0] = 0.0f;
	position[1] = 0.0f;
	position[2] = 0.0f;

	color[0] = 1.0f;
	color[1] = 1.0f;
	color[2] = 1.0f;

	intensity = 1.0f;

	type = POINT;
}

Light::~Light()
{
}

void Light::GetPosition(vec3 pos)
{
	pos[0] = position[0];
	pos[1] = position[1];
	pos[2] = position[2];

}

void Light::SetPosition(vec3 pos)
{
	position[0] = pos[0];
	position[1] = pos[1];
	position[2] = pos[2];
}

void Light::GetColor(vec3 col)
{
	col[0] = color[0];
	col[1] = color[1];
	col[2] = color[2];
}

void Light::SetColor(vec3 col)
{
	color[0] = col[0];
	color[1] = col[1];
	color[2] = col[2];
}

float Light::GetIntensity()
{
	return intensity;
}

void Light::SetIntensity(float i)
{
	intensity = i;
}

LightType Light::GetType()
{
	return type;
}

void Light::SetType(LightType i)
{
	type = i;
}
