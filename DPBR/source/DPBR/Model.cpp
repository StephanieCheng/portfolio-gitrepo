#include "Model.h"
#include <fstream>
#include <sstream>

#include <iostream>

Model::Model()
{
	vao = 0;
	vbo = 0;

	position[0] = 0.0f;
	position[1] = 0.0f;
	position[2] = 0.0f;

	quat_identity(rotation);

	scale = 1.0f;

	polycount = 0;

	albedo = nullptr;

}

Model::~Model()
{
	Unload();
}

void Model::Load(const std::string& filename)
{
	std::vector<GLfloat> v;
	std::vector<GLfloat> vt;
	std::vector<GLfloat> vn;
	std::vector<unsigned int> vi;
	std::vector<unsigned int> ti;
	std::vector<unsigned int> ni;

	std::ifstream file(filename.c_str());
	std::string word;
	
	GLfloat a;
	while (file.good())
	{
		file >> word;
		if (file.eof()) break;

		if (word[0] == '#') 
		{
			getline(file, word);
		}
		else if (word == "v") 
		{
			for (unsigned int i = 0; i < 3; ++i)
			{
				file >> a;
				v.push_back(a);
			}
		}
		else if (word == "vt")
		{
			for (unsigned int i = 0; i < 2; ++i)
			{
				file >> a;
				vt.push_back(a);
			}
		}
		else if (word == "vn") 
		{
			for (unsigned int i = 0; i < 3; ++i)
			{
				file >> a;
				vn.push_back(a);
			}
		}
		else if (word == "f")
		{
			unsigned int index;
			std::string vertex;

			for (unsigned int i = 0; i < 3; ++i)
			{
				file >> vertex;

				std::istringstream vs(vertex.substr(0, vertex.find('/')));
				vs >> index;
				vi.push_back(index - 1);

				vertex = vertex.substr(vertex.find('/') + 1);
				std::istringstream ts(vertex.substr(0, vertex.find('/')));
				ts >> index;
				ti.push_back(index - 1);

				vertex = vertex.substr(vertex.find('/') + 1);
				std::istringstream ns(vertex.substr(0, vertex.find('/')));
				ns >> index;
				ni.push_back(index - 1);
			}

			polycount++;
		}
	}

	file.close();

	std::vector<GLfloat> tangents;
	for (int i = 0; i < v.size(); i++) {
		tangents.push_back(0);
	}

	computeTangentBasis(v, vt, vi, ti, tangents);

	//first 3 no - position, 4,5th uv, 678- normals, 9, 10, 11 - tangent

	std::vector<GLfloat> vertexarray;

	for (unsigned int i = 0; i < vi.size(); ++i)
	{
		//pos
		vertexarray.push_back(v.at(3 *  vi.at(i)));
		vertexarray.push_back(v.at(3 * vi.at(i) + 1));
		vertexarray.push_back(v.at(3 * vi.at(i) + 2));

		//uv
		vertexarray.push_back(vt.at(2 * ti.at(i)));
		vertexarray.push_back(vt.at(2 * ti.at(i) + 1));

		//normals
		vertexarray.push_back(vn.at(3 * ni.at(i)));
		vertexarray.push_back(vn.at(3 * ni.at(i) + 1));
		vertexarray.push_back(vn.at(3 * ni.at(i) + 2));

		//tangents
		vertexarray.push_back(tangents.at(3 * vi.at(i)));
		vertexarray.push_back(tangents.at(3 * vi.at(i) + 1));
		vertexarray.push_back(tangents.at(3 * vi.at(i) + 2));

	}

	//send to graphics card

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, vertexarray.size() * sizeof(GLfloat), &vertexarray[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(5 * sizeof(GLfloat)));
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(8 * sizeof(GLfloat)));

	glBindVertexArray(0);
}


void Model::Unload()
{
	if (vbo) glDeleteBuffers(1, &vbo);
	if (vao) glDeleteVertexArrays(1, &vao);

	vbo = 0;
	vao = 0;

	polycount = 0;
}

void Model::SetPosition(vec3 pos)
{
	position[0] = pos[0];
	position[1] = pos[1];
	position[2] = pos[2];
}

void Model::GetPosition(vec3 pos)
{
	pos[0] = position[0];
	pos[1] = position[1];
	pos[2] = position[2];
}

void Model::SetRotation(quat rot)
{
	rotation[0] = rot[0];
	rotation[1] = rot[1];
	rotation[2] = rot[2];
	rotation[3] = rot[3];
}

void Model::GetRotation(quat rot)
{
	rot[0] = rotation[0];
	rot[1] = rotation[1];
	rot[2] = rotation[2];
	rot[3] = rotation[3];
}

void Model::SetScale(float sca)
{
	scale = sca;
}

float Model::GetScale()
{
	return scale;
}

void Model::GetModelMatrix(mat4x4 m)
{
	mat4x4 I;
	mat4x4 S;
	mat4x4 R;
	mat4x4 T;

	mat4x4_identity(I);
	mat4x4_scale(S, I, scale);
	S[3][3] = 1;

	mat4x4_from_quat(R, rotation);
	mat4x4_translate(T, position[0], position[1], position[2]);
	mat4x4_mul(I, R, S);
	mat4x4_mul(m, T, I);
}

GLuint Model::getVAO()
{
	return vao;
}


unsigned int Model::GetPolycount()
{
	return polycount;
}

void Model::SetAlbedoTexture(Texture* tex)
{
	albedo = tex;
}

Texture* Model::GetAlbedoTexture()
{
	return albedo;
}

void Model::SetMetallnessRoughnessAOTexture(Texture * tex)
{
	metalnessRoughnessAO = tex;
}

Texture * Model::GetMetallnessRoughnessAOTexture()
{
	return metalnessRoughnessAO;
}


void Model::SetNormalTexture(Texture * tex)
{
	normal = tex;
}

Texture * Model::GetNormalTexture()
{
	return normal;
}


void Model::computeTangentBasis(
	// inputs
	std::vector<GLfloat> & v,
	std::vector<GLfloat> & uvs,
	std::vector<unsigned int> &vi,
	std::vector<unsigned int> &ti,
	// outputs
	std::vector<GLfloat> & tangents
	//std::vector<vec3> & bitangents
)
{
	for (int i = 0; i < vi.size(); i += 3) { //we iterate over faces //group of 3 = 1 face

		// Shortcuts for vertices
		vec3 v0;
		v0[0] = v.at(3 * vi.at(i));
		v0[1] = v.at(3 * vi.at(i) + 1);
		v0[2] = v.at(3 * vi.at(i) + 2);

		vec3 v1;
		v1[0] = v.at(3 * vi.at(i + 1));
		v1[1] = v.at(3 * vi.at(i + 1) + 1);
		v1[2] = v.at(3 * vi.at(i + 1) + 2);

		vec3 v2;
		v2[0] = v.at(3 * vi.at(i + 2));
		v2[1] = v.at(3 * vi.at(i + 2) + 1);
		v2[2] = v.at(3 * vi.at(i + 2) + 2);


		// Edges of the triangle : postion delta
		vec3 deltaPos1;
		vec3_sub(deltaPos1, v1, v0);
		vec3 deltaPos2;
		vec3_sub(deltaPos1, v2, v0);
		//deltaPos1 = v1 - v0;
		//GLfloat deltaPos2 = v2 - v0;

		// Shortcuts for UVs
		float uv0x = uvs.at(2 * ti.at(i));
		float uv1x = uvs.at(2 * ti.at(i + 1));
		float uv2x = uvs.at(2 * ti.at(i + 2));

		float uv0y = uvs.at(2 * ti.at(i) + 1);
		float uv1y = uvs.at(2 * ti.at(i + 1) + 1);
		float uv2y = uvs.at(2 * ti.at(i + 2) + 1);

		// UV delta
		float deltaUV1x = uv1x = uv0x;
		float deltaUV1y = uv1y - uv0y;

		float deltaUV2x = uv2x - uv0x;
		float deltaUV2y = uv2y - uv0y;


		vec3 deltaPos1y;
		vec3 deltaPos2y;
		vec3 deltaPos1x;
		vec3 deltaPos2x;
		vec3_scale(deltaPos1y, deltaPos1, deltaUV2y);
		vec3_scale(deltaPos2y, deltaPos2, deltaUV1y);
		vec3_scale(deltaPos1x, deltaPos1, deltaUV2x);
		vec3_scale(deltaPos2x, deltaPos2, deltaUV1x);

		float r = 1.0f / (deltaUV1x * deltaUV2y - deltaUV1y * deltaUV2x);
		// glm::vec3  tangent = (deltaPos1 * deltaUV2y - deltaPos2 * deltaUV1y)*r;
		// glm::vec3  bitangent = (deltaPos2 * deltaUV1x - deltaPos1 * deltaUV2x)*r;

		vec3 tangent;
		vec3_sub(tangent, deltaPos1y, deltaPos2y);
		vec3_scale(tangent, tangent, r);

		//vec3 bitangent;
		//vec3_sub(bitangent, deltaPos1y, deltaPos2y);
		//vec3_scale(bitangent, bitangent, r);
		

		//tangents have the same index count as vi, since we calculate the tangent for 3 verticies it will be averaged and saved 3 times

		if (tangents.size() < 3* vi.at(i)+2 || tangents.size() < 3 * vi.at(i + 1) + 2 || tangents.size() < 3 * vi.at(i + 2) + 2) {
			std::cout << "tangents not initialised properly " << std::endl;
		}
		else {
			vec3 tg;

			tg[0] = tangent[0] + tangents.at(3 * vi.at(i));
			tg[1] = tangent[1] + tangents.at(3 * vi.at(i) + 1);
			tg[2] = tangent[2] + tangents.at(3 * vi.at(i) + 2);
			//vec3_norm(tg,tg);

			tangents.at(3 * vi.at(i) + 2) = tg[0];
			tangents.at(3 * vi.at(i) + 2) = tg[1];
			tangents.at(3 * vi.at(i) + 2) = tg[2];

			tg[0] = tangent[0] + tangents.at(3 * vi.at(i + 1));
			tg[1] = tangent[1] + tangents.at(3 * vi.at(i + 1) + 1);
			tg[2] = tangent[2] + tangents.at(3 * vi.at(i + 1) + 2);
			//vec3_norm(tg, tg);

			tangents.at(3 * vi.at(i + 1) + 2) = tg[0];
			tangents.at(3 * vi.at(i + 1) + 2) = tg[1];
			tangents.at(3 * vi.at(i + 1) + 2) = tg[2];

			tg[0] = tangent[0] + tangents.at(3 * vi.at(i + 2));
			tg[1] = tangent[1] + tangents.at(3 * vi.at(i + 2) + 1);
			tg[2] = tangent[2] + tangents.at(3 * vi.at(i + 2) + 2);
			//vec3_norm(tg, tg);

			tangents.at(3 * vi.at(i + 2) + 2) = tg[0];
			tangents.at(3 * vi.at(i + 2) + 2) = tg[1];
			tangents.at(3 * vi.at(i + 2) + 2) = tg[2];

			
		}

		// Set the same tangent for all three vertices of the triangle.
		// They will be merged later, in vboindexer.cpp
		//tangents.push_back(tangent);
		//tangents.push_back(tangent);
		//tangents.push_back(tangent);

		// Same thing for binormals
		//bitangents.push_back(bitangent);
		//bitangents.push_back(bitangent);
		//bitangents.push_back(bitangent);
	}
}
