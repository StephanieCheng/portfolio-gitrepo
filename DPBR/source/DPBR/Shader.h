#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>

class Shader
{
	public:
		Shader();
		~Shader();

		void Load(const std::string& filename);
		void Unload();

		GLuint GetProgram();

	private:
		GLuint program;
};