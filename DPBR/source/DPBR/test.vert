#version 330 core

in vec3 pos;
in vec2 uv;
in vec3 norm;

out vec3 position;
out vec2 texcoord;
out vec3 normal;

uniform mat4 M;
uniform mat4 MVP;

void main()
{
	position = (M * vec4(pos, 1.0)).xyz;
	texcoord = uv;
	normal = (M * vec4(norm, 0.0)).xyz;
	
	gl_Position = MVP * vec4(pos, 1.0);
}