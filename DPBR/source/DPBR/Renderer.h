#pragma once
#include "Shader.h"
#include "Model.h"
#include "Light.h"
#include "Texture.h"
#include "CubeMap.h"
#include <vector>

class Renderer 
{
public:
	Renderer();
	~Renderer();
	void Init(unsigned int width, unsigned int height);
	void Uninit();

	void CalculateViewProjectionMatrix();

	void OrbitCamera(float xdelta, float ydelta);

	void TranslateCamera(vec3 amount);

	void Clear();
	void StartGeometryPass();
	void RenderModel(Model* model);
	void StartLightPass();
	void RenderLight(Light* light);

	void RenderSkybox(CubeMap* skymap);

	void GetCameraUp(vec3 up);
	void GetCameraForward(vec3 fwd);
	void GetCameraRight(vec3 r);

private:
	GLuint gBuffer, gPosition, gNormal, gColor, gMaterialProperties, rboDepth;
	GLuint quadVAO;
	GLuint quadVBO;

	GLuint skyboxVAO;
	GLuint skyboxVBO;

	Shader* gBufferShader;
	Shader* sceneShader;
	Shader* skyboxShader;

	vec3 cameraPos;
	vec3 lookAt;
	vec3 upVector;

	float cameraPitch;

	float y_fov;
	float scr_width;
	float scr_height;
	float aspect;
	float near;
	float far;

	CubeMap* envMap;
	CubeMap* irrMap;

	mat4x4 viewProjectionMatrix;
};