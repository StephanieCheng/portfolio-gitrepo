#pragma once
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "ddsLoader.h"

class Texture
{
	public:
		Texture(const std::string filename);
		~Texture();

		GLuint GetId();

	private:
		unsigned int id;
};