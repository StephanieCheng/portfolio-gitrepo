#pragma once
#include <fstream>
#include <sstream>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class ddsLoader
{
public:
	ddsLoader();
	~ddsLoader();

	bool Load(const std::string filename);
	void Unload();
	
	unsigned int GetFormat();
	unsigned int GetMipMapCount();
	unsigned int GetWidth();
	unsigned int GetHeight();
	char* GetData();

private:
	unsigned int format;
	unsigned int mipMapCount;
	unsigned int width;
	unsigned int height;

	char* buffer;
};

