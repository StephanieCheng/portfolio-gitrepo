#version 330 core


//out vec4 FragColor;
//in vec2 TexCoords;
//in vec3 WorldPos;
//in vec3 Normal;
  
uniform vec3 camPos;
  
//uniform vec3  albedo;
//uniform float metallic;
//uniform float roughness;
//uniform float ao;

//OR
//uniform sampler2D albedoMap;
//uniform sampler2D normalMap;
//uniform sampler2D metallicMap;
//uniform sampler2D roughnessMap;
//uniform sampler2D aoMap;

in vec2 texcoord;

out vec3 FragColor;

uniform sampler2D gPosition;
uniform sampler2D gNormals;
uniform sampler2D gColor;
uniform sampler2D gMateral;

//uniform samplerCube cubemap;

uniform vec3 lightPosition;
uniform vec3 lightColor;
uniform float lightIntensity;
uniform int lightType;

const float PI = 3.14159265359;

float D_GGX_TR(vec3 N, vec3 H, float a);
float DistributionGGX(vec3 N, vec3 H, float roughness);
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
vec3 fresnelSchlick(float cosTheta, vec3 F0);
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness);

void main()
{
	//texttures needed - albedo, normal, metallic, roughness, AO
	vec3 normal = texture2D(gNormals, texcoord).xyz;
	vec3 fragPos = texture2D(gPosition, texcoord).xyz;
	vec3 albedo = texture2D(gColor, texcoord).rgb;
	
	
	//PBR textures
	//vec3 albedo     = pow(texture(albedoMap, TexCoords).rgb, 2.2);
    //vec3 normal     = getNormalFromMap();
    //float metallic  = texture(metallicMap, TexCoords).r;
    //float roughness = texture(roughnessMap, TexCoords).r;
    //float ao        = texture(aoMap, TexCoords).r;
	
	
	//TEMP
	float metallic = 0;
	float roughness = 1;
	float ao = 1; //white ao
	
	vec3 color = vec3(0.0);
	
	if(lightType == 1){
	

		vec3 N = normalize(normal);
		//vec3 V = normalize(camPos - fragPos);
		vec3 V = normalize(fragPos);
		
		
		//we assume 0.04 is good enough for dielectrics, but for metals we use the albedo
		
		vec3 F0 = vec3(0.04);
		F0      = mix(F0, albedo, metallic);
		vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness); 
		
		
		vec3 kS = F; 									// reflection/specular fraction
		vec3 kD = vec3(1.0) - kS;                      // refraction/diffuse  fraction
		kD *= 1.0 - metallic;  							//metallic surfaces refract nothing
		
		//TEMP
		//vec3  lightColor  = vec3(23.47f, 21.31f, 20.79f);
		
		//vec3  wi          = normalize(lightPosition - fragPos);
		//float cosTheta    = max(dot(N, wi), 0.0f);
		//float attenuation = calculateAttenuation(fragPos, lightPosition);
		//float radiance    = lightColor * attenuation * cosTheta;
		
		
		
		//reflectance equation Lo (p,wo) = Integral ( fr(p,wi,wo) * Li(p,wi) * dot(n,wi) * dwi)
		//radial flux is simply RGB
		//solid angle w 
		//radial intensity -> I = d(radialflux)/dw
		//radiance -> L = d^2 (radial flux) / (dA dw cos theta) -- total observed energy over area A over w with radflux
		
		//float cosTheta = dot(lightDir, N);  
		
		//float cosTheta = dot(lightDir, N); -- dot(n,wi) in reflectance equation (moved out of radiance)
		//for single point -> w direction, A is point p
		//irradiance - sum of all incoming light on point p
		//Lo the result of reflectance is the refllected sum of lights irradiance on point p as viewed from wo
		//our A is a hemisphere around th normal
		//instead of an integral we use the nummerical solution Riemann sum
		
		
		//int steps = 100;
		//float sum = 0.0f;
		//vec3 P    = ...;
		//vec3 Wo   = ...;
		//vec3 N    = ...;
		//float dW  = 1.0f / steps;
		//for(int i = 0; i < steps; ++i) 
		//{
		//	vec3 Wi = getNextIncomingLightDir(i);
		//	sum += Fr(p, Wi, Wo) * L(p, Wi) * dot(N, Wi) * dW;
		//}
		
		
		//loop over all light sources
		vec3 Lo = vec3(0.0);
		//for(int i = 0; i < 4; ++i) 
		//{
		vec3 L = normalize(lightPosition - fragPos);
		vec3 H = normalize(V + L);
	 
		float distance    = length(lightPosition - fragPos);
		float attenuation = 1.0 / (distance * distance);
		vec3 radiance     = lightColor * attenuation; 
		
		float NDF = DistributionGGX(N, H, roughness);       
		float G   = GeometrySmith(N, V, L, roughness); 
		
		//cook-torrace
		vec3 nominator    = NDF * G * F;
		float denominator = 4 * max(dot(V, N), 0.0) * max(dot(L, N), 0.0) + 0.001; 
		vec3 brdf         = nominator / denominator;  

		//each lights contribution
	  
		float NdotL = max(dot(N, L), 0.0);        
		Lo += (kD * albedo / PI + brdf) * radiance * NdotL;
		//}
		
		color = Lo;
	
	}
	
	//ambient term 
	
	else if( lightType == 0){
	
		color = vec3(0.03) * albedo * ao;
		//color = texture(cubemap, fragPos) * albedo * ao;
		//vec3 color   = ambient + Lo; 
	
	}
	
	//gamma correction
	//Lo grows high but gets clamped due to LDR, we need HDR
	
	color = color / (color + vec3(1.0));
	color = pow(color, vec3(1.0/2.2)); 
	
	//final output
	//FragColor = vec4(color, 1.0);
	FragColor = color;
	
	//The reflectance equation sums up the radiance of all incoming light directions ωi
	//over the hemisphere Ω scaled by fr that hit point p
	//and returns the sum of reflected light Lo in the viewer's direction. 
	
	
	//BRDF - bidirectional reflective distribution funciton
	//-- a function that takes as input the incoming (light) direction ωi
	// the outgoing (view) direction ωo,
	//the surface normal n
	//and a surface parameter a that represents the microsurface's roughness. 
	//The BRDF approximates how much each individual light ray ωi
	//contributes to the final reflected light of an opaque surface given its material properties
	//fr=kd * flambert + ks * fcook−torrance
	//kd - light that gest refracted
	//ks - light that gets reflected
	//flambert -- lambertian diffuse = c/pi -- c is surface color
	//fcook-torrace = DFG/ ( 4 * (wo dot n) * (wi dot n) )
	// D - normal distribution function - approximates the amount the surface's microfacets 
	//		are aligned to the halfway vector influenced by the roughness of the surface
	// F - fresnel equation -- the ratio of surface reflection at different surface angles
	// G - geometry function -  the self-shadowing property of the microfacets.
	
	
}


// normal distribution function D
//Trowbridge-Reitz GGX
// NDF ggxtr (n,h,alpha) = alpha^2 / ( pi * ( (n dot h)^2 * (aplha^2 -1 ) + 1 )^2 )
// h - halfway vector, a - rougness
float D_GGX_TR(vec3 N, vec3 H, float a)
{
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float nom    = a2;
    float denom  = (NdotH2 * (a2 - 1.0) + 1.0);
    denom        = PI * denom * denom;
	
    return nom / denom;
}
//roughness version
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return nom / denom;
}


// geometry function
// GSchlickGGX(n,v,k)=(n dot v)/ ((n dot v)(1−k)+k)
// k - remapping of aplha based on whether we use direct lighting or IBL
// kdirect=(α+1)^2 /8
// kIBL =α^2 / 2
// smiths method
// G(n,v,l,k)=Gsub(n,v,k) * Gsub(n,l,k)

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return nom / denom;
}


float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

// fresnel equation
//all surfaces are perfect mirrors at 90 degree angles, and all start to gain this property at the same angle
// fresnel schlick approximation -- only for dielectrics, but we use for both
//FSchlick(n,v,F0)=F0+(1−F0)(1−(n⋅v))^5
// F0 - base reflectivity, IOR, but is represened as triplet since some metals are tinted, precomputed
// paramter metallness - normaly 0 or 1



// cosTheta being the dot product result between the surface's normal n and the view direction v.

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

//below: modified to account for microfacets in rough surfaces
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

//cook torrance reflectance equation
//Lo(p,ωo)=∫Ω (kd * c/π + ks * DFG / (4(ωo dot n)(ωi dot n))  * Li(p,ωi) * n dot ωi * dωi
