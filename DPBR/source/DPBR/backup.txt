
std::vector<GLfloat> tangents (v.size() , 0);



void Model::computeTangentBasis(
	// inputs
	std::vector<GLfloat> & v,
	std::vector<GLfloat> & uvs,
	std::vector<unsigned int> &vi,
	std::vector<unsigned int> &ti,
	// outputs
	std::vector<GLfloat> & tangents
	//std::vector<vec3> & bitangents
)
{
	for (int i = 0; i < vi.size(); i += 3) { //we iterate over faces //group of 3 = 1 face

		// Shortcuts for vertices
		vec3 v0;
		v0[0] = v.at(3 * vi.at(i));
		v0[1] = v.at(3 * vi.at(i) + 1);
		v0[2] = v.at(3 * vi.at(i) + 2);

		vec3 v1;
		v1[0] = v.at(3 * vi.at(i + 1));
		v1[1] = v.at(3 * vi.at(i + 1) + 1);
		v1[2] = v.at(3 * vi.at(i + 1) + 2);

		vec3 v2;
		v2[0] = v.at(3 * vi.at(i + 2));
		v2[1] = v.at(3 * vi.at(i + 2) + 1);
		v2[2] = v.at(3 * vi.at(i + 2) + 2);


		// Edges of the triangle : postion delta
		vec3 deltaPos1;
		vec3_sub(deltaPos1, v1, v0);
		vec3 deltaPos2;
		vec3_sub(deltaPos1, v2, v0);
		//deltaPos1 = v1 - v0;
		//GLfloat deltaPos2 = v2 - v0;

		// Shortcuts for UVs
		float uv0x = uvs.at(2 * ti.at(i));
		float uv1x = uvs.at(2 * ti.at(i + 1));
		float uv2x = uvs.at(2 * ti.at(i + 2));

		float uv0y = uvs.at(2 * ti.at(i) + 1);
		float uv1y = uvs.at(2 * ti.at(i + 1) + 1);
		float uv2y = uvs.at(2 * ti.at(i + 2) + 1);

		// UV delta
		float deltaUV1x = uv1x = uv0x;
		float deltaUV1y = uv1y - uv0y;

		float deltaUV2x = uv2x - uv0x;
		float deltaUV2y = uv2y - uv0y;


		vec3 deltaPos1y;
		vec3 deltaPos2y;
		vec3 deltaPos1x;
		vec3 deltaPos2x;
		vec3_scale(deltaPos1y, deltaPos1, deltaUV2y);
		vec3_scale(deltaPos2y, deltaPos2, deltaUV1y);
		vec3_scale(deltaPos1x, deltaPos1, deltaUV2x);
		vec3_scale(deltaPos2x, deltaPos2, deltaUV1x);

		float r = 1.0f / (deltaUV1x * deltaUV2y - deltaUV1y * deltaUV2x);
		// glm::vec3  tangent = (deltaPos1 * deltaUV2y - deltaPos2 * deltaUV1y)*r;
		// glm::vec3  bitangent = (deltaPos2 * deltaUV1x - deltaPos1 * deltaUV2x)*r;

		vec3 tangent;
		vec3_sub(tangent, deltaPos1y, deltaPos2y);
		vec3_scale(tangent, tangent, r);

		//vec3 bitangent;
		//vec3_sub(bitangent, deltaPos1y, deltaPos2y);
		//vec3_scale(bitangent, bitangent, r);
		

		//tangents have the same index count as vi, since we calculate the tangent for 3 verticies it will be averaged and saved 3 times

		if (tangents.size() < i+3) {
			std::cout << "tangents not initialized properly" << std::endl;
			return;
		}
		else {
			tangents.at(3 * vi.at(i)) = tangent[0] + tangents.at(3 * vi.at(i));
			tangents.at(3 * vi.at(i) + 1) = tangent[1] + tangents.at(3 * vi.at(i) + 1);
			tangents.at(3 * vi.at(i) + 2) = tangent[2] + tangents.at(3 * vi.at(i) + 2);

			tangents.at(3 * vi.at(i + 1)) = tangent[0] + tangents.at(3 * vi.at(i + 1));
			tangents.at(3 * vi.at(i + 1) + 1) = tangent[1] + tangents.at(3 * vi.at(i + 1) + 1);
			tangents.at(3 * vi.at(i + 1) + 2) = tangent[2] + tangents.at(3 * vi.at(i + 1) + 2);

			tangents.at(3 * vi.at(i + 2)) = tangent[0] + tangents.at(3 * vi.at(i + 2));
			tangents.at(3 * vi.at(i + 2) + 1) = tangent[1] + tangents.at(3 * vi.at(i + 2) + 1);
			tangents.at(3 * vi.at(i + 2) + 2) = tangent[2] + tangents.at(3 * vi.at(i + 2) + 2);
		}

		// Set the same tangent for all three vertices of the triangle.
		// They will be merged later, in vboindexer.cpp
		//tangents.push_back(tangent);
		//tangents.push_back(tangent);
		//tangents.push_back(tangent);

		// Same thing for binormals
		//bitangents.push_back(bitangent);
		//bitangents.push_back(bitangent);
		//bitangents.push_back(bitangent);
	}
}