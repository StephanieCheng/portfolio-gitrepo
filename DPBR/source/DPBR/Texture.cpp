#include "Texture.h"

Texture::Texture(const std::string filename)
{
	id = 0;

	ddsLoader tex;
	if (!tex.Load(filename))
		return;

	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);

	unsigned int blockSize = (tex.GetFormat() == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
	unsigned int offset = 0;

	unsigned int width = tex.GetWidth();
	unsigned int height = tex.GetHeight();

	for (unsigned int level = 0; level < tex.GetMipMapCount() && (width || height); ++level)
	{
		unsigned int size = ((width + 3) / 4) * ((height + 3) / 4) * blockSize;
		glCompressedTexImage2D(GL_TEXTURE_2D, level, tex.GetFormat(), width, height, 0, size, tex.GetData() + offset);

		offset += size;
		width /= 2;
		height /= 2;
	}

	tex.Unload();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

Texture::~Texture()
{
	glDeleteTextures(1, &id);
}

GLuint Texture::GetId()
{
	return id;
}