#version 330 core

in vec2 texcoord;

out vec3 FragColor;

uniform sampler2D gPosition;
uniform sampler2D gNormals;
uniform sampler2D gColor;

uniform vec3 lightPosition;
uniform vec3 lightColor;
uniform float lightIntensity;
uniform int lightType;

void main()
{
	vec3 normal = texture2D(gNormals, texcoord).xyz;
	vec3 fragPos = texture2D(gPosition, texcoord).xyz;
	vec3 color = texture2D(gColor, texcoord).rgb;	
	
	if(lightType == 0)
	{
		FragColor = color * lightColor * lightIntensity;
	}
	else if(lightType == 1)
	{
		vec3 lightDirection = normalize(lightPosition - fragPos);
		vec3 Id = max(dot(normal , lightDirection),0.0) * color * lightColor * lightIntensity;
		FragColor = Id;
	}
	
	//FragColor = fragPos;
}