#include "Shader.h"
#include <fstream>
#include <sstream>
#include <iostream>


Shader::Shader()
{
	program = 0;
}

Shader::~Shader()
{
	Unload();
}

void Shader::Load(const std::string & filename)
{
	std::stringstream buffer;
	std::string source;

	std::ifstream vsf((filename + ".vert").c_str());
	buffer << vsf.rdbuf();
	source = buffer.str();
	const char* Vchars = source.c_str();
	
	GLuint vsid = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vsid, 1, &Vchars, NULL);
	glCompileShader(vsid);

	GLint status;
	glGetShaderiv(vsid, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		std::cout << "VERTEX SHADER ERROR" << std::endl;
		char buffer[512];
		glGetShaderInfoLog(vsid, 512, NULL, buffer);
		std::cout << buffer;
	}

	std::ifstream fsf((filename + ".frag").c_str());
	buffer.str(std::string());
	buffer << fsf.rdbuf();
	source = buffer.str();
	const char*  Fchars = source.c_str();

	GLuint fsid = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fsid, 1, &Fchars, NULL);
	glCompileShader(fsid);

	glGetShaderiv(fsid, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		std::cout << "FRAGMENT SHADER ERROR" << std::endl;
		char buffer[512];
		glGetShaderInfoLog(fsid, 512, NULL, buffer);
		std::cout << buffer;
	}

	program = glCreateProgram();

	glAttachShader(program, vsid);
	glAttachShader(program, fsid);

	glLinkProgram(program);

	glDeleteShader(vsid);
	glDeleteShader(fsid);
}

void Shader::Unload()
{
	if (program) glDeleteProgram(program);

	program = 0;
}

GLuint Shader::GetProgram()
{
	return program;
}

