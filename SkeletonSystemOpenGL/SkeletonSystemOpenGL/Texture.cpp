#include "Texture.h"
#include "SOIL.h"
#include <iostream>

Texture::Texture() {
	texture = 0;
	type = "";
}

Texture::~Texture() {
	//glDeleteTextures(1, &texture);
}
void Texture::Unload() {
	glDeleteTextures(1, &texture);
}

void Texture::LoadTextureFromDirectory(const char * path, std::string directory)
{
	//Generate texture ID and load texture data 
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	glGenTextures(1, &texture);
	int width, height;

	//filename = "qb/qb/3fe.jpg";
	std::cout << filename << std::endl;
	
	
	//cannot load JPG, but can load JPEG and PNG
	unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, 0, SOIL_LOAD_RGB);


	if (image == nullptr)
		std::cout << "nullptr texture" << std::endl;

	std::cout << width << " " << height << std::endl;
	// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(image);
}

void Texture::LoadTexture(const char* filename, const char* name) {
	int width, height;
	unsigned char* image = SOIL_load_image(filename, &width, &height, 0, SOIL_LOAD_RGB);

	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);



	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	//glGenrateMipMap(GL_TEXTURE_2D);

	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);

	type = name;
}

void Texture::SetTexture(GLuint tex)
{
	texture = tex;
}

void Texture::SetType(std::string type)
{
	this->type = type;
}

void Texture::SetPath(aiString path)
{
	this->path = path;
}

GLuint Texture::GetTexture() {
	return texture;
}

std::string Texture::GetType()
{
	return type;
}

aiString Texture::GetPath()
{
	return path;
}
