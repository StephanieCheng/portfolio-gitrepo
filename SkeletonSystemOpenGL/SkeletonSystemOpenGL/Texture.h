#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <assimp\scene.h>
class Texture {
public:
	Texture();
	~Texture();

	void Unload();

	void LoadTextureFromDirectory(const char * path, std::string directory);
	void LoadTexture(const char* filename, const char* name);
	void SetTexture(GLuint tex);
	void SetType(std::string type);
	void SetPath(aiString path);

	GLuint GetTexture();
	std::string GetType();
	aiString GetPath();

private:
	GLuint texture;
	std::string type;
	aiString path;
};