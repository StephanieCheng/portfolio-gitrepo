#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in ivec4 boneIDs;
layout (location = 2) in vec4 weights;

uniform int selectedBone;
uniform int renderType;

out vec4 col;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

const int MAX_BONES = 200;

uniform mat4 gBones[MAX_BONES];
void main(){
	vec4 newpos = vec4(position,1.0f);
	mat4 bonetransform = gBones[boneIDs[0]] * weights[0];	

	newpos = bonetransform * vec4(position, 1.0f);
	gl_Position =  projection * view * model * newpos;
	col = vec4(1.0f, 0.0f, 1.0f, 1.0f);
	if(renderType == 2){
			for(int i = 0; i < 1; ++i){
				if(boneIDs[i] == selectedBone){
					col = vec4(0.0f, 1.0f, 1.0f, 1.0f);
				}
			}
		}
	gl_PointSize = 5.0;
}