#pragma once
#include "linmath.h"
#include "Texture.h"
#include <vector>
#include <GL/glew.h>
#include "Shader.h"

#define NUM_BONES_PER_VERTEX 4

typedef struct Vertex {
	vec3 position;
	vec3 normal;
	vec2 texcoord;
};
struct MatStruct {
	mat4x4 mat;
};

typedef struct VertexBoneData {
	GLuint boneID[NUM_BONES_PER_VERTEX];
	GLfloat weights[NUM_BONES_PER_VERTEX];
};

class Mesh {
public:
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	Mesh();
	~Mesh();

	void Mesh::Unload();

	void setupMesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures);
	
	void assignSkeleton();

	void SetDataEBO(GLfloat * vertices, GLuint * indices, GLsizei sizeofvertices, GLsizei sizeofindices);

	void Draw(Shader& shader);

	GLuint GetVertexArray();

	void AddBoneData(GLuint BoneID, float weight, GLuint vertexID);

	bool UsesSkeleton();


	void printVertexBoneData();
private:
	GLuint vao, vbo, ebo;
	bool usesSkeleton = false;
	std::vector<VertexBoneData> vbd;
	GLuint boneVB;

};

class SkeletonMesh{
public:

	SkeletonMesh();
	~SkeletonMesh();

	//vec3 wont work because of reasons
	std::vector<GLfloat> positions;
	std::vector<VertexBoneData> bones;

	void AddBoneData(GLint prevBoneID, GLuint BoneID, vec3 pos, vec3 pos2);

	void assignToBuffers();

	void Draw();
private:
	GLuint vao, vbo, ebo, boneVB;
};