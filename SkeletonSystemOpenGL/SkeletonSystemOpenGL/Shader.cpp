#include "Shader.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

Shader::Shader() {
	shaderProgram = 0;
}

Shader::~Shader() {
	Unload();
}
void Shader::Unload() {
	std::cout << "shader unloaded" << std::endl;
	if (shaderProgram) glDeleteProgram(shaderProgram);

	shaderProgram = 0;
}

void Shader::Load(const std::string &filename) {

	//vertex read
	std::ifstream file((filename + ".vert").c_str());
	if (!file.is_open()) {
		std::cout << "could not open file" << std::endl;
		return;
	}
	std::stringstream buffer;

	buffer << file.rdbuf();
	std::string str = buffer.str();
	const GLchar* vertexShaderSource = str.c_str();
	//std::cout << vertexShaderSource << std::endl;


	//compile
	GLuint vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);


	//check compile
	GLint success;
	GLchar infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	//fragment read
	std::ifstream file2((filename + ".frag").c_str());
	if (!file2.is_open()) {
		std::cout << "could not open file" << std::endl;
		return;
	}
	std::stringstream buffer2;
	buffer2 << file2.rdbuf();
	str = buffer2.str();
	const GLchar* fragmentShaderSource = str.c_str();
	//std::cout << fragmentShaderSource << std::endl;


	//fragment
	GLuint fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);


	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	//creating a program
	Shader::shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	//check
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::LINKER_FAILED\n" << infoLog << std::endl;
	}
	
	glDeleteShader(vertexShader);
	glDeleteProgram(fragmentShader);

}

GLuint Shader::GetProgram()
{

	return shaderProgram;
}

void Shader::UseProgram()
{
	glUseProgram(shaderProgram);
}
