/** Camera Controls.
*  WASD -> move
* SHIFT + mouse -> look around
* CTRL + mouse -> pan
* ALT + mouse -> orbit around look at point
* scroll -> zoom in/out
*/


/** Animation Controls.
* 0 -> turn off animations
* 1 -> turn on animations / play next animation
* P -> play / pause animation
* right arrow -> next frame (pauses the animation and advances a frame)
* left arrow -> previous frame
* CTRL + right arrow -> skip 10 frames
* CTRL + left arrow -> go back 10 frames
* B -> toggle playing blends or regular animations
*/

/** Render Controls
* Press K to toggle render modes -- UNSHADED, ALLWEIGHTS and WEIGHTS per bone
* SHIFT + . -- to iterate over bones only in render type WEIGHT
* Press L to toggle armature
*/

/**Console Commands
* F1 – start console
* "playAnimation" --invoke an animation by name
* "selectBone" --select a bone in bone weight render mode to display its weights
* "turnOnMask" – create a mask by choosing a Bone from which the animation plays, and then the animaion that will Play
* "clearMasks" – removes the current mask
* "createTransition" - create and store a transition (indices start and 0)
* “createAnimationBlend” – create and store an animation blend(indices start at 0)
* “playAnimationBlend” – play an animation blend by its index
* “listAnimations” – lists imported animations by name(animations with the prefix “rig | ” should be typed without it)
*/




//#define GLEW_STATIC
#define _USE_MATH_DEFINES
#include <cmath> 


#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <assimp\Importer.hpp>

#include "Shader.h"
#include "Model.h"
#include "Texture.h"
#include "linmath.h"
#include "Camera.h"

#include <chrono>


#include <iostream>


typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::milliseconds ms;
typedef std::chrono::duration<float> fsec;

#define ASPECTRATIO 1.3333333f
#define WINDOW_WIDTH 800.0f
#define	WINDOW_HEIGHT (WINDOW_WIDTH / ASPECTRATIO)
#define ORBITSPEED (12.5664/WINDOW_WIDTH)

void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void do_WASDmovement();
void handleKeyInput(Model* model);


Camera camera = Camera::Camera();

bool keys[1024];
bool pressed[1024];

GLfloat deltaTime = 0.0f;	// Time between current frame and last frame
GLfloat lastFrame = 0.0f;  	// Time of last frame

GLfloat lastX = 400, lastY = 300;

GLfloat fov = 45.0f;

bool firstMouse = true;

bool animation = false;

bool blending = false;

bool mask = false;

Time::time_point t0;

bool drawArmatures = false;

enum RenderType {
	UNSHADED,
	ALLWEIGHTS,
	WEIGHT
};

RenderType renderType = UNSHADED;
int selectedBone = 0;

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);


	GLFWwindow* window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Skeleton", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);


	glfwSetKeyCallback(window, key_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetCursorPos(window, 800.0f / 2, 600.0f / 2);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	glViewport(0, 0, width, height);

	glEnable(GL_DEPTH_TEST);


	Shader shader = Shader::Shader();
	shader.Load("shader");

	Shader skeletonShader = Shader::Shader();
	skeletonShader.Load("skeletonShader");


	//load fbx with triangulization, load dae without
	Model model = Model::Model();
	//model.LoadAssimp("models/nanosuit.obj", false);
	//model.LoadAssimp("three/three.dae", false);
	//model.LoadAssimp("qb/qub4.dae", false);
	//model.LoadAssimp("qb/test2.dae", false);
	//model.LoadAssimp("char/chara12-bindposefixDBO2.fbx", true);
	//model.LoadAssimp("qb/tail2.fbx", true);
	model.LoadAssimp("char/chara13-AssimpReady.fbx", true);



	//generic armature bone, will be handled specially
	//Model& bone = Model::Model();
	//bone.LoadAssimp("Bone/Bone.fbx");



	//model.SetPosition(0.0f, -1.0f, 0.0f);
	//model.SetPosition(0.0f, -1.75f, 0.0f);
	model.SetScale(0.02f, 0.02f, 0.02f);
	//model.SetScale(0.2f, 0.2f, 0.2f);

	mat4x4 projectionMat;
	mat4x4_perspective(projectionMat, degreesToRadians(fov), ASPECTRATIO, 0.01f, 100.0f);
	
	camera.SetYFov(degreesToRadians(fov));
	camera.SetAspectRatio(ASPECTRATIO);
	camera.SetNearPlane(0.01f);
	camera.SetFarPlane(1000.0f);


	camera.SetPosition(0.0f, 0.0f, 5.0f);
	camera.SetLookAt(0.0f, 0.0f, 0.0f);
	camera.SetUpVector(0.0f, 1.0f, 0.0f);

	//Renderer renderer = Renderer::Renderer();
	//renderer.SetCamera(camera);

	//std::cout << "shader " << shader.GetProgram() << std::endl;

	
	//model.printMeshBones();
	
	t0 = Time::now();

	while (!glfwWindowShouldClose(window))
	{

		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// Check and call events
		glfwPollEvents();
		do_WASDmovement();
		handleKeyInput(&model);

		// Rendering commands here
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glUseProgram(shader.GetProgram());

		mat4x4 viewMat;
		camera.GetLookAtMatrix(viewMat);

		

		GLuint viewLoc = glGetUniformLocation(shader.GetProgram(), "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, (GLfloat *)viewMat);

		GLuint projectionLoc = glGetUniformLocation(shader.GetProgram(), "projection");
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, (GLfloat *)projectionMat);


		mat4x4 modelMat;
		model.GetModelMatrix(modelMat);
		GLuint modelLoc = glGetUniformLocation(shader.GetProgram(), "model");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, (GLfloat *)modelMat);


		GLuint boneLoc = glGetUniformLocation(shader.GetProgram(), "selectedBone");
		glUniform1i(boneLoc, selectedBone);

		GLuint renderLoc = glGetUniformLocation(shader.GetProgram(), "renderType");
		glUniform1i(renderLoc, renderType);


		//needed for animation
		auto t1 = Time::now();

		fsec RunningTime =  (t1-t0);

		//std::cout << RunningTime.count() << std::endl;

		std::vector<MatStruct> Transforms;

		model.BoneTransforms(RunningTime.count(), Transforms, drawArmatures);

		//model.printMeshBones();

		for (int i = 0; i < Transforms.size(); i++) {
			std::string res1 = "gBones[" + std::to_string(i) +"]";
			const char* res = res1.c_str();

			GLuint gBones = glGetUniformLocation(shader.GetProgram(), res);

			//std::cout << gBones << std::endl;

			mat4x4 id;
			mat4x4_identity(id);
			glUniformMatrix4fv(gBones, 1, GL_FALSE, (GLfloat *)Transforms[i].mat);
		}



		model.Draw(shader);

		if (drawArmatures) {

			glUseProgram(skeletonShader.GetProgram());

			viewLoc = glGetUniformLocation(skeletonShader.GetProgram(), "view");
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, (GLfloat *)viewMat);

			projectionLoc = glGetUniformLocation(skeletonShader.GetProgram(), "projection");
			glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, (GLfloat *)projectionMat);


			modelLoc = glGetUniformLocation(skeletonShader.GetProgram(), "model");
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, (GLfloat *)modelMat);

			GLuint renderLoc = glGetUniformLocation(shader.GetProgram(), "renderType");
			glUniform1i(renderLoc, renderType);

			boneLoc = glGetUniformLocation(skeletonShader.GetProgram(), "selectedBone");
			glUniform1i(boneLoc, selectedBone);

			for (int i = 0; i < Transforms.size(); i++) {
				std::string res1 = "gBones[" + std::to_string(i) + "]";
				const char* res = res1.c_str();

				GLuint gBones = glGetUniformLocation(skeletonShader.GetProgram(), res);

				//std::cout << gBones << std::endl;

				mat4x4 id;
				mat4x4_identity(id);
				glUniformMatrix4fv(gBones, 1, GL_FALSE, (GLfloat *)Transforms[i].mat);
			}

			model.DrawArmature();
		}

		// Swap the buffers
		glfwSwapBuffers(window);
	}

	model.Unload();


	glfwTerminate();

	return 0;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	
	xpos = xpos - WINDOW_WIDTH / 2;
	ypos = ypos - WINDOW_HEIGHT / 2;


	if (firstMouse) {
		//glfwSetMousePos(xpos, ypos);
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	


	if (keys[GLFW_KEY_LEFT_SHIFT]) {
		GLfloat xoffset = (xpos - lastX) * ORBITSPEED;
		GLfloat yoffset = (lastY - ypos) * ORBITSPEED;
		lastX = xpos;
		lastY = ypos;

		//std::cout << "offset " << xoffset << " " << yoffset << std::endl;

		camera.LookAround(xoffset, yoffset);
	}

	

	else if (keys[GLFW_KEY_LEFT_ALT]){
		GLfloat xoffset = (lastX - xpos) * ORBITSPEED;
		GLfloat yoffset = (lastY - ypos) * ORBITSPEED;
		lastX = xpos;
		lastY = ypos;



		camera.OrbitCamera(xoffset, yoffset);
	}
	else if (keys[GLFW_KEY_LEFT_CONTROL]) {
		GLfloat xoffset = (lastX - xpos) * ORBITSPEED;
		GLfloat yoffset = (lastY - ypos) * ORBITSPEED;
		camera.PanCamera(xoffset, yoffset);
	}
	lastX = xpos;
	lastY = ypos;

}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{

	// go forward and backward without changing look at
	vec3 camPos;
	camera.GetPosition(camPos);
	GLfloat cameraSpeed = 100.0f * deltaTime;
	vec3 cameraForward;
	camera.GetForwardVector(cameraForward);
	vec3 cameraForwardNorm;
	vec3_norm(cameraForwardNorm, cameraForward);
	camPos[0] += cameraSpeed * yoffset * cameraForwardNorm[0];
	camPos[1] += cameraSpeed * yoffset * cameraForwardNorm[1];
	camPos[2] += cameraSpeed * yoffset * cameraForwardNorm[2];

	camera.SetPosition(camPos[0], camPos[1], camPos[2]);

}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	// When a user presses the escape key, we set the WindowShouldClose property to true, 
	// closing the application
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	//TODO for certain keys we must turn off continuous input

	if (action == GLFW_PRESS) {
		keys[key] = true;
	}
	else if (action == GLFW_RELEASE) {
		pressed[key] = false;
		keys[key] = false;
	}


	/*if (key == GLFW_KEY_0) {
		animation = false;
	}
	if (key == GLFW_KEY_1) {
		animation = true;
	}*/
		
}

void do_WASDmovement() {
	vec3 camPos;
	camera.GetPosition(camPos);
	vec3 cameraForward;
	camera.GetForwardVector(cameraForward);
	vec3 right;
	camera.GetRightVector(right);
	vec3 cameraForwardNorm;
	vec3_norm(cameraForwardNorm, cameraForward);


	GLfloat cameraSpeed = 5.0f * deltaTime;
	if (keys[GLFW_KEY_W]) {
		camPos[0] += cameraSpeed * cameraForwardNorm[0];
		camPos[1] += cameraSpeed * cameraForwardNorm[1];
		camPos[2] += cameraSpeed * cameraForwardNorm[2];
	}

	if (keys[GLFW_KEY_S]) {
		camPos[0] -= cameraSpeed * cameraForwardNorm[0];
		camPos[1] -= cameraSpeed * cameraForwardNorm[1];
		camPos[2] -= cameraSpeed * cameraForwardNorm[2];
	}


	if (keys[GLFW_KEY_A]){


		camPos[0] -= cameraSpeed * right[0];
		camPos[1] -= cameraSpeed * right[1];
		camPos[2] -= cameraSpeed * right[2];

	}


	if (keys[GLFW_KEY_D]) {

		camPos[0] += cameraSpeed * right[0];
		camPos[1] += cameraSpeed * right[1];
		camPos[2] += cameraSpeed * right[2];
	}

	
	camera.SetPosition(camPos[0], camPos[1], camPos[2]);
	camera.SetForwardVector(cameraForward[0], cameraForward[1], cameraForward[2]);
	
}

void handleKeyInputforModel(Model * model) {
}
// needs the selected model (can be null)
void handleKeyInput(Model * model) {


	if (keys[GLFW_KEY_K] == true && pressed[GLFW_KEY_K] == false) {
		pressed[GLFW_KEY_K] = true;
		switch (renderType) {
		case UNSHADED: renderType = ALLWEIGHTS; std::cout << "renderType allWeights" << std::endl;
			break;
		case ALLWEIGHTS: renderType = WEIGHT; std::cout << "renderType Weight" << std::endl;
			selectedBone = 0;
			break;
		case WEIGHT: renderType = UNSHADED; std::cout << "renderType Unshaded" << std::endl;
			break;
		default: renderType = UNSHADED;
		}
	}
	if (keys[GLFW_KEY_L] == true && pressed[GLFW_KEY_L] == false) {
		pressed[GLFW_KEY_L] = true;
		drawArmatures = !drawArmatures;
	}

	if (model == NULL) { return; }

	if (renderType == WEIGHT) {
		if ((keys[GLFW_KEY_RIGHT_SHIFT] || keys[GLFW_KEY_LEFT_SHIFT]) && keys[GLFW_KEY_PERIOD] && pressed[GLFW_KEY_PERIOD] == false) {
			pressed[GLFW_KEY_PERIOD] = true;
			selectedBone++;
			if (selectedBone == model->boneMap.size()) { selectedBone = 0; }
			//really slow debug for writing out selected bones name
			/*std::cout << selectedBone << std::endl;
			for (std::map<std::string, int>::iterator it = model->boneMap.begin(); it != model->boneMap.end(); ++it) {
				if (it->second == selectedBone)
					std::cout << "selected " << it->first << std::endl;
			}*/
		}
	}
	//iterates through animations or animationBlends
	if (keys[GLFW_KEY_1] == true && pressed[GLFW_KEY_1] == false) {
		pressed[GLFW_KEY_1] = true;
		if (model->animateSpecial != -1) {
			t0 = Time::now();
			model->PlayNextBlend();
		}
		else {
			//play next animation
			t0 = Time::now();
			model->PlayNextAnimation();
		}
	}
	if (keys[GLFW_KEY_0] == true && pressed[GLFW_KEY_0] == false) {
		pressed[GLFW_KEY_0] = true;
		model->StopAnimation();
		model->TurnOffPlayBlend();
		std::cout << "Stopped animation " << std::endl;
	}
	if (keys[GLFW_KEY_P] == true && pressed[GLFW_KEY_P] == false) {
		pressed[GLFW_KEY_P] = true;
		model->TogglePlayPause();
	}
	if (keys[GLFW_KEY_RIGHT] == true) {
		if (keys[GLFW_KEY_LEFT_CONTROL] == true || keys[GLFW_KEY_RIGHT_CONTROL] == true) {
			model->ForwardTenFrames();
		}
		else {
			model->NextFrame();
		}
	}
	if (keys[GLFW_KEY_LEFT] == true) {
		if (keys[GLFW_KEY_LEFT_CONTROL] == true || keys[GLFW_KEY_RIGHT_CONTROL] == true) {
			model->BackwardTenFrames();
		}
		else {
			model->PreviousFrame();
		}
	}

	if (keys[GLFW_KEY_UP] == true) {
		model->AddBlendWeight(false);
	}
	if (keys[GLFW_KEY_DOWN] == true) {
		model->AddBlendWeight(true);
	}

	//turn on/off playing blends
	if (keys[GLFW_KEY_B] == true && pressed[GLFW_KEY_B] == false) {
		pressed[GLFW_KEY_B] = true;
		model->TogglePlayAnimBlend();
	}

	//turn on of blends vs transitions
	if (keys[GLFW_KEY_B] == true && pressed[GLFW_KEY_B] == false && (keys[GLFW_KEY_LEFT_SHIFT] || keys[GLFW_KEY_RIGHT_SHIFT])) {
		pressed[GLFW_KEY_B] = true;
		model->ToggleTransitionsOrBlends();
	}
	
	//open console window and handle commands
	if (keys[GLFW_KEY_F1] == true && pressed[GLFW_KEY_F1] == false) {
		pressed[GLFW_KEY_F1] = true;
		std::cout << "console open " << std::endl;
		std::cout << "commands: turnOnMask, clearMasks, playAnimation, createTransition, createAnimationBlend, selectBone, playAnimationBlend, listAnimations " << std::endl;
		//open console input
		std::string command;
		std::cin >> command;
		//std::cout << command << std::endl;
		if (command == "playAnimation") {
			std::string AnimationName;
			std::cin >> AnimationName;
			model->PlayAnimation(AnimationName);
		}
		else if (command == "playAnimationBlend") {
			int index;
			std::cin >> index;
			model->PlayAnimBlend(index);
		}
		else if (command == "listAnimations") {
			std::vector<std::string> v;
			for (std::map<std::string, std::map<std::string, aiNodeAnim*> >::iterator it = model->animations.begin(); it != model->animations.end(); ++it) {
				v.push_back(it->first);
				std::cout << it->first << "\n";
			}
		}
		else if (command == "turnOnMask") {
			std::cout << "root bone of mask?" << std::endl;
			std::string BoneName;
			std::cin >> BoneName;
			std::cout << "which animation to play on mask?" << std::endl;
			std::string AnimationName;
			std::cin >> AnimationName;
			//set mask and assign an animation to it in model
			model->SetMaskAndAnimation(BoneName, AnimationName);
		}
		else if (command == "clearMasks") {
			model->ClearMasks();
		}
		else if (command == "createTransition") {
			std::cout << "first animation: " << std::endl;
			std::string anim1;
			std::cin >> anim1;
			std::cout << "second animation: " << std::endl;
			std::string anim2;
			std::cin >> anim2;

			int iAnim1 = model->FindAnimationIndex(anim1);
			int iAnim2 = model->FindAnimationIndex(anim2);
			std::cout << "transition start frame: (" << anim1 << " duration in frames: " << model->scene->mAnimations[iAnim1]->mDuration << " )" << std::endl;
			int frameStart;
			std::cin >> frameStart;

			std::cout << "transition end frame:" << std::endl;

			int frameEnd;
			std::cin >> frameEnd;

			std::cout << "result : " << std::endl;
			//display blend duration in ticks
			
			for (int i = 0; i < model->scene->mAnimations[iAnim1]->mDuration; i++) {
				std::cout << ". ";
			}
			std::cout << std::endl;
			for (int i = 0; i < frameStart; i++) {
				std::cout << "  ";
			}
			for (int i = 0; i < model->scene->mAnimations[iAnim2]->mDuration; i++) {
				std::cout << ". ";
			}
			std::cout << std::endl;

			//play animation blend
			model->MakeTransitions(iAnim1, iAnim2, frameStart, frameEnd);

		}
		else if (command == "createAnimationBlend") {
			std::cout << "first animation: " << std::endl;
			std::string anim1;
			std::cin >> anim1;
			std::cout << "second animation: " << std::endl;
			std::string anim2;
			std::cin >> anim2;
			int iAnim1 = model->FindAnimationIndex(anim1);
			int iAnim2 = model->FindAnimationIndex(anim2);
			std::cout << "playback weight (0 to 1): " << std::endl;
			float weight;
			std::cin >> weight;

			model->MakeBlend(iAnim1, iAnim2, weight);
		}
		else if (command == "selectBone") {
			std::cin >> command;
			selectedBone = model->SelectBone(command);
		}
		else {
			std::cout << "invalid command" << std::endl;
		}
	}
}