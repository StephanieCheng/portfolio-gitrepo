An OpenGL application for reading and displaying skeleton data, and playing keyframed animations.<br/><br/>

<b>Camera controls:</b><br/>
W A S D – move<br/>
SHIFT + mouse -> look around<br/>
CTRL + mouse -> pan<br/>
ALT + mouse -> orbit around look at point<br/>
scroll -> zoom in/out<br/><br/>

**Animation Controls:**<br/>
0 -> turn off animations<br/>
1 -> turn on animations / play next animation<br/>
P -> play / pause animation<br/>
right arrow -> next frame (pauses the animation and advances a frame)<br/>
left arrow -> previous frame<br/>
CTRL + right arrow -> skip 10 frames<br/>
CTRL + left arrow -> go back 10 frames<br/>
B -> toggle playing blends or regular animations<br/>
Up arrow – increase the weight when playing an animation blend<br/>
Down arrow – decrease the weight when playing an animation blend<br/><br/>

**Render Controls:**<br/>
Press K to toggle render modes -- UNSHADED, ALLWEIGHTS and WEIGHTS per bone<br/>
SHIFT + . -- to iterate over bones only in render type WEIGHTS<br/>
Press L to toggle armature<br/><br/>

**Console Commands:**<br/>
F1 – start console<br/>
"playAnimation" -- invoke an animation by name<br/>
"selectBone" -- select a bone in bone weight render mode to display its weights<br/>
"turnOnMask" – create a mask by choosing a Bone from which the animation plays, and then the animation that will Play<br/>
"clearMasks" – removes the current mask<br/>
“createAnimationBlend” – create and store an animation blend (indices start at 0)<br/>
“createTransition” – create and store a transition (indices start at 0)<br/>
“playAnimationBlend” – play an animation blend by its index<br/>
“listAnimations” – lists imported animations by name (animations with the prefix “rig|” should be typed without it)<br/>


**Examples:**<br/><br/>
<img src="https://imgur.com/3dJeRWj.png" width="500">
<img src="https://imgur.com/pmjGnbN.png" width="500">
<img src="https://imgur.com/i4hBi7s.png" width="500">
<img src="https://imgur.com/Qw9b9uN.png" width="500">
<img src="https://imgur.com/kXxqDbK.png" width="500">
<img src="https://imgur.com/KyyxeV8.png" width="500">
