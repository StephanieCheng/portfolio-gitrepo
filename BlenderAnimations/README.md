### Walk and Run anaimation  
<img src="https://a.pomf.cat/flaheq.gif" width="300">  
<img src="https://a.pomf.cat/gjbcns.gif" width="300">  


### Jumping Animation

Animation stages:

<img src="https://imgur.com/FeGIY0m.gif" width="300">
<img src="https://imgur.com/4n1wphe.gif" width="300">
<img src="https://imgur.com/EHI0f30.gif" width="300">
<img src="https://imgur.com/ZK31h3r.gif" width="300">


How it looks ingame:

<img src="https://imgur.com/XK1dnJd.gif">

### Attack Animation

Animation stages:

<img src="https://imgur.com/MYzzCUW.gif" width="300">
<img src="https://imgur.com/H4sn7cx.gif" width="300">
<img src="https://imgur.com/KxtvWj3.gif" width="300">
<img src="https://imgur.com/zieBbIe.gif" width="300">

How it looks ingame:

<img src="https://imgur.com/iN9XAs3.gif">
<img src="https://imgur.com/B7uxWoe.gif"> 
